<?php

class Member_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function register($data) {
        $this->db->insert('member', $data);
    }

    function login($email, $password) {
        $this->db->from('member');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        return $this->db->count_all_results() > 0;
    }

    function check_email($email) {
        $this->db->from('member');
        $this->db->where('email', $email);
        return $this->db->count_all_results();
    }

    function get_id_from_email($email) {
        $this->db->from('member');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row()->id;
    }

    function get_member_data($member_id) {
        $this->db->from('member');
        $this->db->where('id', $member_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_member_id() {
        $CI = & get_instance();
        $CI->load->model('Member_model', 'member');
        $member_id = $CI->member->get_id_from_email($this->session->userdata('email'));
        return $member_id;
    }

    function get_lab_from_id($lab_id) {
        $this->db->from('equipment_type');
        $this->db->where('id', $lab_id);
        $query = $this->db->get();
        return $query->row();
    }
}

?>