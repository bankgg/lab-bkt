<?php
class News_model extends CI_Model {
    function __construct() {
        parent::__construct();   
    }
    
    function submit_news($data) {
		return $this->db->insert('news', $data);	
	}
	
	function get_news($limit_start = 0, $limit = 10) {
		$this->db->from('news');
		$this->db->order_by('id', 'desc'); 
		//$this->db->limit($limit, $limit_start);
		$query = $this->db->get();
		return $query->result(); 
	}
	
	function count_news() {
		$this->db->from('news');
		return $this->db->count_all_results();	
	}
	
	function delete_news($id) {
		$a = $this->db->delete('news', array('id' => $id));	
		$b = $this->db->delete('news_edit', array('news_id' => $id));
		return $a && $b;
	}
	
	function get_news_data($id) {
		$this->db->from('news');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row();	
	}
	
	function submit_edit_news($news_id, $member_id, $data) {
		$this->db->where('id', $news_id);
		$a = $this->db->update('news', $data);
		
		$d = array(
				'news_id'	=>	$news_id,
				'member_id'	=>	$member_id,
				);
		$b = $this->db->insert('news_edit', $d);
		
		return $a && $b;
	}
	
	function get_home_news() {
		$this->db->from('news');
		$this->db->order_by('id', 'desc');
		$this->db->limit(10);	
		$query = $this->db->get();
		return $query->result();
	}
}
?>