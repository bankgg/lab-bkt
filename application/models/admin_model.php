<?php

class Admin_model extends CI_Model {

    private $member_id = 0;

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->model('Member_model', 'member');
        $this->member_id = $CI->member->get_id_from_email($this->session->userdata('email'));
    }

    function add_type($data) {
        return $this->db->insert('equipment_type', $data);
    }

    function add_room($data) {
        return $this->db->insert('room', $data);
    }

    function add_equipment($data) {
        return $this->db->insert('equipment', $data);
    }

    function get_all_equipment_data() {
        $this->db->select('equipment_id, e.name, r.name as room, limit_times, limit_hours, tag, columns, asset_id, serial_no, budget_name, brand, model, place_at,' .
                ' remark, equipment_image_path, specific_info_path, e.status, concat(m.firstname, \' \', m.lastname) as person_responsible, seller_name, seller_tel_no, seller_address,' .
                ' department_responsible_id, department_responsible_name, b.name as buying_method, received_date, buying_cost, funds, department_id,' .
                ' department_name, plan, project, date_start_calculate, et.name as type', false);

        $this->db->from('equipment e');
        $this->db->join('buying_method b', 'e.buying_method = b.id', 'left');
        $this->db->join('room r', 'e.room = r.id');
        $this->db->join('equipment_type et', 'r.type = et.id');
        $this->db->join('member m', 'e.person_responsible = m.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function get_buying_methods() {
        $this->db->from('buying_method');
        $query = $this->db->get();
        return $query->result();
    }

    function get_stats($room) {
        $query = $this->db->query('SELECT equipment.equipment_id as equipment_id,name, tag, SEC_TO_TIME( SUM( TIME_TO_SEC( TIMEDIFF( time_end, time_start ) ) ) ) AS hours, sum(abs(time_to_sec(timediff(time_end, time_start))) / 60 / 60) as real_hours, COUNT( * ) AS times, limit_hours, limit_times
FROM log_book
JOIN booking on booking.booking_id = log_book.booking_id
RIGHT JOIN equipment ON equipment.equipment_id = booking.equipment_id where room = ' . $room . ' group by equipment.equipment_id');
        return $query->result();
    }

    function get_approved_forms($lab) {
        $query = $this->db->query('select rq.id as id, student_id, concat(firstname, \' \', lastname) as name, 
			(select group_concat(concat(equipment.name) separator \', \')
			from equipment
			join request_item on equipment.equipment_id = request_item.equipment_id
			where request_form_id = rq.id
			group by request_form_id) as items, equipment_type.name as lab, concat(start_date, \' - \', end_date) as date
			from request_form rq
			join member on rq.member_id = member.id
			join equipment_type on rq.lab = equipment_type.id
			where rq.status = 1 and rq.lab = \'' . $lab . '\' order by rq.timestamp desc');
        return $query->result();
    }

    function get_first_lab() {
        $this->db->from('equipment_type');
        $this->db->order_by('id', 'asc');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    function get_unapproved_forms() {
        $query = $this->db->query('select rq.id as id, cast(rq.timestamp as date) as d, student_id, concat(firstname, \' \', lastname) as name, course, equipment_type.name as lab,
						(select group_concat(concat(equipment.name) separator \', \')
									from equipment
									join request_item on equipment.equipment_id = request_item.equipment_id
									where request_form_id = rq.id
									group by request_form_id) as items
						, concat(start_date, \' - \', end_date) as date
						from request_form rq
						join member on rq.member_id = member.id
						join equipment_type on rq.lab = equipment_type.id
						where rq.status = 0 order by rq.timestamp desc');
        return $query->result();
    }

    function request_approve($id) {
        $data = array('status' => '1');
        $this->db->where('id', $id);
        $this->db->update('request_form', $data);
    }

    function request_unapprove($id) {
        $data = array('status' => '0');
        $this->db->where('id', $id);
        $this->db->update('request_form', $data);
    }

    function request_cancel($id) {
        $this->db->delete('request_form', array('id' => $id));
        $this->db->delete('request_item', array('request_form_id' => $id));
    }

    function get_request_form($id) {
        $this->db->select('request_form.*, equipment_type.name as type_name');
        $this->db->from('request_form');
        $this->db->join('equipment_type', 'request_form.lab = equipment_type.id');
        $this->db->where('request_form.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_request_form_items($id) {
        $this->db->select('equipment.name');
        $this->db->from('request_form');
        $this->db->join('request_item', 'request_form.id = request_item.request_form_id');
        $this->db->join('equipment', 'request_item.equipment_id = equipment.equipment_id');
        $this->db->where('request_form.id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_equipment_by_id($equipment_id) {
        $this->db->select('equipment_id, e.name, r.name as room, limit_times, limit_hours, tag, columns, asset_id, serial_no, budget_name, brand, model, place_at,' .
                ' remark, equipment_image_path, specific_info_path, e.status, concat(m.firstname, \' \', m.lastname) as person_responsible, seller_name, seller_tel_no, seller_address,' .
                ' department_responsible_id, department_responsible_name, b.name as buying_method, received_date, buying_cost, funds, department_id,' .
                ' department_name, plan, project, date_start_calculate, et.name as type', false);

        $this->db->from('equipment e');
        $this->db->join('buying_method b', 'e.buying_method = b.id', 'left');
        $this->db->join('room r', 'e.room = r.id');
        $this->db->join('equipment_type et', 'r.type = et.id');
        $this->db->join('member m', 'e.person_responsible = m.id', 'left');
        $this->db->where('e.equipment_id', $equipment_id);
        $query = $this->db->get();
        return $query->row();
    }

}

?>