<?php

class Me_model extends CI_Model {

    private $member_id = 0;

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->model('Member_model', 'member');
        $this->member_id = $CI->member->get_id_from_email($this->session->userdata('email'));
    }
    
    function get_privilege() {
        $this->db->select('p.id, p.name');
        $this->db->from('member m');
        $this->db->join('privilege p', 'm.privilege = p.id');
        $this->db->where('m.id', $this->member_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_me() {
        $this->db->select('member.id, student_id, password, firstname, lastname, email, tel, tel_in, equipment_type.name as lab, university, faculty, course, status, image_path');
        $this->db->from('member');
        $this->db->join('equipment_type', 'member.lab = equipment_type.id', 'left');
        $this->db->where('member.id', $this->member_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_booking($limit = 0) {
        $this->db->select('equipment.name as equipment_name, equipment.tag as equipment_tag, equipment_type.name as type, room.name as room_name, room.id as room_id, booking.timestamp as timestamp, start, end, count(end) as c');
        $this->db->from('member');
        $this->db->join('booking', 'booking.member_id = member.id');
        $this->db->join('equipment', 'equipment.equipment_id = booking.equipment_id');
        $this->db->join('room', 'equipment.room = room.id');
        $this->db->join('equipment_type', 'equipment_type.id = room.type');
        $this->db->where('member.id', $this->member_id);
        $this->db->where('booking.timestamp >', strtotime(date('Y-m-d H:i:s')));
        $this->db->group_by('booking.timestamp');
        $this->db->order_by('booking.timestamp', 'desc');
        //$this->db->limit(15, $limit);
        $query = $this->db->get();
        return $query->result();
    }

    function count_booking() {
        $this->db->from('booking');
        $this->db->where('member_id', $this->member_id);
        $this->db->group_by('timestamp');
        return count($this->db->get()->result());
    }

    function get_privileges($type) {
        $this->db->distinct();
        $this->db->select('equipment.name as name');
        $this->db->from('request_item');
        $this->db->join('request_form', 'request_item.request_form_id = request_form.id');
        $this->db->join('equipment', 'equipment.equipment_id = request_item.equipment_id');
        $this->db->join('member', 'request_form.member_id = member.id');
        $this->db->join('room', 'equipment.room = room.id');
        $this->db->join('equipment_type', 'equipment_type.id = room.type');
        $this->db->where('member.id', $this->member_id);
        $this->db->where('type', $type);
        $this->db->where('request_form.status', '1');
        $this->db->where('curdate() >=', 'start_date', false);
        $this->db->where('curdate() <=', 'end_date', false);
        $query = $this->db->get();
        return $query->result();
    }

    function get_types_i_can() {
        $this->db->distinct();
        $this->db->select('equipment_type.id as id, equipment_type.name as name');
        $this->db->from('equipment');
        $this->db->join('request_item', 'request_item.equipment_id = equipment.equipment_id');
        $this->db->join('request_form', 'request_item.request_form_id = request_form.id');
        $this->db->join('room', 'equipment.room = room.id');
        $this->db->join('equipment_type', 'room.type = equipment_type.id');
        $this->db->join('member', 'member.id = request_form.member_id');
        $this->db->where('member.id', $this->member_id);
        $this->db->where('request_form.status', '1');
        $this->db->where('curdate() >=', 'start_date', false);
        $this->db->where('curdate() <=', 'end_date', false);
        $query = $this->db->get();
        return $query->result();
    }

    function save_log($data) {
        return $this->db->insert('log_book', $data);
    }

    function request($data) {
        $this->db->insert('request_form', $data);
        return $this->db->insert_id();
    }

    function request_item_add($request_form_id, $equipment_id) {
        $this->db->insert('request_item', array(
            'request_form_id' => $request_form_id,
            'equipment_id' => $equipment_id)
        );
    }

    function delete_approval($request_form_id) {
        return $this->db->delete('request_item', array('request_form_id' => $request_form_id)) && $this->db->delete('request_form', array('id' => $request_form_id));
    }

    function get_requestform_by_member() {
        $member_id = $this->member->get_member_id();
        $this->db->select("request_form.id,status,request_form.timestamp,equipment.name as equipment_name,equipment_type.name as type_name ");
        $this->db->from('request_form');
        $this->db->join('request_item', 'request_form.id = request_item.request_form_id');
        $this->db->join('equipment', 'request_item.equipment_id = equipment.equipment_id','left');
        $this->db->join('equipment_type', 'request_form.lab = equipment_type.id','left');
        $this->db->where('member_id', $member_id);
        $this->db->order_by('request_form.timestamp', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    function get_requestform_by_id($request_id) {
        $this->db->select("*");
        $this->db->from('request_form');
        $this->db->where('id', $request_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_items_by_id($request_id) {
        $this->db->select("equipment.name");
        $this->db->from('request_item');
        $this->db->join('equipment', 'equipment.equipment_id=request_item.equipment_id');
        $this->db->where('request_form_id', $request_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_total_status_by_member($status_id) {
        $member_id = $this->member->get_member_id();
        $this->db->select("status");
        $this->db->from('request_form');
        $this->db->where('member_id', $member_id);
        $this->db->where('status', $status_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function insert_temp_key($email) {
        $member_id = $this->member->get_id_from_email($email);
        $rand = $this->rand_char(32);
        $data = array(
            'key' => $rand,
            'member_id' => $member_id
        );
        $this->db->insert('temp_key', $data);
        return $rand;
    }

    function rand_char($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    function get_member_id_from_key($key) {
        $this->db->select('member_id');
        $this->db->from('temp_key');
        $this->db->where('key', $key);
        $query = $this->db->get();
        return $query->row()->member_id;
    }

    function delete_key($key) {
        return $this->db->delete('temp_key', array('key' => $key));
    }

    function key_exists($key) {
        $this->db->from('temp_key');
        $this->db->where('key', urldecode($key));
        $query = $this->db->get();
        return $query->num_rows() > 0;
    }

    function change_password($key, $password) {
        $member_id = $this->get_member_id_from_key($key);
        $data = array(
            'password' => md5($password)
        );
        $this->db->where('id', $member_id);
        $this->db->update('member', $data);
        $this->delete_key($key);
    }

    function change_password_me($password) {
        $data = array(
            'password' => md5($password)
        );
        $this->db->where('id', $this->member_id);
        return $this->db->update('member', $data);
    }

    function update_photo($file_name) {
        $data = array(
            'image_path' => $file_name
        );
        $this->db->where('id', $this->member_id);
        return $this->db->update('member', $data);
    }

    function is_old_password_correct($old_password) {
        $this->db->from('member');
        $this->db->where('id', $this->member_id);
        $this->db->where('password', md5($old_password));
        $query = $this->db->get();
        return $query->num_rows() > 0;
    }

}

?>