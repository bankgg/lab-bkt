<?php

class Updates_model extends CI_Model {

    public $id;
    public $filename;
    public $date;
    public $timestamp;

    public function __construct() {
        parent::__construct();
    }
    
    public function get_updates() {
        return $this->db->get('updates')->result();
    }
    
    public function add_update($data) {
        return $this->db->insert('updates', $data);
    }
}

?>