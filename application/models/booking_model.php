<?php

class Booking_model extends CI_Model {

    private $member_id = 0;

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->model('Member_model', 'member');
        $this->member_id = $CI->member->get_id_from_email($this->session->userdata('email'));
    }

    function book($data) {
        return $this->db->insert('booking', $data);
    }

    function get_labs() {
        $this->db->from('equipment_type');
        $query = $this->db->get();
        return $query->result();
    }

    function isBooked($start, $end) {
        $this->db->from('booking');
        $this->db->where('start', $start);
        $this->db->where('end', $end);
        return $this->db->count_all_results() > 0;
    }

    function get_rooms($type) {
        $this->db->from('room');
        $this->db->where('type', $type);
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_rooms() {
        $this->db->select('equipment_type.name as type_name, room.name as room_name, room.id as room_id');
        $this->db->from('equipment_type');
        $this->db->join('room', 'room.type = equipment_type.id');
        $query = $this->db->get();
        return $query->result();
    }

    function get_room_name($roomId) {
        $this->db->select('name');
        $this->db->from('room');
        $this->db->where('id', $roomId);
        $query = $this->db->get();
        return $query->row()->name;
    }

    function get_room_from_equip_name($name) {
        $query = $this->db->query('SELECT r.id,r.name FROM room r where r.id in (select  room from equipment where name like \'' . $name . '%\')');
        return $query->result();
    }

    function get_all_equipments() {
        $this->db->select('equipment_id, name');
        $this->db->from('equipment');
        $this->db->group_by('name');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_equipments_and_tag() {
        $this->db->select('equipment_id, equipment.name, room.name as room_name, tag');
        $this->db->from('equipment');
        $this->db->join('room', 'equipment.room = room.id');
        $this->db->order_by('equipment.name');
        $query = $this->db->get();
        return $query->result();
    }

    function get_all_equipments_groupby_name() {
        $this->db->select('equipment_id, name');
        $this->db->from('equipment');
        $this->db->group_by("name");
        $query = $this->db->get();
        return $query->result();
    }

    function get_equipments($room) {
        $this->db->select('equipment_id, name');
        $this->db->from('equipment');
        $this->db->where('room', $room);
        $query = $this->db->get();
        return $query->result();
    }

    function get_equipments_from_nameAndRoom($name, $room) {
        $this->db->select('equipment_id,name,tag ');
        $this->db->from('equipment');
        $this->db->where('room', $room);
        $this->db->like('name', $name, "after");
        $query = $this->db->get();
        return $query->result();
    }

    function get_equipment_from_tag($tag) {
        $this->db->select('equipment.name, equipment.equipment_id, equipment.tag, room.name as room_name');
        $this->db->from('equipment');
        $this->db->join('room', 'equipment.room = room.id');
        $this->db->where('tag', $tag);
        $query = $this->db->get();
        return $query->row();
    }

    function get_equipment_types() {
        $this->db->from('equipment_type');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    function get_equipment_type($id) {
        $this->db->from('equipment_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_initial_equipment_type() {
        $this->db->from('equipment_type');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    function get_equipment_name_from_id($id) {
        $this->db->select('name');
        $this->db->from('equipment');
        $this->db->where('equipment_id', $id);
        $query = $this->db->get();
        return $query->row()->name;
    }
    
    function get_equipment_name_from_booking_id($booking_id) {
        $this->db->select('name');
        $this->db->from('equipment');
        $this->db->join('booking', 'booking.equipment_id = equipment.equipment_id');
        $this->db->where('booking.booking_id', $booking_id);
        $query = $this->db->get();
        return $query->row()->name;
    }

    /* function get_bookings($type_id, $room, $date) {
      $this->db->select('booking.equipment_id as equipment_id, firstname, description, start, end, equipment.name as equipment_name, booking.member_id');
      $this->db->from('booking');
      $this->db->join('member', 'booking.member_id = member.id');
      $this->db->join('equipment', 'booking.equipment_id = equipment.equipment_id');
      $this->db->join('room', 'room.id = equipment.room');
      $this->db->where('room.type', $type_id);
      $this->db->where('room', $room);
      $this->db->like('start', $date, 'after');
      $this->db->like('end', $date, 'after');
      $query = $this->db->get();
      return $query->result();
      } */

    function get_booking($equipment_id, $start, $end) {
        $this->db->from('booking');
        $this->db->where('start >=', $start);
        $this->db->where('end <=', $end);
        $query = $this->db->get();
        return $query->row();
    }

    function get_booking_from_timestamp($timestamp) {
        $this->db->distinct();
        $this->db->select('member_id');
        $this->db->from('booking');
        $this->db->where('timestamp', $timestamp);
        $query = $this->db->get();
        return $query->row();
    }

    function get_bookings($equipment_id, $date) {
        $this->db->select('booking.equipment_id as equipment_id, firstname, description, cast(start as time) as start, cast(end as time) as end, booking.member_id');
        $this->db->from('booking');
        $this->db->join('member', 'booking.member_id = member.id');
        $this->db->where('booking.equipment_id', $equipment_id);
        $this->db->like('start', $date, 'after');
        $query = $this->db->get();
        return $query->result();
    }

    function get_bookings_for_log() {
        $query = $this->db->query('select equipment.equipment_id as id, equipment.name as ename, room.name as rname, cast(booking.start as date) as date, start, end from booking join equipment on booking.equipment_id = equipment.equipment_id join room on equipment.room = room.id where member_id = ' . $this->member_id . ' and start <= curdate() and not exists (select equipment_id from log_book where log_book.equipment_id = equipment.equipment_id and cast(start as date) = cast(old_end as date)) group by date, equipment.equipment_id order by booking.start');
        return $query->result();
    }

    function check_privilege($equipment_id) {
        $query = $this->db->query('select count(*) as c
from request_form
join request_item on request_form.id = request_item.request_form_id
join equipment eq on eq.equipment_id = request_item.equipment_id
where request_form.member_id = ' . $this->member_id . '
and curdate() >= start_date
and curdate() <= end_date
and status = 1
and eq.name in (select e.name from equipment e where e.equipment_id = ' . $equipment_id . ')');
        return $query->row()->c > 0;
    }

    function check_free_time($eid, $time1, $time2) {
        $this->db->from('booking');
        $this->db->where('start <=', $time1);
        $this->db->where('end >=', $time2);
        $this->db->where('equipment_id', $eid);
        $count = $this->db->count_all_results();
        return $count == 0;
    }

    function search($lab = null, $query = null) {
        $this->db->select('equipment_id, equipment.name as name, room, limit_times, limit_hours');
        $this->db->from('equipment');
        $this->db->join('room', 'room.id = equipment.room');
        $this->db->join('equipment_type', 'room.type = equipment_type.id');
        if ($query != null)
            $this->db->like('equipment.name', $query);
        if ($lab != null)
            $this->db->where('equipment_type.id', $lab);
        $this->db->group_by('name');
        $query = $this->db->get();
        return $query->result();
    }

    function cancel($equipment_id, $start, $end) {
        return $this->db->delete('booking', array('equipment_id' => $equipment_id, 'start' => $start, 'end' => $end));
    }

    function cancel_by_timestamp($timestamp) {
        return $this->db->delete('booking', array('timestamp' => $timestamp));
    }

    function get_tag_from_nameAndRoom($name, $room) {
        $this->db->select('equipment_id,tag');
        $this->db->from('equipment');
        $this->db->where('room', $room);
        $this->db->like('name', $name, 'after');
        $query = $this->db->get();
        return $query->result();
    }

    function get_booking_from_equipment($id) {
        $this->db->select('*');
        $this->db->from('booking');
        $this->db->where('equipment_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_booking_from_nameAndRoom($name, $room) {
        $query = $this->db->query('select * from booking where equipment_id in (select equipment_id from equipment where name like \'%' . $name . '%\' and room =' . $room . ') order by equipment_id,start');
        return $query->result();
    }

    function get_booking_from_nameRoomAndTag($name, $room, $tag) {
        $query = $this->db->query('select * from booking where equipment_id in (select equipment_id from equipment where name like \'%' . $name . '%\' and room =' . $room . ' and tag like \'' . $tag . '\') order by  start');
        return $query->result();
    }
    
    function delete_room($id) {
        $this->db->where('id', $id);
        $query1 = $this->db->delete('room');
        
        return $query1;
    }
    
    function delete_equipment_type($id) {
        $this->db->where('id', $id);
        $query1 = $this->db->delete('equipment_type');

        $this->db->where('type', $id);
        $query2 = $this->db->delete('room');
        
        return $query1 && $query2;
    }

}

?>