<?php

class Log_model extends CI_Model {

    private $member_id = 0;

    function __construct() {
        parent::__construct();
        $ci = & get_instance();
        $ci->load->model('Member_model', 'member');
        $this->member_id = $ci->member->get_id_from_email($this->session->userdata('email'));
    }

    function add_column($name, $thai_name, $type) {
        $t = $type == 'int' ? 'int' : $type == 'varchar' ? 'varchar(255)' : 'datetime';
        $sql = 'alter table log_book add ' . $name . ' ' . $t;
        $query = $this->db->query($sql);
        
        $data = array(
            'name' => $name,
            'thai_name' => $thai_name
        );
        $query1 = $this->db->insert('columns', $data);
        return $query && $query1;
    }

    function get_columns($booking_id) {
        $this->db->select('columns');
        $this->db->from('equipment');
        $this->db->join('booking', 'booking.equipment_id = equipment.equipment_id');
        $this->db->where('booking_id', $booking_id);
        $query = $this->db->get();
        return json_decode($query->row()->columns, true);
    }
	
	 function get_columns_from_equipment_id($equipment_id) {
        $this->db->select('columns');
        $this->db->from('equipment');
        $this->db->where('equipment_id', $equipment_id);
        $query = $this->db->get();
        return json_decode($query->row()->columns, true);
    }
	
    function get_all_columns() {
        $query = $this->db->query('SELECT COLUMN_NAME as name
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = \'bankgginth_lab\'
AND TABLE_NAME =  \'log_book\'');
        return $query->result();
    }

    function get_column_type($name) {
        $query = $this->db->query('SELECT COLUMN_TYPE as type
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = \'bankgginth_lab\'
AND TABLE_NAME =  \'log_book\' AND COLUMN_NAME = \'' . $name . '\'');
        return $query->row()->type;
    }

    function get_thai_name($name) {
        $this->db->select('thai_name');
        $this->db->from('columns');
        $this->db->where('name', $name);
        $query = $this->db->get();
        return $query->row()->thai_name;
    }

    function get_log_book($booking_id) {
        $select = '';
        $columns = $this->get_columns($booking_id);
        foreach ($columns as $column) {
            $select .= $column . ', ';
        }
        $select = rtrim($select, ',');

        $this->db->select($select);
        $this->db->from('log_book');
        $this->db->where('booking_id', $booking_id);
        $query = $this->db->get();
        return $query->row();
    }

    function get_log_books() {
        $query = $this->db->query('SELECT booking.booking_id as booking_id, equipment.name, cast(start as date) as reserved_date, date_add(cast(end as date), INTERVAL 7 day) as expected_to_record, 
case
when log_book.time_start is null then 0
else 1 end as recorded
from booking
join equipment on equipment.equipment_id = booking.equipment_id
left join log_book on log_book.booking_id = booking.booking_id
where member_id = ' . $this->member_id . '
group by booking.equipment_id, cast(start as date) ');
        return $query->result();
    }
    
    function get_log_books_from_equipment_id($equipment_id) {
        $columns = $this->get_columns_from_equipment_id($equipment_id);
        $columns_str = '';
        foreach ($columns as $c) {
            $columns_str .= $c . ', ';
        }
        $columns_str = rtrim($columns_str);
        $this->db->select($columns_str);
        $this->db->from('log_book');
        $this->db->join('booking', 'log_book.booking_id = booking.booking_id');
        $this->db->where('equipment_id', $equipment_id);
        $query = $this->db->get();
        return $query->result();
    }

    function update_columns($equipment_id, $columns) {
        $data = array(
            'columns' => json_encode($columns)
        );
        $this->db->where('equipment_id', $equipment_id);
        return $this->db->update('equipment', $data);
    }

    function add_log($data) {
        return $this->db->insert('log_book', $data);
    }

    function edit_log($data, $booking_id) {
        $this->db->where('booking_id', $booking_id);
        return $this->db->update('log_book', $data);
    }
    
    function delete_column($name) {
        $query = $this->db->query("ALTER TABLE log_book DROP " . $name . "");
        $this->db->where('name', $name);
        $query1 = $this->db->delete('columns');
        $query3 = true;
        
        $this->db->select('equipment_id, columns');
        $this->db->from('equipment');
        $this->db->like('columns', '"' . $name . '"');
        $query2 = $this->db->get();
        
        foreach ($query2->result() as $q) {
            $arr = json_decode($q->columns);
            $index = array_search($name, $arr);
            unset($arr[$index]);
            $data = array(
                'columns' => json_encode($arr)
            );
            $this->db->where('equipment_id', $q->equipment_id);
            $query3 = $query3 && $this->db->update('equipment', $data);
        }
        return $query && $query1 && $query3;
    }

}

?>