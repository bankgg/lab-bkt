<?php
class Subscribe_model extends CI_Model {
	function __construct() {
        parent::__construct();   
    }
    
    function add_subscribe($data) {
		$result = $this->db->insert('subscribe', $data );   
		return $result;
    }
	function remove_subscribe($email){ 
		$this->db->delete('subscribe', array('email'=>$email));
		return $this->db->affected_rows(); 
	} 
	function get_all_subscriber(){
	 	$this->db->select('email');
        $this->db->from('subscribe');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function check_subscribe($email){
		$this->db->select('email');
        $this->db->from('subscribe');
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->num_rows();
		
	}
} 
?>