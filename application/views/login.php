<link rel="stylesheet" href="<?php echo base_url() . 'module/loadover/loadover.css'; ?>" />
<div class="warper">
    <div class="row" id="form_login">
        <div class="span6 offset3 content"  onSubmit="return login()">

            <div id="login_error" style="display: none; color: red; text-align: center">อีเมล์/รหัสผ่าน ผิดพลาด!</div><br />
            <form class="form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="login-inputEmail">อีเมล</label>
                    <div class="controls">
                        <input type="text" class="span3" id="login-inputEmail" placeholder="Email">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="login-inputPassword">รหัสผ่าน</label>
                    <div class="controls">
                        <input type="password" class="span3" id="login-inputPassword" placeholder="Password"><br /><br />
                        <a href="javascript:;" id="forget"><span id="forget-text">ลืมรหัสผ่าน?</span></a>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-main">เข้าสู่ระบบ</button>
                        <a class="btn btn-sup regisShow" id="regisShow" onClick="click_regis()">สมัครสมาชิก</a>
                    </div>
                </div>
            </form>
        </div> <!-- End form_login -->
    </div> <!-- End row -->
</div>
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/bootbox.min.js'; ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'module/loadover/loadover.js'; ?>"></script>
<script type="text/javascript">
                            $('#forget').click(function() {
                                bootbox.prompt("กรุณากรอกที่อยู่อีเมล์ของคุณ", function(result) {
                                    if (result == '') {
                                        $.pnotify({
                                            title: 'การดำเนินการ',
                                            text: 'คุณยังไม่ได้กรอกที่อยู่อีเมล์!',
                                            type: 'error'
                                        });
                                        return false;
                                    } else if (result === null) {
                                    } else {
                                        $('body').loadOverStart();
                                        $.post('/member/send_email_forget', {email: result}, function(data) {
                                            $('body').loadOverStop();
                                            $.pnotify({
                                                title: 'การดำเนินการ',
                                                text: 'การดำเนินการสำเร็จ! ระบบจะส่งอีเมล์พร้อมกับลิ้งค์เพื่อสร้างรหัสผ่านใหม่ไปที่อีเมล์ของคุณ',
                                                delay: 5000,
                                                type: 'success'
                                            });
                                        });
                                    }
                                });
                            });
</script>