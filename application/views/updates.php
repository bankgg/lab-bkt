<link rel="stylesheet" href="<?php echo base_url() . 'module/DataTables/css/demo_table.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'asset/css/jasny-bootstrap.min.css'; ?>" />
<div class="warper">
    <div class="row">
        <div class="span8 offset2">
            <div class="content" style="padding-top: 0">
                <div class="title"><center>รายการไฟล์อัพเดทความเคลื่อนไหว</center></div>
                <table class="table" id="updates-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>วันที่นัดคุย</th>
                            <th>ดาวน์โหลด</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($updates as $u) {
                            echo '<tr>';
                            echo '<td>' . $u->id . '</td>';
                            echo '<td>' . $u->date . '</td>';
                            echo '<td><a href="' . (base_url() . 'updates/' . $u->filename) . '"><button class="btn btn-main" type="button">ดาวน์โหลด</button></a></td>';
                            echo '</tr>';
                        }

                        if (count($updates) == 0) {
                            echo '<tr>';
                            echo '<td style="text-align: center; color: red" colspan="3">(ไม่มีข้อมูล)</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
                <br />
                <hr/>
                <div class="row">
                    <div class="span6 offset1">
                        <button class="btn btn-main btn-block" id="add-btn">เพิ่มไฟล์</button>
                      
                        <?php echo form_open_multipart('admin/add_update', array('class' => 'form-horizontal', 'style' => 'margin-top: 20px', 'id' => 'add-file')); ?>
                                <div class="control-group">
                                	<label class="control-label" for="date">ไฟล์</label>
                                    <div class="controls" >
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group">
                                            <div class="form-control uneditable-input span3" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">เลือกไฟล์</span><span class="fileinput-exists">เปลี่ยน</span><input type="file" name="userfile"></span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">ลบ</a>
                                        </div>
                                    </div>
                                    </div> 
                                </div>
                         
                            <div class="control-group">
                                <label class="control-label" for="date">วันที่นัดคุย</label>
                                <div class="controls" >
                                    <input type="date" class="form-control" id="date" name="date">
                                </div>
                            </div> 
                            <div style="margin:0 auto; width:80%"> 
                                <button class="btn btn-main" type="submit" style="width:54%">เพิ่ม</button>&nbsp; 
                                <a class="btn " href="javascript:;" id="cancel-btn">ยกเลิก</a> 
                            </div>
                        </form>
                    </div>
                </div>
            </div><!--end content-->
        </div><!-- end span -->
    </div>
</div>
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'module/DataTables/js/jquery.dataTables.js'; ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'asset/js/jasny-bootstrap.min.js'; ?>"></script> 
<script>
                                    $(function() {
                                        $('#add-file').hide();
                                        
                                        $('#add-btn').click(function() {
                                           $(this).slideToggle();
                                           $('#add-file').slideToggle();
                                        });
                                        
                                        $('#cancel-btn').click(function() {
                                           $('#add-file').slideToggle();
                                           $('#add-btn').slideDown();
                                        });
                                        
                                        if ($('#updates-table tbody tr').length > 1) {
                                            $('#updates-table').dataTable({
                                                "bPaginate": true,
                                                "bLengthChange": true,
                                                "bFilter": true,
                                                "bSort": true,
                                                "bInfo": true,
                                                "bAutoWidth": true,
                                                'iDisplayLength': 10,
                                                "aaSorting": [[1, "desc"]]
                                            });
                                        }

                                        $('#priviledge tbody').on("click", "tr[class!='acceptMenu']", function() {
                                            var id = $(this).attr("data-id");
                                            var html = '<tr class="acceptMenu"><td colspan="7"><button class="btn pull-right span1" style="margin-left: 10px" onClick="cancel(\'' + id + '\')">ไม่ให้สิทธิ์</button><button class="btn btn-main span2 pull-right" onClick="approve(\'' + id + '\')"><i class="icon-ok"></i> ยืนยันสิทธิ์</button></td></tr>';
                                            if (!$(this).next().hasClass("acceptMenu")) {
                                                $(html).insertAfter(this);
                                            } else {
                                                $(this).next().remove();
                                            }
                                        });

                                        $('#approved_list tbody').on("click", "tr[class!='cancelMenu']", function() {
                                            var id = $(this).attr("data-id");
                                            var html = '<tr class="cancelMenu"><td colspan="6"><button class="btn pull-right span1" style="margin-left: 10px" onClick="unapprove(\'' + id + '\')">ถอนสิทธิ์</button></td></tr>';
                                            if (!$(this).next().hasClass("cancelMenu")) {
                                                $(html).insertAfter(this);
                                            } else {
                                                $(this).next().remove();
                                            }
                                        });

                                        $('#select_approved_list').change(function(e) {
                                            loadApproved($(this).val());
                                        });


                                    });
</script>