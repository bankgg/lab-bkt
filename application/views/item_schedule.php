<link rel='stylesheet' type='text/css' href="<?php echo base_url() . 'module/full-calendar/fullcalendar.css'; ?>" />
<style>
.fc-day:hover {
	cursor: pointer;
	background-color: #FFF;
}
.fc-widget-content:hover, .fc-event-inner:hover {
	cursor: pointer;
}
.label-warning {
	font-size: 15px;
	padding: 5px;
	margin-left: 8px;
	position: absolute;
}
.fc-today {
	background: white;
}
</style>
<div class="warper">
  <div class="content" style="padding-top:0">
    <div class="page-title">เลือกอุปกรณ์ที่ต้องการ</div>
    <div class="row" >
      <div class="span10 offset1" id="filterSelected">
        <div class="span3">
          <div class="filterTitle"><span class="circle-icon">1</span>&nbsp;&nbsp;อุปกรณ์</div>
          <select size="7" id="selectEquip">
            <?php
                        for ($i = 0; $i < count($equipments); $i++) {
                            echo '<option value="' . $equipments[$i]->name . '" ' . ($equipments[$i]->name == rawurldecode($name) ? 'selected' : '') . '>' . $equipments[$i]->name . '</option>';
                        }
                        ?>
          </select>
          <form class="form-search">
              <div class="input-append">
                <input type="text" class="span3 search-query" placeholder="ตัวคัดกรอง" id="equipment_filter">
                <a href="javascript:;" class="btn btn-main" style="font-size:11px"><i class="icon icon-search"></i></a>
              </div> 
          </form> 
        </div>
        <div class="span3">
          <div class="filterTitle"><span class="circle-icon">2</span>&nbsp;&nbsp;ห้อง</div>
          <select size="7" disabled="disabled" id="selectRoom">
          </select>
        </div>
        <div class="span3">
          <div class="filterTitle"><span class="circle-icon">3</span>&nbsp;&nbsp;เลขเครื่อง</div>
          <select size="7" disabled="disabled" id="selectTag">
          </select>
          <span class="text-error pull-right">*กรุณาเลือกอย่างน้อยสองตัว</span> </div>
      </div>
    </div>
    <div id="calendar_list"> </div>
    <div id="temp"></div>
    <input type="hidden" id="name" value="<?php echo $name; ?>" />
    <input type="hidden" id="room" value="<?php echo $room; ?>" />
    <input type="hidden" id="tag" value="<?php echo $tag; ?>" />
    <input type="hidden" id="roomName" value="" />
    <!--<div class="row">
                <div class="span12 calendarBox">
                        <div class="span10 offset1">
                        <div class="page-header">
                        <center><i class="icon-chevron-down"></i> Cell Culture Vessels ห้อง A01</center>
                    </div>	
                        <div class='calendar'></div>
                </div>
            </div>
        </div>
        <div class="row">
                <div class="span12 calendarBox">
                        <div class="span10 offset1">
                        <div class="page-header">
                        <center><i class="icon-chevron-down"></i> Cell Culture Vessels ห้อง A01</center>
                    </div>	
                        <div class='calendar'></div>
                </div>
            </div>
        </div>--> 
  </div>
</div>
<script type='text/javascript' src="<?php echo base_url() . 'module/full-calendar/fullcalendar.min.js'; ?>"></script> 
<script>
    $(document).ready(function() {
        if ($('#name').val().length > 0) {
            if ($('#room').val().length > 0) {
                equip($('#name').val(), $('#room').val());
                room($('#room').val());
                tag($('#name').val(), $('#room').val(), $('#tag').val());
            } else {
                equip($('#name').val(), '');
            }
        }

        $("#selectEquip").change(function(e) {
            var name = $(this).val();
            equip(name, '');
        });

        $("#selectRoom").change(function(e) {
            var roomid = $(this).val();
            room(roomid);
        });

        $("#selectTag").change(function(e) {
            tag($('#selectEquip').val(), $('#selectRoom').val(), $('#selectTag').val());
        });
	
		
		jQuery.fn.filterByText = function(textbox, selectSingleMatch) {
			return this.each(function() {
				var select = this;
				var options = [];
				$(select).find('option').each(function() {
					options.push({value: $(this).val(), text: $(this).text()});
				});
				$(select).data('options', options);
				$(textbox).bind('change keyup', function() {
					var options = $(select).empty().data('options');
					var search = $(this).val().trim();
					var regex = new RegExp(search,"gi");
				  
					$.each(options, function(i) {
						var option = options[i];
						if(option.text.match(regex) !== null) {
							$(select).append(
							   $('<option>').text(option.text).val(option.value)
							);
						}
					});
					if (selectSingleMatch === true && $(select).children().length === 1) {
						$(select).children().get(0).selected = true;
					}
				});            
			});
		};
		
		  $('#selectEquip').filterByText($('#equipment_filter'), true);
    });

    function equip(name, roomid) {
        $.ajax({
            type: 'POST',
            url: '/member/findRoomFromEquipName',
            data: {name: name},
            success: function(data) {
                var json = JSON.parse(data);
                var html = "";
                var roomNameId = 0;
                for (var i = 0; i < json.room.length; i++) {
                    var id = json.room[i].id;
                    var name = json.room[i].name;
                    html += '<option value="' + id + '" ' + (roomid == id ? 'selected' : '') + '>' + name + '</option>';
                    if (roomid == id)
                        roomNameId = i;
                }
                $('#roomName').val(json.room[roomNameId].name);
                $("#selectRoom").removeAttr("disabled").html(html);

            }
        });
        $("#selectTag").attr("disabled", "disabled").html("");
    }

    function room(roomid) {
        var name = $("#selectEquip").val();
        var room_name = $("#selectRoom").find("option[value=" + roomid + "]").text();

        //เมื่อมีการเลือกให้ไปหา Tag ตาม ขื่ออุปกรณ์และ room ออกมา
        $.ajax({
            type: 'POST',
            url: '/member/findTagFromRoom',
            data: {name: name, room: roomid},
            success: function(data) {
                var json = JSON.parse(data);
                var html = "";
                for (var i = 0; i < json.tag.length; i++) {
                    var tag = json.tag[i].tag;
                    html += '<option value="' + tag + '" ' + ($('#tag').val() == tag ? 'selected' : '') + '>' + tag + '</option>';
                }
                $("#selectTag").removeAttr("disabled").html(html);

            }
        });

        //เมื่อได้ Tag จากนั้นจะไปหา equipments ตาม ชื่ออุปกรณ์ และ room เพื่อนำไปสร้างเป็นปฎิทินของแต่ละอุปกรณ์
        $.ajax({
            type: 'POST',
            url: '/member/find_equipments_from_nameAndRoom',
            data: {name: name, room: roomid},
			beforeSend  :function(){
				 var loading = '<div class="span4 offset4" id="loading" style="font-weight: bold; text-align: center"><center><i class="icon-spinner icon-spin icon-4x"></i></center><br>กำลังโหลดข้อมูล...</div> <!-- End success -->';
    			$('#calendar_list').html(loading);
			},
            success: function(data) {
                //สร้าง calendar_class ไว้เก็บชื่อ calendar ที่มีทั้งหมดของอุปกรณ์ เพื่อนำชื่อไปใช้ใส่ข้อมูลตาม calendar
                var calendar_class = [];
                //เก็บ equipment_id ทั้งหมดลง array 
                var equipment_id = [];
                var json = JSON.parse(data);
                var html = "";
				var tag = [];
                for (var i = 0; i < json.equipments.length; i++) {
                    var id = json.equipments[i].equipment_id;
                    var name = json.equipments[i].name;
                    html += '<div class="row"><div class="span12 calendarBox"><div class="header-calender"><center><i class="icon-chevron-down"></i> <span>' + name + ' - ' + room_name + '</span><span class="label label-warning">' + json.equipments[i].tag + '</span></center></div><div class="span10 offset1"><div class="calendar' + i + '"></div></div></div></div>';
                    calendar_class.push("calendar" + i);
                    equipment_id.push(id);					
                    tag.push(json.equipments[i].tag);
                }

                //เขียน calendar ลงบนเว็บ
                $("#calendar_list").html(html);

                //เมื่อได้ calendar ว่างๆแล้ว ไปหาข้อมูล booking ตาม name และ room ออกมา
                $.ajax({
                    type: 'POST',
                    url: '/member/find_booking_from_nameAndRoom',
                    data: {name: name, room: roomid},
                    success: function(data) {
						var json = JSON.parse(data);
                        //json_of_each_equip เป็นข้อมูล booking ของแต่ละอุปกรณ์
                        var json_of_each_equip = [];
                        //loop ตามจำนวน calendar ที่มีอยุ่บนหน้าเว็บ

                        var d, m, y, h, mi, d2, m2, y2, h2, mi2;
                        var startDate, endDate;
                        var member_id,desc,time;
                        for (var s = 0; s < calendar_class.length; s++) {
                            //เคลีย json_of_each_equip ทุกรอบเมื่อวนรอบ ปฎิทินใหม่
                            json_of_each_equip = [];
                            //loop ตามจำนวนข้อมูล booking ที่มีทั้งหมดเพื่อนำข้อมูลการจองไปลงใน json_of_each_equip
                            for (var n = 0; n < json.booking.length; n++) {
                                if (equipment_id[s] == json.booking[n].equipment_id) {
                                    // Split timestamp into [ Y, M, D, h, m, s ]
                                    var t = (json.booking[n].start).split(/[- :]/);
                                    // Apply each element to the Date function
                                    var date = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                    d = date.getDate();
                                    m = date.getMonth();
                                    y = date.getFullYear();
                                    h = date.getHours();
                                    mi = date.getMinutes();

                                    var t2 = (json.booking[n].end).split(/[- :]/);
                                    // Apply each element to the Date function
                                    date2 = new Date(t2[0], t2[1] - 1, t2[2], t2[3], t2[4], t2[5]);
                                    d2 = date2.getDate();
                                    m2 = date2.getMonth();
                                    y2 = date2.getFullYear();
                                    h2 = date2.getHours();
                                    mi2 = date2.getMinutes();

                                    if (startDate == null || endDate == null) {
                                        startDate = new Date(y, m, d, h, mi);
                                        endDate = new Date(y2, m2, d2, h2, mi2);
                                        member_id = json.booking[n].member_id;
										desc  = json.booking[n].description;
										time = json.booking[n].timestamp;
                                    }

                                    var tmpStartDate = new Date(y, m, d, h, mi);
                                    var tmpEndDate	 = new Date(y2, m2, d2, h2, mi2);
                                    var tmpMember 	= json.booking[n].member_id;
									var tmpDesc 	= json.booking[n].description;
									var tmpTime		=  json.booking[n].timestamp;
									if (endDate - tmpEndDate != 0) {
										if (tmpEndDate - endDate == 1800000 && member_id == tmpMember && time == tmpTime) {
											endDate = new Date(y2, m2, d2, h2, mi2);
											if (n == json.booking.length - 1) {
												json_of_each_equip.push({title: desc, start: startDate, end: endDate, allDay: false, editable: false});
											}
										} else if (n == json.booking.length - 1 && time != tmpTime) {
											json_of_each_equip.push({title: desc, start: startDate, end: endDate, allDay: false, editable: false});
											json_of_each_equip.push({title: desc, start: tmpStartDate, end: tmpEndDate, allDay: false, editable: false});
										} else {
											json_of_each_equip.push({title: desc, start: startDate, end: endDate, allDay: false, editable: false});
											startDate = tmpStartDate;
											endDate = tmpEndDate;
											member_id = tmpMember;
											desc = tmpDesc;
											time=tmpTime;
										}
									}
                                }
                            }
                            //ทำการ add event ลงบน calendar;
                            fullcalendar(calendar_class[s], json_of_each_equip, tag[s]);
                        }
                    }
                });

            }
        });
    }

    function tag(name, roomid, tag) {
        //alert(tag)
        $.ajax({
            type: 'POST',
            url: '/member/find_booking_from_nameRoomAndTag',
            data: {name: name, room: roomid, tag: tag},
			beforeSend  :function(){
				 var loading = '<div class="span4 offset4" id="loading" style="font-weight: bold; text-align: center"><center><i class="icon-spinner icon-spin icon-4x"></i></center><br>กำลังโหลดข้อมูล...</div> <!-- End success -->';
    			$('#calendar_list').html(loading);
			},
            success: function(data) {
				var json = JSON.parse(data);
                var calendar_class = "calendar" + roomid;
                var equipment_name = json.equipments.name;
                var html = '<div class="row"><div class="span12 calendarBox"><div class="header-calender"><center><i class="icon-chevron-down"></i> ' + equipment_name + " - " + $('#roomName').val() + '<span class="label label-warning">' + tag + '</span></center></div><div class="span10 offset1"><div class="' + calendar_class + '"></div></div></div></div>'; 
                //เขียน calendar ลงบนเว็บ
                $("#calendar_list").html(html);
                var json_event = [];
                //loop ตามจำนวนข้อมูล booking ที่มีทั้งหมดเพื่อนำข้อมูลการจองไปลงใน json_of_each_equip

                var d, m, y, h, mi, d2, m2, y2, h2, mi2;
                var startDate, endDate;
                var member_id,desc,time ;
				//alert(JSON.stringify(json));
                for (var n = 0; n < json.booking.length; n++) {
                    // Split timestamp into [ Y, M, D, h, m, s ]

                    var t = (json.booking[n].start).split(/[- :]/);
                    // Apply each element to the Date function
                    var date = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                    d = date.getDate();
                    m = date.getMonth();
                    y = date.getFullYear();
                    h = date.getHours();
                    mi = date.getMinutes();


                    var t2 = (json.booking[n].end).split(/[- :]/);
                    // Apply each element to the Date function
                    var date2 = new Date(t2[0], t2[1] - 1, t2[2], t2[3], t2[4], t2[5]);
                    d2 = date2.getDate();
                    m2 = date2.getMonth();
                    y2 = date2.getFullYear();
                    h2 = date2.getHours();
                    mi2 = date2.getMinutes();

                    if (startDate == null || endDate == null) {
                        startDate = new Date(y, m, d, h, mi);
                        endDate = new Date(y2, m2, d2, h2, mi2);
                        member_id = json.booking[n].member_id;
                    	desc  = json.booking[n].description;
						time = json.booking[n].timestamp;
					}

                    var tmpStartDate = new Date(y, m, d, h, mi);
                    var tmpEndDate = new Date(y2, m2, d2, h2, mi2);
                    var tmpMember = json.booking[n].member_id;
					var tmpDesc = json.booking[n].description;
					var tmpTime=  json.booking[n].timestamp;
					//alert(time + " : " + tmpTime);
                    if (endDate - tmpEndDate != 0) {
                        if (tmpEndDate - endDate == 1800000 && member_id == tmpMember && time == tmpTime) {
                            endDate = new Date(y2, m2, d2, h2, mi2);
							if (n == json.booking.length - 1) {
								json_event.push({title: desc, start: startDate, end: endDate, allDay: false, editable: false});
                            }
                        } else if (n == json.booking.length - 1 && time != tmpTime) {
							json_event.push({title: desc, start: startDate, end: endDate, allDay: false, editable: false});
                           	json_event.push({title: desc, start: tmpStartDate, end: tmpEndDate, allDay: false, editable: false});
                        } else {
							json_event.push({title: desc, start: startDate, end: endDate, allDay: false, editable: false});
                            startDate = tmpStartDate;
                            endDate = tmpEndDate;
                            member_id = tmpMember;
							desc = tmpDesc;
							time=tmpTime;
                        }
                    }
				}
				
                //ทำการ add event ลงบน calendar;
                fullcalendar(calendar_class, json_event, $('#selectTag').val());
            }
        });
    }

    function fullcalendar(calendarid, json_of_each_equip, tag) { 
		//alert(JSON.stringify(json_of_each_equip));
        $("." + calendarid).fullCalendar({
            dayClick: function(date, allDay, jsEvent, view) {
                date = $.fullCalendar.formatDate(date, "dd-MM-yyyy");
                click_booking(date, $("#selectRoom").val(), tag);
            },
            eventClick: function(calEvent, jsEvent, view) {
                date = $.fullCalendar.formatDate(calEvent.start, "dd-MM-yyyy");
                click_booking(date, $("#selectRoom").val(), tag);
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek'
            },
            editable: true,
            allDaySlot: false,
            contentHeight: 400,
            events: json_of_each_equip,
			/*eventRender: function(event, element) {
				element.qtip({
					content: event.description
				});
			},*/
			eventAfterRender: function(event, $el, view ) {
				var formattedTime = $.fullCalendar.formatDates(event.start, event.end, "HH:mm { - HH:mm}");
				// If FullCalendar has removed the title div, then add the title to the time div like FullCalendar would do
				if($el.find(".fc-event-title").length === 0) {
					$el.find(".fc-event-time").text(formattedTime + " - " + event.title);
				}
				else {
					$el.find(".fc-event-time").text(formattedTime);
				} 
				$(".fc-content").each(function() {
                    if($(this).find(".fc-view-month").attr("class")=="fc-view fc-view-month fc-grid"){
						$(this).find(".fc-event-title").text("");		
					}
                });
				
			}
        });

    }



</script> 
