<div class="warper">
  <div class="row" id="form_register">
    <div class="span10 offset1 content" style="padding-top: 0px;">
      <form class="form-horizontal" onSubmit="return register()">
        <legend class="page-title" style="padding:5px 0 "><span style="margin-left:20px">สมัครสมาชิก</span></legend>
        <div class="row">
          <div class="span5">
            <fieldset>
              <legend class="page-header"><center>ข้อมูลทั่วไป</center></legend>
              <div class="control-group">
                <label class="control-label" for="regis-inputFirstname">ชื่อ</label>
                <div class="controls">
                  <input type="text"  id="regis-inputFirstname" placeholder="Firstname">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-inputLastname">นามสกุล</label>
                <div class="controls">
                  <input type="text"  id="regis-inputLastname" placeholder="Lastname">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-inputEmail">อีเมล</label>
                <div class="controls">
                  <input type="email"  id="regis-inputEmail" placeholder="Email" onKeyUp="checkEmail()">
                  <span id="error-email" class="error"></span> </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-inputPassword">รหัสผ่าน</label>
                <div class="controls">
                  <input type="password"  id="regis-inputPassword" placeholder="Password">
                  <br>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-inputConPassword">ยืนยันรหัสผ่าน</label>
                <div class="controls">
                  <input type="password"  id="regis-inputConPassword" style="margin-bottom: 10px;" placeholder="Comfirm Password" onKeyUp="confirmPassword()">
                  <br>
                  <span id="error-password" class="text-error"></span> </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-tel">เบอร์โทรศัพท์</label>
                <div class="controls">
                  <input type="text" class="form-control bfh-phone" data-format="(ddd)ddd-dddd"  id="regis-tel" placeholder="Tel no.">
                  <br>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-tel_in">เบอร์โทรศัพท์ (ภายใน)</label>
                <div class="controls">
                  <input type="text"  id="regis-tel_in" placeholder="Tel no. (Ext.)">
                  <br>
                </div>
              </div>
            </fieldset>
          </div>
          <div class="span5">
            <fieldset> 
              <legend class="page-header"><center>ข้อมูลการศึกษา</center></legend>
              <div class="control-group">
                <label class="control-label" for="regis-status">สถานภาพ</label>
                <div class="controls">
                  <input type="radio" id="regis-status-student" name="regis-status" value="นักศึกษา" />
                  นักศึกษา
                  <input type="radio" id="regis-status-researcher" name="regis-status" value="นักวิจัย" />
                  นักวิจัย </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-student_id">รหัสนักศึกษา (ถ้ามี)</label>
                <div class="controls">
                  <input type="text"  id="regis-student_id" placeholder="Student ID (OPT)">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-university">มหาวิทยาลัย</label>
                <div class="controls">
                  <input type="text"  id="regis-university" placeholder="University">
                  <br>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-faculty">คณะ</label>
                <div class="controls">
                  <input type="text"  id="regis-faculty"  placeholder="Faculty" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-course" >หลักสูตร(สังกัด)</label>
                <div class="controls">
                  <input type="text"  id="regis-course" placeholder="Course">
                  <br>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="regis-lab" >สังกัดกลุ่มวิจัย (ห้องปฎิบัติการ)</label>
                <div class="controls">
                  <select id="regis-lab" >
                    <option value="no">-- โปรดเลือกสังกัดกลุ่มวิจัย --</option>
                    <?php
                                        foreach ($labs as $lab) {
                                            echo '<option value="' . $lab->id . '">' . $lab->name . '</option>';
                                        }
                                        ?>
                  </select>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="span2 offset4">
            <button type="submit" class="btn btn-main" id="btn-register">สมัครสมาชิก</button>
            <button type="button" id="loginShow" class="btn btn-sup loginShow" onClick="click_login()">ยกเลิก</button>
          </div>
        </div>
      </form>
    </div>
    <!-- End form_register --> 
  </div>
  <!-- End row --> 
</div>
<script type="text/javascript" src="<?php echo base_url() . '/js/bootstrap-formhelpers-phone.js'; ?>"></script> 
<script type="text/javascript">
    $(function() {
        $('#regis-status-student').click(function() {
            $('#regis-student_id').prop('readonly', false);
        });

        $('#regis-status-researcher').click(function() {
            $('#regis-student_id').prop('readonly', true);
        });
    });
</script>