<link rel="stylesheet" href="<?php echo base_url() . 'module/bootstrap-datetime/css/bootstrap-datetimepicker.min.css'; ?>" />
<div class="warper" >
    <div class="span8 offset2">
        <div class="content">
            <div class="row">
                <div class="span12 page-header" > <a href="javascript:;" class="back" onClick=" loadPage('/member/me/2')" style="text-decoration: none"><i class="icon-arrow-left "></i> กลับ</a> &nbsp;บันทึกการทดลอง - <?php echo $equipment_name; ?> </div> 
            </div>
            <div class="row marginTop30">
                <div class="span4 offset2">
                    <form  id="logbook_form" action="#"> 


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div> 
<input type="hidden" id="edit" value="<?php echo $edit; ?>" />
<input type="hidden" id="booking_id" value="<?php echo $booking_id; ?>" />
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . 'module/bootstrap-datetime/js/bootstrap-datetimepicker.min.js'; ?>"></script> 
<script>
                    $(function() {
                        $.getJSON("/member/get_form/" + $('#booking_id').val(), function(data) {
                            var keys = [];
                            var html = '';
                            $.each(data, function(key, val) {
                                if (val.type == 'datetime') {
                                    html += '<label class="control-label">' + val.thai_name + '</label><div class="input-append date" id="' + key + '" ><input type="text" data-format="yyyy/MM/dd hh:mm" class="input-block-level" name="' + key + '"><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div>';
                                } else {
                                    html += '<label class="control-label">' + val.thai_name + '</label><input type="' + (val.type == 'varchar' ? 'text' : 'number') + '" class="input-block-level" name="' + key + '">';
                                }
                                keys.push(key);
                            });
                            html += '<input type="hidden" value="<?php echo $booking_id; ?>" name="booking_id"><div><button type="button"  onclick="save_log()" class="btn btn-main">บันทึก</button>&nbsp;&nbsp; <a class="btn" href="javascript:;"  onClick="loadPage(\'/member/me/2\')" style="text-decoration: none">ยกเลิก</a></div>';
                            $("#logbook_form").html(html);
                            $('div.input-append').datetimepicker({
                                language: 'pt-BR',
                                pickSeconds: false
                            });

                            if ($('#edit').val() == 'true')
                                insert_data(keys);
                        });
                    });

                    function insert_data(keys) {
                        keys.forEach(function(value) {
                            $.get('/member/get_form/' + $('#booking_id').val() + '/' + value, function(data) {
                                var attr = $('input[name="' + value + '"]').attr('data-format');
                                if (typeof attr !== 'undefined' && attr !== false) {
                                    $('input[name="' + value + '"]').val(data.substring(0, data.length - 3));
                                } else {
                                    $('input[name="' + value + '"]').val(data);
                                }
                            });
                        });
                    }

                    function save_log() {
                        if (new Date(Date.parse($('input[name="time_start"]').val())).getTime() >=
                                new Date(Date.parse($('input[name="time_end"]').val())).getTime() ||
                                new Date(Date.parse($('input[name="time_start"]').val())).getTime() >= new Date().getTime()) {
                            $.pnotify({
                                title: 'การแจ้งเตือน',
                                text: 'วัน/เวลาการใช้งานไม่ถูกต้อง!',
                                type: 'error'
                            });
                            return false;
                        }
                        $.post($('#edit').val() == 'true' ? "/member/add_log/edit" : '/member/add_log', $("#logbook_form").serialize())
                                .done(function(data) {
                                    loadPage('/member/me/2');
                                });
                    }


</script> 
