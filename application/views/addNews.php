<!-- addNews-->
<link rel="stylesheet" href="<?php echo base_url() . 'module/DataTables/css/demo_table.css'; ?>" /> 
<link rel="stylesheet" href="<?php echo base_url() . 'module/loadover/loadover.css'; ?>" />
<div class="warper" > 	
    <div class="content" id="news_content" style="padding-top:0;padding-bottom:0">
        <div class="title"><center>ข่าว</center></div>

        <div class="row" >
            <div class="span10 offset1" style="margin-top:20px;margin-bottom:20px">
                <table class="table" style="margin-bottom:0" id="news-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>หัวข้อ</th>
                            <th>วัน/เวลาที่ลง</th>
                        </tr>
                    </thead>
                    <tbody id="news-list">
                        <?php
                        $i = 1;
                        foreach ($all_news as $news) {
                            echo '<tr data-id="' . $news->id . '">';
                            echo '<td>' . $i++ . '</td>';
                            echo '<td>' . $news->title . '</td>';
                            echo '<td>' . $news->timestamp . '</td>';
                            echo '</tr>';
                        }

                        if (count($all_news) == 0) {
                            echo '<tr>';
                            echo '<td style="text-align: center; color: red" colspan="5">(ยังไม่มีข่าว)</td>';
                            echo '</tr>';
                        }
                        ?>


                    </tbody>
                </table>
                <center id="loading" class="hide"><i class="icon-spinner icon-spin icon-4x"></i></center>
                <div class="row">
                    <!--<div class="span1 offset4">
                    <?php
                    if ($count_news > 10) {
                        echo '<button onclick="appendNews()" class="btn btn-main">ดูเพิ่ม</button>';
                    }
                    ?>
                </div>-->
                    <div class="span6 offset2" style="text-align: center">
                        <button class="btn  btn-main" onclick="showAddForm()"  > เพิ่มข่าว</button>
                    </div>
                </div>

            </div>
        </div>

        <!--<div class="row">
            <div class="span12" id="addMenu" onclick="showAddForm()">
                    <center><p >เพิ่มข่าว</p></center>
            </div>
        </div>-->

        <div class="row" id="addForm">

            <div class="span12" style="padding-bottom:0">

                <form class="form-horizontal well" style="margin-bottom:0" onSubmit="return submitNews();">
                    <div class="control-group">
                        <label class="control-label" for="news_title">หัวข้อ</label>
                        <div class="controls" >
                            <input type="text" class="span5" name="news_title" id="news_title"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="banner">แบนเนอร์</label>
                        <div class="controls">
                            <div class="input-append">
                                <button class="btn" type="button" id="banner">เลือกรูป</button>
                            </div>
                        </div>
                    </div>
                    <div class="control-group" id="ct">
                        <label class="control-label" for="caption">คำบรรยายใต้ภาพ</label>
                        <div class="controls">
                            <div class="input-append">
                                <input type="text" class="span5" name="caption" id="caption"/>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="news_content">เนื้อหา</label>
                        <div class="controls" >
                            <textarea rows="12" name="news_content" class="span7"></textarea>
                        </div>
                    </div>   
                    <div class="control-group">
                        <div class="controls" id="addNews" >
                            <input type="submit" class="btn btn-main span3"  value="เพิ่ม!">
                        </div> 
                    </div>
                    <input type="hidden" name="member_id" id="member_id" value="<?php echo $member_id; ?>" />
                </form> 
            </div>
        </div>
    </div>

</div>
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'module/DataTables/js/jquery.dataTables.js'; ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'module/loadover/loadover.js'; ?>"></script>
<script type="text/javascript">
                    var newsPos = 10;

                    $(document).ready(function(e) {
                        $('#addForm').hide();

                        tinymce.init({
                            selector: "textarea",
                            theme: "modern",
                            skin: "light",
                            height: 300,
                            plugins: [
                                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                "emoticons paste textcolor jbimages"
                            ],
                            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",
                            toolbar2: "print preview media | forecolor backcolor emoticons",
                            image_advtab: true
                        });

                        // แสดงช่อง caption เมื่อเลือก รูปแต่อันนี้ทำให้เมื่อคลิกแล้วแสดงช่องอย่างเดียว
                        $('#ct').hide();
                        $('#banner').click(function(e) {
                            $('#ct').slideDown();
                        }); 

                        if ($('#news-table tbody tr').length >= 1) {
                            $('#news-table').dataTable({
                                "bPaginate": true,
                                "bLengthChange": false,
                                "bFilter": true,
                                "bSort": true,
                                "bInfo": true,
                                "bAutoWidth": true,
                                'iDisplayLength': 10,
                            });
                            append_button_news();
						}
						
						$('#news-table_previous,#news-table_next,.sorting').on('click', function() {
							append_button_news();
						});
                    });
					 
					function append_button_news(){
						$("#news-table tbody tr").each(function(index, element) { 
							if (typeof $(this).find(".dynamic_td").html() == "undefined") {
                                var id = $(this).attr("data-id");
                                var custom_column = '<td class="dynamic_td"><a href="javascript:;" onClick="editNews(\'' + id + '\')">แก้ไข</a></td><td class="dynamic_td"><a href="javascript:;" onClick="deleteNews(\'' + id + '\')"><i class="icon-remove-sign"></i></a></td>';
                                $(this).append(custom_column);
							}
                         });	
						
					}
					
                    function submitNews() {
                        if ($.trim($('input[name="news_title"]').val()).length == 0 || $.trim(tinyMCE.activeEditor.getContent()).length == 0) {
                            alert('กรุณากรอกข้อมูลให้ครบถ้วน!');
                            return false;
                        }
                        var val = $('form').serialize();
                        val += '&news_content=' + encodeURIComponent(tinyMCE.activeEditor.getContent());
						$('#addForm').loadOverStart();
                  		$.post('/admin/submit_news', val, function(data) {
							$('#addForm').loadOverStop(); 
           					if (data != 0) {
                                addNews();
                            } else {
                                alert('เกิดข้อผิดพลาด!');
                            } 
                        });
						 
                        return false;
                    }

                    function appendNews() {
                        $('#loading').removeClass('hide');
                        $.post('/admin/get_news', {start: newsPos}, function(msg) {
                            $('#news-list').append(msg);
                            $('#loading').addClass('hide');
                        });
                        newsPos += 10;
                    }

                    function deleteNews(id) {
                        var c = confirm('คุณต้องการลบข่าวนี้จริงหรือไม่?');
                        if (c) {
                            $.post('/admin/delete_news', {id: id}, function(msg) {
                                if (msg != 0) {
                                    addNews();
                                } else {
                                    alert('เกิดข้อผิดพลาด!');
                                }
                            });
                        }
                    }

                    function editNews(id) {
                        loadPage('/admin/edit_news/' + id);
                    }

</script>