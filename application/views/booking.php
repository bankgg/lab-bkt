<div class="warper">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'module/jquery.calendars/kmutt.calendars.picker.css'; ?>"/>
    <style>
        table thead tr th{
            text-align:center !important;
        }
        tr:hover{
            cursor:pointer;
        }
    </style>
    <div class="content">

        <div id="current_date" style="display: none"></div>
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="javascript:;" onClick="loadPage('/member/item_schedule/<?php echo rawurlencode($equipment->name); ?>/<?php echo $room; ?>/<?php echo $tag; ?>');">จองอุปกรณ์</a> <span class="divider">/</span></li> 
                    <li class="active"> <?php echo $equipment->name; ?></li> 
                </ul> 
            </div>
        </div> 
        <div class="row">
            <div class="span12 page-header" style="margin-top: 0;" > 
                <a href="javascript:;" class="back" onClick="loadPage('/member/item_schedule/<?php echo rawurlencode($equipment->name); ?>/<?php echo $room; ?>/<?php echo $tag; ?>');" style="text-decoration: none">
                    <span class="icon-stack">
                        <i class="icon-calendar icon-stack-base "></i>
                        <i class="icon-arrow-left" style="font-size: 1em;margin-top: 0.2em;"></i>
                    </span> ปฏิทิน</a> &nbsp;&nbsp;&nbsp; <?php echo $equipment->name; ?> (<?php echo $equipment->tag; ?>) ห้อง <?php echo $equipment->room_name; ?>  วันที่  <?php echo $date ?>
            </div>
        </div>
        <div class="row">
            <div class="span5 offset1" id="morning">
                <table class="table table-bordered  table-condensed">
                    <thead><tr><th>เวลา</th><th>#</th></tr></thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
            <div class="span5" id="evening">
                <table class="table table-bordered  table-condensed">
                    <thead><tr style="text-align:center"><th>เวลา</th><th>#</th></tr></thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>

        <?php
        foreach ($equipment_types as $type) {
            echo '<option value="' . $type->id . '">' . $type->name . '</option>';
        }
        ?>
        </select>
    </div>
</div>
<!--
<div class="control-group">
    <label class="control-label" for="room">ห้อง</label>
    <div class="controls">	
        <select id="room" disabled="disabled">
            <option value="no">-- กรุณาเลือกห้อง --</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="time">ช่วงเวลา</label>
    <div class="controls">
        <select id="time" disabled="disabled">
            <option value="no">-- กรุณาเลือกช่วงเวลา --</option>
        </select>
    </div> 
</div>-->
</div>

<div class="modal hide fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form onsubmit="return bookRange()">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">จองอุปกรณ์ โดย <?php echo $member->firstname . ' ' . $member->lastname; ?></h4>
                </div>
                <div class="modal-body">
                    <label>สิ่งที่จะทำ<span class="text-error">*</span></label>
                    <input type="text" id="description" name="description"  > 
                    <label class="checkbox">
                        <input type="checkbox" id="allday" name="allday"> ตลอดทั้งวัน
                    </label><br />
                    <label>จากวันที่ / เวลา</label>
                    <input type="date" id="date_start" value="<?php echo $date_inverse; ?>" />&nbsp;&nbsp; <input type="time" id="time_start" class="span2" step="1800" />
                    <label>ถึงวันที่ / เวลา</label>
                    <input type="date" id="date_end" value="<?php echo $date_inverse; ?>" />&nbsp;&nbsp; <input type="time" id="time_end" class="span2" step="1800" />
                    <input type="hidden" id="id" value="<?php echo $equipment->equipment_id; ?>" />
                    <input type="hidden" id="email" value="<?php echo $member->email; ?>" />
                    <input type="hidden" id="tr_id" />
                    <input type="hidden" id="error_count" value="0" />
                    <input type="hidden" id="date" value="<?php echo $date_inverse; ?>" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">ยกเลิก</button>
                    <a href="javascript:;" onClick="bookRange()" class="btn btn-main">จองอุปกรณ์เดี๋ยวนี้!</a> 
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="<?php echo base_url() . 'module/jquery.calendars/jquery.calendars.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'module/jquery.calendars/jquery.calendars.plus.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'module/jquery.calendars/jquery.calendars.picker.min.js'; ?>"></script>
<script type="text/javascript">
                        // Script ที่ใช้ gen 
                        var time1 = ['00:00 - 00:30', '00:30 - 01:00', '01:00 - 01:30', '01:30 - 02:00', '02:00 - 02:30', '02:30 - 03:00', '03:00 - 03:30', '03:30 - 04:00', '04:00 - 04:30', '04:30 - 05:00', '05:00 - 05:30', '05:30 - 06:00', '06:00 - 06:30', '06:30 - 07:00', '07:00 - 07:30', '07:30 - 08:00', '08:00 - 08:30', '08:30 - 09:00', '09:00 - 09:30', '09:30 - 10:00', '10:00 - 10:30', '10:30 - 11:00', '11:00 - 11:30', '11:30 - 12:00'];
                        var time2 = ['12:00 - 12:30', '12:30 - 13:00', '13:00 - 13:30', '13:30 - 14:00', '14:00 - 14:30', '14:30 - 15:00', '15:00 - 15:30', '15:30 - 16:00', '16:00 - 16:30', '16:30 - 17:00', '17:00 - 17:30', '17:30 - 18:00', '18:00 - 18:30', '18:30 - 19:00', '19:00 - 19:30', '19:30 - 20:00', '20:00 - 20:30', '20:30 - 21:00', '21:00 - 21:30', '21:30 - 22:00', '22:00 - 22:30', '22:30 - 23:00', '23:00 - 23:30', '23:30 - 24:00'];
                        var html = '';

                        $.event.special.rightclick = {
                            bindType: "contextmenu",
                            delegateType: "contextmenu"
                        };

                        $(document).ready(function(e) {
                            for ($i = 0; $i < time1.length; $i++) {
                                //Note// ลบ onclick="loadRequestForm();" ออกจาก td ไป

                                html = '<tr id="t' + ($i + 1) + '"><td style="width: 110px; text-align: center">' + time1[$i] + '</td><td class="span6" ></td></tr>';
                                $("#morning table tbody").append(html);
                            }
                            for ($j = 0; $j < time2.length; $j++) {
                                html = '<tr id="t' + ($j + 1 + 24) + '"><td style="width: 110px; text-align: center">' + time2[$j] + '</td><td class="span6"></td></tr>';
                                $("#evening table tbody").append(html);
                            }

                            $.get('/member/get_bookings/<?php echo $equipment->equipment_id; ?>/<?php echo $date_inverse; ?>', function(data) {
                                for (var i = 0; i < data.length; i++) {
                                    var start = data[i].start.substring(0, 5);

                                    for ($i = 0; $i < time1.length; $i++) {
                                        //Note// ลบ onclick="loadRequestForm();" ออกจาก td ไป
                                        var time = time1[$i].substring(0, 5);
                                        if (time == start) {
                                            $('#t' + ($i + 1)).find('td:eq(1)').html(time == start ? (data[i].description == '' ? '<b>จองโดย:</b> ' + data[i].firstname : '<b>จองโดย:</b> ' + data[i].firstname + ' (' + data[i].description
                                                    + ')') : '').addClass('bookline');
                                        }
                                    }
                                    for ($j = 0; $j < time2.length; $j++) {
                                        var time = time2[$j].substring(0, 5);
                                        if (time == start) {
                                            $('#t' + ($j + 24 + 1)).find('td:eq(1)').html(time == start ? (data[i].description == '' ? '<b>จองโดย:</b> ' + data[i].firstname : '<b>จองโดย:</b> ' + data[i].firstname + ' (' + data[i].description
                                                    + ')') : '').addClass('bookline');
                                        }
                                    }
                                }

                                $('#morning, #evening').on('mouseover', 'td', function(e) {
                                    if ($(this).text() == '') {
                                        $(this).addClass('hover-bg');
                                        $(this).html('คลิกเพื่อจอง');
                                    }
                                }).on('mouseout', 'td', function(e) {
                                    if ($(this).text() == 'คลิกเพื่อจอง' || $(this).text() == '') {
                                        $(this).removeClass('hover-bg');
                                        $(this).text('');
                                    }
                                });
                            }, 'json');

                            $('#current_date').text($.datepicker.formatDate('yy-mm-dd', new Date())).trigger('change');

                            $('#description').on('click', function() {
                                $(this).select();
                            });

                            $('#calendar').calendarsPicker({
                                monthsToShow: 3,
                                onSelect: function(d) {
                                    if ($('#equipment_types').val() == 'no' || $('#room').val() == 'no' || $('#time').val() == 'no') {
                                        $('#current_date').text(d);
                                    } else {
                                        $('#current_date').text(d).trigger('change');
                                    }
                                },
                            });

                            $('#equipment_types').change(function() {
                                if ($(this).val() != 'no') {
                                    $('#room').html('<option>กำลังโหลด..</option>');
                                    $.ajax({
                                        type: 'POST',
                                        url: '/member/book/type',
                                        data: {id: $('#equipment_types').val()},
                                        success: function(msg) {
                                            $('#room').html(msg);
                                            $('#room').removeAttr('disabled');
                                        }
                                    });
                                }
                            });

                            $('#room').change(function() {
                                if ($(this).val() != 'no') {
                                    if ($('#equipment_types').val() != 'no' && $('#time').val() != 'no') {
                                        $('#current_date').trigger('change');
                                    } else {
                                        $('#time').html('<option>กำลังโหลด..</option>');
                                        $('#time').html('<option value="no">-- กรุณาเลือกช่วงเวลา --</option>');
                                        $('#time').append('<option value="1">00.00 - 12.00น.</option>');
                                        $('#time').append('<option value="2">12.00 - 24.00น.</option>');
                                        $('#time').removeAttr('disabled');
                                    }
                                }
                            });

                            $('#time').change(function() {
                                if ($('#equipment_types').val() != 'no' && $('#room').val() != 'no')
                                    $('#current_date').trigger('change');
                            });

                            $('#current_date').change(function() {
                                var typeId = $('#equipment_types').val();
                                var roomId = $('#room').val();
                                var time = $('#time').val();
                                var d = $('#current_date').text();

                                $('#table_caption').html('ตารางการจองของห้อง <b>' + $('#room option:selected').text() + '</b> วันที่ <b>' + $('#time option:selected').text() + '</b>');
                                $('#table_shown').html('<h5 style="color: red; text-align: center">กำลังโหลดข้อมูล..</h5>');
                                $.ajax({
                                    type: 'POST',
                                    url: '/member/book/time',
                                    data: {typeId: typeId, roomId: roomId, date: d, time: time},
                                    success: function(msg) {
                                        $('#table_shown').html(msg);
                                    }
                                });
                            });
                        });

                        function bookFinal(o, equipmentId, date, startTime, endTime, description, email) {
                            var start = date + ' ' + startTime + ':00';
                            var end = date + ' ' + endTime + ':00';
                            var ret = 0;

                            $.ajax({
                                type: 'POST',
                                url: '/member/book_done',
                                data: {email: email, start: start, end: end, description: description, equipment_id: equipmentId},
                                statusCode: {
                                    500: function() {
                                        $('#error_count').val(parseInt($('#error_count').val(), 10) + 1);
                                    }
                                },
                                success: function(msg) {
                                    if (msg == -1) {
                                        $('#error_count').val(parseInt($('#error_count').val(), 10) + 1);
                                    } else {
                                        var d = new Date();
                                        var date = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
                                        if (start != date) {
                                            o.text('กำลังจองห้อง..');
                                            o.removeAttr('style');
                                            o.removeClass('hover-bg');
                                            o.addClass('bookline');
                                            o.html(msg);
                                        }
                                    }
                                }
                            });
                        }


                        function bookRange() {
                            var equipmentId = $('#id').val();
                            var description = $('#description').val();
                            var startDate = $('#date_start').val();
                            var endDate = $('#date_end').val();
                            var startTime = $('#time_start').val();
                            var endTime = $('#time_end').val();
                            var allDay = $('#allday').is(':checked');
                            var email = $('#email').val();

                            var currentDate = new Date().getTime();
                            var date1 = new Date(startDate);
                            var date2 = new Date(endDate);
                            var exactDateTimeStart = new Date(startDate + ' ' + startTime).getTime();
                            var exactDateTimeEnd = new Date(endDate + ' ' + endTime).getTime();
                            if (description == '') {
                                alert('กรุณาใส่สิ่งที่จะทำด้วย');
                                $('#description').focus();
                                return false;
                            }
                            
                            $.post('/member/check_privilege', {equipment_id: equipmentId}, function(data) {
                                if (data == 0) {
                                    loadRequestForm();
                                    return false;
                                }
                            });

                            if (exactDateTimeStart > exactDateTimeEnd ||
                                    exactDateTimeStart / 6000 % 30 != 0 ||
                                    exactDateTimeEnd / 6000 % 30 != 0 ||
                                    (exactDateTimeEnd - exactDateTimeStart) / 6000 / 30 < 1 ||
                                    currentDate > exactDateTimeStart) {
                                alert('เวลาไม่ถูกต้อง! กรุณาตรวจสอบรายละเอียดดังนี้\n\n- สามารถจองล่วงหน้าได้เท่านั้น ไม่สามารถจองย้อนหลังได้\n- ต้องจองเป็นหน่วยของครึ่ง ชม. เช่น 1 ชั่วโมง, 1 ชั่วโมง 30 นาที');
                                return false;
                            }

                            var dateDuration = date2.getDate() - date1.getDate();

                            if (allDay) {
                                var date = new Date(startDate + ' 00:00');
                                for (var i = 0; i <= dateDuration; i++) {
                                    for (var j = 0; j < 48; j++) {
                                        var tableObjectId = j + 1;
                                        var o = $('#t' + tableObjectId).find('td:eq(1)');
                                        var tempDate = new Date(date.getTime() + 30 * 60000);
                                        bookFinal(o, equipmentId, date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate(), date.getHours() + ':' + date.getMinutes(), tempDate.getHours() + ':' + tempDate.getMinutes(), description, email);
                                        date = new Date(date.getTime() + 30 * 60000);
                                    }
                                    date = new Date(date.getTime() + 30 * 60000);
                                }
                            } else {
                                var start = new Date(startDate + ' ' + startTime);
                                var end = new Date(endDate + ' ' + endTime);
                                var duration = (end.getTime() - start.getTime()) / 60000 / 30;
                                var date = start;

                                var d = new Date(startDate + ' 00:00');
                                var row = 1;
                                while (d.getTime() < exactDateTimeStart) {
                                    d = new Date(d.getTime() + 30 * 60000);
                                    row++;
                                }

                                var d2 = new Date(d.getTime() + 30 * 60000);
                                var ret = 0;


                                for (var i = 0; i < duration; i++, row++) {
                                    var tempDate = new Date(date.getTime() + 30 * 60000);
                                    var o = $('#t' + row).find('td:eq(1)');
                                    bookFinal(o, equipmentId, date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate(), date.getHours() + ':' + date.getMinutes(), tempDate.getHours() + ':' + tempDate.getMinutes(), description, email);
                                    date = new Date(date.getTime() + 30 * 60000);
                                }
                            }

                            $('#myModal').modal('hide');

                            return false;
                        }

                        function cancel(obj) {
                            var timeAll = obj.parents('tr').find('td:eq(0)').text();
                            var start = $('#date').val() + ' ' + timeAll.substring(0, 5);
                            var end = $('#date').val() + ' ' + timeAll.substring(8, 13);
                            var eid = $('#id').val();
                            var email = $('#email').val();
                            var c = confirm('คุณต้องการลบการจองนี้จิงหรือไม่?');
                            if (c) {
                                if (new Date().getTime() > new Date(start).getTime()) {
                                    $.pnotify({
                                        title: 'การดำเนินการ',
                                        text: 'ไม่สามารถลบการจองที่ผ่านมาแล้วได้!',
                                        type: 'error'
                                    });
                                    return false;
                                }

                                $.post('/member/cancel', {equipment_id: eid, start: start, end: end, email: email}, function(data) {
                                    if (data == '1') {
                                        $.pnotify({
                                            title: 'การดำเนินการ',
                                            text: 'ลบการจองเรียบร้อย!',
                                            type: 'success'
                                        });
                                        obj.removeClass('bookline');
                                        obj.parents('tr').find('td:eq(1)').text('');
                                    } else if (data == '2') {
                                        $.pnotify({
                                            title: 'การดำเนินการ',
                                            text: 'ไม่สามารถลบการจองของผู้อื่นได้!',
                                            type: 'error'
                                        });
                                    } else {
                                        $.pnotify({
                                            title: 'การดำเนินการ',
                                            text: 'เกิดข้อผิดพลาด!',
                                            type: 'error'
                                        });
                                    }
                                });
                            }
                        }
                        $('td').on("click", function() {
                            if ($(this).text().indexOf('จองโดย') != -1) {
                                cancel($(this));
                                return false;
                            }
                            if ($(this).attr('onClick') == 'loadRequestForm();') {
                                loadRequestForm();
                                return false;
                            }

                            var d = new Date('2013-01-01 00:00');
                            var id = parseInt($(this).parents('tr').attr('id').substr(1), 10);

                            for (var i = 0; i < id - 1; i++) {
                                d = new Date(d.getTime() + 30 * 60000);
                            }

                            var d2 = new Date(d.getTime() + 30 * 60000);

                            var start = addZero(d.getHours()) + ':' + addZero(d.getMinutes());
                            var end = addZero(d2.getHours()) + ':' + addZero(d2.getMinutes());

                            $('#time_start').val(addZero(d.getHours()) + ':' + addZero(d.getMinutes()));
                            $('#time_end').val(addZero(d2.getHours()) + ':' + addZero(d2.getMinutes()));

                            $('#myModal').modal('show');

                            return false;
                        });

                        // เช็คว่าตลอดทั้งวัน ถูกติ๊กหรือไม่ ถ้าติ๊กให้เอาเวลาออก
                        $('#allday').click(function() {
                            if ($(this).is(':checked')) {
                                $('#time_start').hide();
                                $('#time_end').hide();
                            } else {
                                $('#time_start').show();
                                $('#time_end').show();
                            }
                        });
</script>

</div>