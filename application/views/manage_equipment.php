<!-- view Manage Equipment-->
<link rel="stylesheet" href="<?php echo base_url() . 'module/DataTables/css/demo_table.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'asset/js/tagsinput2.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.default.css'; ?>" />
<style>
    .controls .checkbox {
        width: 7em;
    }
    .nav-tabs li a {
        background-color: #fafffa;
    }
    .nav-tabs li a:hover {
        background-color: #f4f9f4;
    }
    .nav-tabs .active a {
        background-color: #D5F0CD !important;
    }
    .content {
        margin-bottom: 0px;
    }
    legend {
        font-weight: bold;
    }
</style>
<div class="warper" style="margin-top: 30px;">
    <div class="row">
        <div class="span9">
            <ul class="nav nav-tabs" id="myTab" style="margin: 0;border-bottom: none;">
                <li class="active"><a  href="#all_equipment">อุปกรณ์ทั้งหมด</a></li>
                <li><a href="#add_new_sci_equipment">เพิ่มครุภัณฑ์วิทยาศาสตร์</a></li>
                <li><a href="#add_new_equipment">เพิ่มครุภัณฑ์ทั่วไป</a></li> 
                <li><a href="#add_new_equipment_from_file">เพิ่มครุภัณฑ์จากไฟล์</a></li>
            </ul>
            <div class="content" style="padding-top:0">
                <div class="tab-content"> 

                    <!-- tab 1 -->
                    <div class="tab-pane active" id="all_equipment"  >
                        <div class="title">
                            <center>
                                อุปกรณ์ทั้งหมด
                            </center>
                        </div>
                        <div id="table_equipment" class="center">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Serial Number</td>
                                        <td>ชื่อ</td>
                                        <td>ผู้ดูแล</td>
                                        <td>ด้าน</td>
                                        <td>ห้อง</td>
                                    </tr>
                                </thead>
                                <tbody class="">
                                    <?php
                                    $i = 1;
                                    foreach ($all_equipment_data as $e) {
                                        echo '<tr onClick="details(' . $e->equipment_id . ')">';
                                        echo '<td>' . $i++ . '</td>';
                                        echo '<td>' . $e->serial_no . '</td>';
                                        echo '<td>' . $e->name . '</td>';
                                        echo '<td>' . $e->person_responsible . '</td>';
                                        echo '<td>' . $e->type . '</td>';
                                        echo '<td>' . $e->room . '</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- tab 2 -->
                    <div class="tab-pane" id="add_new_sci_equipment">
                        <div class="title">
                            <center>
                                เพิ่มครุภัณฑ์วิทยาศาสตร์
                            </center>
                        </div>
                        <!--<form class="form-horizontal" style="margin-top:20px" id="form-add">
                        <fieldset>
                          <div class="control-group">
                            <label class="control-label" for="equipment-name">ชื่ออุปกรณ์</label>
                            <div class="controls" >
                              <input class="input-80" style="width: 230px" class="typeahead" type="text" name="equipment-name" id="equipment-name" required="required" />
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="equipment-type">ด้านการปฎิบัติการ</label>
                            <div class="controls" >
                              <select class="input-80" name="equipment-type" id="equipment-type">
                                <option value="no">-- กรุณาเลือกด้านปฏิบัติการ --</option>
                        <?php
                        foreach ($equipment_types as $e) {
                            echo '<option value="' . $e->id . '">' . $e->name . '</option>';
                        }
                        ?>
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="equipment-room">ห้อง</label>
                            <div class="controls">
                              <select class="input-80" name="equipment-room" id="equipment-room" disabled="disabled">
                                <option value="no">-- กรุณาเลือกห้อง --</option>
                              </select>
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="equipment-room">แท็คอุปกรณ์</label>
                            <div class="controls">
                              <input class="input-80" type="text" name="equipment-tag" id="equipment-tag" required="required" />
                            </div>
                          </div>
                          <div class="control-group">
                            <label class="control-label" for="equipment-limit-times">ใช้งานได้</label>
                            <div class="controls" >
                              <input type="number" min="0" name="equipment-limit-times" id="equipment-limit-times" class="span1" />
                              ครั้ง และ/หรือ
                              <input type="number" step="any" min="0" name="equipment-limit-hours" id="equipment-limit-hours" class="span1"/>
                              ชั่วโมง </div>
                          </div> 
                          <div class="control-group">
                            <label class="control-label" for="equipment-room">บันทึกที่ต้องการเก็บ</label>
                            <div class="controls">
                        <?php
                        $count = 0;
                        foreach ($columns as $name => $col) {
                            echo '<label class="checkbox inline">';
                            echo '<input type="checkbox" name="columns[]" value="' . $name . '" ' . ($name == 'time_start' || $name == 'time_end' ? 'checked="checked" disabled="disabled"' : '') . ' />' . $col['thai_name'];
                            echo '</label>';
                            echo $count % 2 == 1 ? '<br />' : '';
                            $count++;
                        }
                        ?>
                              <div style="margin-top:10px" id="other_column"> อื่นๆ &nbsp;
                                <input type="text" name="columns_added[]" class="other" />
                                <a href="javascript:;" id="add"><i class="icon-plus-sign none_underline" ></i></a> </div>
                            </div>
                          </div>
                        </fieldset>
                        <div class="row">
                          <div class="span3 offset1"> <a class="btn btn-main btn-block" href="javascript:;" onClick="addEquipment()">เพิ่ม</a> </div>
                        </div>
                      </form>-->
                        <?php echo form_open_multipart('admin/add_equipment'); ?>
                        <div class="row" style="padding:0 15px">
                            <div class=" span4 content" >
                                <legend>รายละเอียดครุภัณฑ์</legend>
                                <table>
                                    <tr>
                                        <td>รหัสทรัพย์สิน</td>
                                        <td><input type="text" id="" name="asset_id"/></td>
                                    </tr>
                                    <tr>
                                        <td>Serial Number</td>
                                        <td><input type="text" id="" name="serial_no"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อทรัพยสิน</td>
                                        <td><input type="text" id="" name="name"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อตั้งงบประมาณ</td>
                                        <td><input type="text" id="" name="budget_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>ยี่ห้อ</td>
                                        <td><input type="text" id="" name="brand"/></td>
                                    </tr>
                                    <tr>
                                        <td>รุ่น</td>
                                        <td><input type="text" id="" name="model"/></td>
                                    </tr>
                                    <tr>
                                        <td>ประจำที่</td>
                                        <td><input type="text" id="" name="place_at"/></td>
                                    </tr>
                                    <tr>
                                        <td>บันทึก</td>
                                        <td><input type="text" id="" name="remark"/></td>
                                    </tr>
                                    <tr>
                                        <td>รูปอุปกรณ์</td>
                                        <td><input type="file" id="" name="equipment_image_path"/></td>
                                    </tr>
                                    <tr>
                                        <td>ข้อมูลจำเพาะ</td>
                                        <td><input type="file" id="" name="specific_info_path"/></td>
                                    </tr> 
                                </table>
                            </div>
                            <div class="span4 content">
                                <legend>ผู้รับผิดชอบ</legend>
                                <table>
                                    <tr>
                                        <td>อุปกรณ์ด้าน</td>
                                        <td><select name="equipment_type" id="">
                                                <option value="no">-- กรุณาเลือกด้านปฏิบัติการ --</option>
                                                <?php
                                                foreach ($equipment_types as $e) {
                                                    echo '<option value="' . $e->id . '">' . $e->name . '</option>';
                                                }
                                                ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td>ห้อง</td>
                                        <td><select name="room" id="room" disabled="disabled">
                                                <option value="no">-- กรุณาเลือกห้อง --</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td>สภาพครุภัณฑ์</td>
                                        <td style="padding-bottom: 10px;"><input type="radio" id="" name="status" value="0"/>
                                            ปกติ
                                            <input type="radio" id="" name="status" value="1"/>
                                            ซ่อมแซม </td>
                                    </tr>
                                    <tr>
                                        <td>ผู้รับผิดชอบ</td>
                                        <td><input type="text" id="" name="person_reponsible"/></td>
                                    </tr>                 
                                    <tr>
                                        <td>ชื่อบริษัทที่ซื้อ</td>
                                        <td><input type="text" id="" name="seller_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>หมายเลขโทรศัพท์</td>
                                        <td><input type="text" id="" name="seller_tel_no"/></td>
                                    </tr>
                                    <tr>
                                        <td>ที่อยู่</td>
                                        <td><textarea name="seller_address"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>หน่วยงานผู้รับผิดชอบ</td>
                                        <td><input type="text" id="" name="department_responsible_id"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อหน่วยงานผู้รับผิดชอบ</td>
                                        <td><input type="text" id="" name="department_responsible_name"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="padding:0 15px">
                            <div class="span4 content">
                                <legend>การจัดซื้อ</legend>
                                <table> 
                                    <tr>
                                        <td>วิธีจัดหา</td>
                                        <td><select id="buying_method">
                                                <?php
                                                foreach ($buying_methods as $b) {
                                                    echo '<option value="' . $b->id . '">' . $b->name . '</option>';
                                                }
                                                ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td>วันที่รับทรัพย์สิน	</td>
                                        <td><input type="date" id="" name="received_date"/></td>
                                    </tr>
                                    <tr>
                                        <td>มูลค่าที่ซื้อ</td>
                                        <td><input type="number" id="" name="buying_cost" step="any"/></td>
                                    </tr>
                                    <tr>
                                        <td>กองทุน</td>
                                        <td><input type="text" id="" name="funds"/></td>
                                    </tr>
                                    <tr>
                                        <td>หน่วยงาน</td>
                                        <td><input type="text" id="" name="department_id"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อหน่วยงาน</td>
                                        <td><input type="text" id="" name="department_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>แผนงาน</td>
                                        <td><input type="text" id="" name="plan"/></td>
                                    </tr>
                                    <tr>
                                        <td>โครงการ	</td>
                                        <td><input type="text" id="" name="project"/></td>
                                    </tr>
                                    <tr>
                                        <td>วันที่เริ่มคำนวณค่าเสื่อมราคา</td>
                                        <td><input type="date" id="" name="date_start_calculate"/></td>
                                    </tr>
                                </table>
                                <input type="hidden" name="type" value="science" />
                            </div>
                          
                            <div class="span4 content">
                                <legend>อายุการใช้งานอุปกรณ์</legend>
                                <table>
                                    <tr>
                                        <td>จำนวนครั้งที่ใช้ได้</td>
                                        <td><input type="number" id="" name="limit_times" step="integer"/></td>
                                    </tr>
                                    <tr>
                                        <td>เวลาที่ใช้ได้</td>
                                        <td><input type="number" id="" name="limit_hours" step="any"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <div class="span5 offset2">
                                <button class="btn btn-main btn-block">เพิ่มอุปกรณ์ !</button>
                            </div>
                        </div>
                        </form>
                    </div>
                    <!-- tab 3 -->
                    <div class="tab-pane" id="add_new_equipment">
                        <div class="title">
                            <center>
                                เพิ่มครุภัณฑ์ทั่วไป
                            </center>
                        </div>
                        <?php echo form_open_multipart('admin/add_equipment'); ?>
                        <div class="row" style="padding:0 15px">
                            <div class=" span4 content" >
                                <legend>รายละเอียดครุภัณฑ์</legend>
                                <table>
                                    <tr>
                                        <td>รหัสทรัพย์สิน</td>
                                        <td><input type="text" id="" name="asset_id"/></td>
                                    </tr>
                                    <tr>
                                        <td>Serial Number</td>
                                        <td><input type="text" id="" name="serial_no"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อทรัพยสิน</td>
                                        <td><input type="text" id="" name="name"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อตั้งงบประมาณ</td>
                                        <td><input type="text" id="" name="budget_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>ยี่ห้อ</td>
                                        <td><input type="text" id="" name="brand"/></td>
                                    </tr>
                                    <tr>
                                        <td>รุ่น</td>
                                        <td><input type="text" id="" name="model"/></td>
                                    </tr>
                                    <tr>
                                        <td>ประจำที่</td>
                                        <td><input type="text" id="" name="place_at"/></td>
                                    </tr>
                                    <tr>
                                        <td>บันทึก</td>
                                        <td><input type="text" id="" name="remark"/></td>
                                    </tr>
                                    <tr>
                                        <td>รูปอุปกรณ์</td>
                                        <td><input type="file" id="" name="equipment_image_path"/></td>
                                    </tr>
                                    <tr>
                                        <td>ข้อมูลจำเพาะ</td>
                                        <td><input type="file" id="" name="specific_info_path"/></td>
                                    </tr> 
                                </table>
                            </div>
                            <div class="span4 content">
                                <legend>ผู้รับผิดชอบ</legend>
                                <table>
                                    <tr>
                                        <td>อุปกรณ์ด้าน</td>
                                        <td><select name="equipment_type" id="equipment-type2">
                                                <option value="no">-- กรุณาเลือกด้านปฏิบัติการ --</option>
                                                <?php
                                                foreach ($equipment_types as $e) {
                                                    echo '<option value="' . $e->id . '">' . $e->name . '</option>';
                                                }
                                                ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td>ห้อง</td>
                                        <td><select name="room" id="equipment-room2" disabled="disabled">
                                                <option value="no">-- กรุณาเลือกห้อง --</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td>สภาพครุภัณฑ์</td>
                                        <td style="padding-bottom: 10px;"><input type="radio" id="" name="status" value="0"/>
                                            ปกติ
                                            <input type="radio" id="" name="status" value="1"/>
                                            ซ่อมแซม </td>
                                    </tr>
                                    <tr>
                                        <td>ผู้รับผิดชอบ</td>
                                        <td><input type="text" id="" name="person_reponsible"/></td>
                                    </tr>                 
                                    <tr>
                                        <td>ชื่อบริษัทที่ซื้อ</td>
                                        <td><input type="text" id="" name="seller_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>หมายเลขโทรศัพท์</td>
                                        <td><input type="text" id="" name="seller_tel_no"/></td>
                                    </tr>
                                    <tr>
                                        <td>ที่อยู่</td>
                                        <td><textarea name="seller_address"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>หน่วยงานผู้รับผิดชอบ</td>
                                        <td><input type="text" id="" name="department_responsible_id"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อหน่วยงานผู้รับผิดชอบ</td>
                                        <td><input type="text" id="" name="department_responsible_name"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="padding:0 15px">
                            <div class="span4 content">
                                <legend>การจัดซื้อ</legend>
                                <table> 
                                    <tr>
                                        <td>วิธีจัดหา</td>
                                        <td><select id="buying_method">
                                                <?php
                                                foreach ($buying_methods as $b) {
                                                    echo '<option value="' . $b->id . '">' . $b->name . '</option>';
                                                }
                                                ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td>วันที่รับทรัพย์สิน	</td>
                                        <td><input type="date" id="" name="received_date"/></td>
                                    </tr>
                                    <tr>
                                        <td>มูลค่าที่ซื้อ</td>
                                        <td><input type="number" id="" name="buying_cost" step="any"/></td>
                                    </tr>
                                    <tr>
                                        <td>กองทุน</td>
                                        <td><input type="text" id="" name="funds"/></td>
                                    </tr>
                                    <tr>
                                        <td>หน่วยงาน</td>
                                        <td><input type="text" id="" name="department_id"/></td>
                                    </tr>
                                    <tr>
                                        <td>ชื่อหน่วยงาน</td>
                                        <td><input type="text" id="" name="department_name"/></td>
                                    </tr>
                                    <tr>
                                        <td>แผนงาน</td>
                                        <td><input type="text" id="" name="plan"/></td>
                                    </tr>
                                    <tr>
                                        <td>โครงการ	</td>
                                        <td><input type="text" id="" name="project"/></td>
                                    </tr>
                                    <tr>
                                        <td>วันที่เริ่มคำนวณค่าเสื่อมราคา</td>
                                        <td><input type="date" id="" name="date_start_calculate"/></td>
                                    </tr>
                                </table>
                                <input type="hidden" name="type" value="general" />
                            </div>
 
                        </div>
                        <div class="row" style="margin-top:20px">
                            <div class="span5 offset2">
                                <button class="btn btn-main btn-block">เพิ่มอุปกรณ์ !</button>
                            </div>
                        </div>
                        </form>
                    </div>

                    <!-- tab 4 -->
                    <div class="tab-pane" id="add_new_equipment_from_file">
                        <div class="title">
                            <center> 
                                เพิ่มครุภัณฑ์จากไฟล์
                            </center>
                        </div> 
                        <div class="row"  style="margin:10px 0"> 
                            <form>
                                <div class=" span5 offset2 content" >
                                    <div>
                                        <p>&nbsp;&nbsp;&nbsp;กรุณาเลือกไฟล์ (.xls) ที่มีรูปแบบตามมาตรฐาน หากไม่ทราบ <a href="">คลิกที่นี่</a> เพื่อดาวโหลดไฟล์ต้นแบบ</p>
                                    </div>
                                    <hr />  
                                    <table class="center">
                                        <tr> 
                                            <td><input type="file" id="" name="" class="input-block-level" /></td>
                                            <td><input type="submit" id="" name="" value="เพิ่มอุปกรณ์" class="btn btn-main"/></td>
                                        </tr> 
                                    </table> 
                                </div> 
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end content เพิ่มอุปกรณ์-->

        <div class="span3 marginTop40">
            <div class="content" style="padding-top:0;margin-bottom:20px">
                <div class="title">
                    <center>
                        ด้านปฎิบัติการ
                    </center>
                </div>
                <div id="table_lab" class="center">
                    <table class="table table-condensed" style="margin-top:20px" >
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>ด้าน</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($equipment_types as $e) {
                                echo '<tr data-equipment-type-id="' . $e->id . '">';
                                echo '<td>' . $count++ . '</td>';
                                echo '<td>' . $e->name . '</td>';
                                echo '<td><a href="javascript:;" onClick="deleteEquipmentType(' . $e->id . ')"><i class="icon-trash"></i></a></td>';
                                echo '</tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                    <br />
                    <hr />
                </div>
                <div class="center">
                    <button class="btn btn-main btn-block" id="add_lab">เพิ่มด้าน</button>
                    <form style="margin-top:20px" id="form_lab">
                        <center>
                            <label  for="type-type">ชื่อด้านการปฎิบัติการ</label>
                            <input  type="text" id="type-type" name="type-type"/>
                            <a class="btn btn-main" href="javascript:;" style="width:60px"  onClick="addType()">เพิ่ม</a>
                            <a class="btn " href="javascript:;" id="cancel_add_lab">ยกเลิก</a> </div>
                        </center>
                    </form>
                </div> 
                <div class="content" style="padding-top:0;">
                    <div class="title">
                        <center>
                            ห้อง
                        </center>
                    </div>
                    <div id="table_room" class="center">
                        <table class="table table-condensed" style="margin-top:20px" >
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>ด้าน</td>
                                    <td>ห้อง</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($rooms as $room) {
                                    echo '<tr data-room-id="' . $room->room_id . '">';
                                    echo '<td>' . $count++ . '</td>';
                                    echo '<td>' . $room->type_name . '</td>';
                                    echo '<td>' . $room->room_name . '</td>';
                                    echo '<td><a href="javascript:;" onClick="deleteRoom(\'' . $room->room_id . '\')"><i class="icon-trash"></i></a></td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <br />
                    <hr />
                    <div class="center">
                        <button class="btn btn-main btn-block" id="add_room">เพิ่มห้อง</button>
                        <form style="margin-top:20px" id="form_room">
                            <label  for="equipment-type">ด้านการปฎิบัติการ</label>
                            <select name="room-type" class="input-block-level" id="room-type">
                                <option value="no">-- กรุณาเลือกด้านปฏิบัติการ --</option>
                                <?php
                                foreach ($equipment_types as $e) {
                                    echo '<option value="' . $e->id . '">' . $e->name . '</option>';
                                }
                                ?>
                            </select>
                            <label  for="room-name">ห้องเลขที่</label>
                            <input  type="text" name="room-name" class="input-block-level"  id="room-name"/>
                            <center>
                                <a class="btn btn-main" style="width:60px" href="javascript:;" onClick="addRoom()">เพิ่ม</a>
                                <a class="btn " href="javascript:;" id="cancel_add_room">ยกเลิก</a>
                            </center>  
                        </form>
                    </div>  
                </div>
                <!-- end content เพิ่มห้อง --> 
            </div>
            <!-- endเพิ่มด้านปฎบัติการ--> 

        </div>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/bootbox.min.js'; ?>"></script> 
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/typeahead.min.js'; ?>"></script> 
        <script type="text/javascript" src="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.min.js'; ?>"></script> 
        <script type="text/javascript" src="<?php echo base_url() . 'module/DataTables/js/jquery.dataTables.js'; ?>"></script> 
        <script type="text/javascript">
                                    var historyTable;

                                    $(function() {
                                        $.pnotify.defaults.delay = 2500;
                                        $.pnotify.defaults.history = false;
                                        $.pnotify.defaults.animation_speed = 'normal';
                                    });

                                    if ($('#table_room table tbody tr').length >= 1) {
                                        $('#table_room table ').dataTable({
                                            "bPaginate": true,
                                            "bLengthChange": false,
                                            "bFilter": false,
                                            "bSort": true,
                                            "bInfo": false,
                                            "bAutoWidth": true,
                                            'iDisplayLength': 5,
                                        });
                                    }

                                    if ($('#table_lab table tbody tr').length >= 1) {
                                        $('#table_lab table ').dataTable({
                                            "bPaginate": true,
                                            "bLengthChange": false,
                                            "bFilter": false,
                                            "bSort": true,
                                            "bInfo": false,
                                            "bAutoWidth": true,
                                            'iDisplayLength': 5,
                                        });
                                    }

                                    if ($('#table_equipment table tbody tr').length >= 1) {
                                        $('#table_equipment table ').dataTable({
                                            "bPaginate": true,
                                            "bLengthChange": false,
                                            "bFilter": true,
                                            "bSort": true,
                                            "bInfo": true,
                                            "bAutoWidth": true,
                                            "sDom": '<"equipmentbar">frtip',
                                            'iDisplayLength': 20,
                                        });
                                        $("div.equipmentbar").html('ครุภัณฑ์ <select id="eq_category"><option value="1">วิทยาศาสตร์</option><option value="1">ทั่วไป</option></select>');
                                    }

                                    $("#form_room,#form_lab").hide();

                                    $("#add_room").click(function() {
                                        $(this).slideUp();
                                        $("#form_room").slideToggle();

                                    });

                                    $("#add_lab").click(function() {
                                        $(this).slideUp();
                                        $("#form_lab").slideToggle();

                                    });

                                    $("#cancel_add_room").click(function() {
                                        $("#add_room").slideDown();
                                        $("#form_room").slideUp();
                                    });

                                    $("#cancel_add_lab").click(function() {
                                        $("#add_lab").slideDown();
                                        $("#form_lab").slideUp();
                                    });

                                    $('#equipment-name').typeahead({
                                        prefetch: '/admin/json_all_equipments'
                                    });

                                    $('body').on('click', 'input.other', function() {
                                        var index = $('input.other').index($(this));
                                        var res = $('input.other:eq(' + $('input.other').index($(this)) + ')').val().split(',');
                                        $('#col_name').val(res[0]);
                                        $('#col_thai_name').val(res[1]);
                                        $('#col_type').val(res[2]);
                                        $('#col_eq').val(index);
                                        $('#myModal').modal('show');
                                    });

                                    $('#myModal').on('shown.bs.modal', function() {
                                        $('#col_name').focus();
                                    });

                                    function addType() {
                                        var type = $('#type-type').val();

                                        if (type == '') {
                                            $.pnotify({
                                                title: 'การดำเนินการ',
                                                text: 'คุณยังไม่ได้กรอกชื่อด้านปฏิบัติการที่ต้องการเพิ่ม!',
                                                type: 'error'
                                            });
                                            return false;
                                        }

                                        $.post('/admin/add_type', {type: type}, function(data) {
                                            if (data == 1) {
                                                $.pnotify({
                                                    title: 'การดำเนินการ',
                                                    text: 'เพิ่มด้านปฏิบัติการเรียบร้อย!',
                                                    type: 'success'
                                                });
                                                activeMenu("#equipmentMenu");
                                                manage_equipment();
                                            } else {
                                                $.pnotify({
                                                    title: 'การดำเนินการ',
                                                    text: 'เกิดข้อผิดพลาดระหว่างการเพิ่มด้านปฏิบัติการ!',
                                                    type: 'error'
                                                });
                                            }
                                        });
                                    }

                                    function addRoom() {
                                        var name = $('#room-name').val();
                                        var type = $('#room-type').val();

                                        if (name == '' || type == 'no') {
                                            $.pnotify({
                                                title: 'การดำเนินการ',
                                                text: 'คุณยังไม่ได้กรอก/เลือกชื่อห้อง และ/หรือชื่อด้านปฏิบัติการ!',
                                                type: 'error'
                                            });
                                            return false;
                                        }

                                        $.post('/admin/add_room', {name: name, type: type}, function(data) {
                                            if (data == 1) {
                                                $.pnotify({
                                                    title: 'การดำเนินการ',
                                                    text: 'เพิ่มห้องเรียบร้อย!',
                                                    type: 'success'
                                                });
                                                activeMenu("#equipmentMenu");
                                                manage_equipment();
                                            } else {
                                                $.pnotify({
                                                    title: 'การดำเนินการ',
                                                    text: 'เกิดข้อผิดพลาดระหว่างการเพิ่มห้อง!',
                                                    type: 'error'
                                                });
                                            }
                                        });
                                    }

                                    function addEquipment() {
                                        var error = 0;
                                        $('input.other').each(function() {
                                            var value = $(this).val();
                                            if (value != '') {
                                                if (value.split(',').length - 1 != 2) {
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'รูปแบบของคอลัมน์ที่เพิ่มผิดพลาด!',
                                                        type: 'error'
                                                    });
                                                    error++;
                                                    return false;
                                                }

                                                var type = value.split(',')[2];
                                                if (type != 'int' && type != 'varchar' && type != 'datetime') {
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'รูปแบบข้อมูลของคอลัมน์ที่เพิ่มผิดพลาด!',
                                                        type: 'error'
                                                    });
                                                    error++;
                                                    return false;
                                                }

                                                var english = /^[A-Za-z0-9]*$/;
                                                if (!english.test(value.split(',')[0])) {
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'ชื่อของคอลัมน์ที่เป็นภาษาอังกฤษผิดพลาด!',
                                                        type: 'error'
                                                    });
                                                    error++;
                                                    return false;
                                                }

                                                if (value != value.toLowerCase()) {
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'ชื่อคอลัมน์ต้องเป็นตัวอักษรพิมพ์เล็กเท่านั้น!',
                                                        type: 'error'
                                                    });
                                                    error++;
                                                    return false;
                                                }
                                            }
                                        });

                                        if ($('#equipment-name').val() == '' ||
                                                $('#equipment-type').val() == 'no' ||
                                                $('#equipment-room').val() == 'no' ||
                                                $('#equipment-tag').val() == '' ||
                                                ($('#equipment-limit-times').val() == '' && $('#equipment-limit-hours').val() == '')) {

                                            $.pnotify({
                                                title: 'การดำเนินการ',
                                                text: 'กรุณากรอกข้อมูลให้ครบถ้วน!',
                                                type: 'error'
                                            });

                                            error++;
                                            return false;
                                        }
                                        if (error == 0) {
                                            $.post('/admin/add_equipment', $('#form-add').serialize(), function(data) {
                                                if (data == 1) {
                                                    $('input').val('');
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'เพิ่มอุปกรณ์เรียบร้อย!',
                                                        type: 'success'
                                                    });
                                                    manage_equipment();
                                                } else {
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'เกิดข้อผิดพลาดระหว่างการเพิ่มอุปกรณ์!',
                                                        type: 'error'
                                                    });
                                                }
                                            });
                                        }
                                    }

                                    $('#equipment-type').change(function() {
                                        if ($(this).val() != 'no') {
                                            $('#equipment-room').html('<option>กำลังโหลด..</option>');
                                            $.ajax({
                                                type: 'POST',
                                                url: '/member/book/type',
                                                data: {id: $('#equipment-type').val()},
                                                success: function(msg) {
                                                    $('#equipment-room').html(msg);
                                                    $('#equipment-room').removeAttr('disabled');
                                                }
                                            });
                                        } else {
                                            $('#equipment-room').attr('disabled', 'disabled');
                                        }
                                    });

                                    $('#equipment-type2').change(function() {
                                        if ($(this).val() != 'no') {
                                            $('#equipment-room2').html('<option>กำลังโหลด..</option>');
                                            $.ajax({
                                                type: 'POST',
                                                url: '/member/book/type',
                                                data: {id: $('#equipment-type2').val()},
                                                success: function(msg) {
                                                    $('#equipment-room2').html(msg);
                                                    $('#equipment-room2').removeAttr('disabled');
                                                }
                                            });
                                        } else {
                                            $('#equipment-room').attr('disabled', 'disabled');
                                        }
                                    });

                                    $('#add').on('click', function() {
                                        $('#other_column').append('<div style="margin:10px 0 0 35px"><input type="text" name="columns_added[]" class="other" /> <a href="javascript:;" id="remove"><i class=" icon-remove none_underline" ></i></a></div> ');
                                    });

                                    $(document).on('click', '#remove', function() {
                                        $(this).parent('div').remove('div');
                                    });

                                    function deleteColumn(name) {
                                        var c = confirm('คุณต้องการลบหัวข้อบันทึกการทดลอง "' + name + '" จริงหรือไม่?');
                                        if (!c) {
                                            return false;
                                        }

                                        $.get('/admin/delete_column/' + name, function(data) {
                                            if (data == 1) {
                                                $('tr[data-col-name="' + name + '"]').fadeOut('slow', function() {
                                                    var pos = historyTable.fnGetPosition(this);
                                                    historyTable.fnDeleteRow(pos);
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'ลบหัวข้อบันทึกการทดลองเรียบร้อย!',
                                                        type: 'success'
                                                    });
                                                });
                                            } else {
                                                $.pnotify({
                                                    title: 'การดำเนินการ',
                                                    text: 'เกิดข้อผิดพลาดระหว่างการลบหัวข้อบันทึกการทดลอง!',
                                                    type: 'error'
                                                });
                                            }
                                        });
                                        return false;
                                    }

                                    function deleteRoom(id) {
                                        var roomName = $('tr[data-room-id="' + id + '"]').find('td:eq(2)').text();
                                        var c = confirm('คุณต้องการลบห้อง "' + roomName + '" จริงหรือไม่?\n\n**คำเตือน: อุปกรณ์ที่อยู่ในห้องนี้ทั้งหมด และการจองของอุปกรณ์นั้น ๆ ทั้งหมดจะถูกลบไปด้วย');
                                        if (!c) {
                                            return false;
                                        }

                                        var pass = prompt('กรุณาใส่รหัสสมาชิกเพื่อยืนยันการลบ');
                                        if (pass == null) {
                                            $.pnotify({
                                                title: 'การดำเนินการ',
                                                text: 'คุณไม่ได้ใส่รหัสยืนยันการลบ!',
                                                type: 'error'
                                            });
                                            return false;
                                        } else {
                                            $.post('/member/check_pass_of_current_user', {password: pass}, function(data) {
                                                if (data == 1) {
                                                    $.get('/admin/delete_room/' + id, function(data) {
                                                        if (data == 1) {
                                                            $('tr[data-room-id="' + id + '"]').fadeOut('slow', function() {
                                                                var pos = historyTable.fnGetPosition(this);
                                                                historyTable.fnDeleteRow(pos);
                                                                $.pnotify({
                                                                    title: 'การดำเนินการ',
                                                                    text: 'ลบห้องเรียบร้อย!',
                                                                    type: 'success'
                                                                });
                                                                manage_equipment();
                                                            });
                                                        } else {
                                                            $.pnotify({
                                                                title: 'การดำเนินการ',
                                                                text: 'เกิดข้อผิดพลาดระหว่างการลบห้อง!',
                                                                type: 'error'
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'รหัสผ่านไม่ถูกต้อง!',
                                                        type: 'error'
                                                    });
                                                }
                                            });
                                        }

                                        return false;
                                    }

                                    function deleteEquipmentType(id) {
                                        var name = $('tr[data-equipment-type-id="' + id + '"]').find('td:eq(1)').text();
                                        var c = confirm('คุณต้องการลบด้านปฏิบัติการ "' + name + '" จริงหรือไม่?\n\n**คำเตือน: ห้องที่อยู่ในด้านปฏิบัตรการนี้ทั้งหมด, อุปกรณ์ที่อยู่ในห้องนี้ทั้งหมด และการจองของอุปกรณ์นั้น ๆ ทั้งหมดจะถูกลบไปด้วย');
                                        if (!c) {
                                            return false;
                                        }

                                        var pass = prompt('กรุณาใส่รหัสสมาชิกเพื่อยืนยันการลบ');
                                        if (pass == null) {
                                            $.pnotify({
                                                title: 'การดำเนินการ',
                                                text: 'คุณไม่ได้ใส่รหัสยืนยันการลบ!',
                                                type: 'error'
                                            });
                                            return false;
                                        } else {
                                            $.post('/member/check_pass_of_current_user', {password: pass}, function(data) {
                                                if (data == 1) {
                                                    $.get('/admin/delete_equipment_type/' + id, function(data) {
                                                        if (data == 1) {
                                                            $('tr[data-equipment-type-id="' + id + '"]').fadeOut('slow', function() {
                                                                var pos = historyTable.fnGetPosition(this);
                                                                historyTable.fnDeleteRow(pos);
                                                                $.pnotify({
                                                                    title: 'การดำเนินการ',
                                                                    text: 'ลบด้านปฏิบัติการเรียบร้อย!',
                                                                    type: 'success'
                                                                });
                                                            });
                                                        } else {
                                                            $.pnotify({
                                                                title: 'การดำเนินการ',
                                                                text: 'เกิดข้อผิดพลาดระหว่างการลบด้านปฏิบัติการ!',
                                                                type: 'error'
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    $.pnotify({
                                                        title: 'การดำเนินการ',
                                                        text: 'รหัสผ่านไม่ถูกต้อง!',
                                                        type: 'error'
                                                    });
                                                }
                                            });
                                        }

                                        return false;
                                    }

                                    $('#myTab a').click(function(e) {
                                        e.preventDefault();
                                        $(this).tab('show');
                                    });

        </script>