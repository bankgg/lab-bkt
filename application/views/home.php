<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.default.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'module/nivo-slider3.2/nivo-slider/nivo-slider.css'; ?>"/>
<link rel="stylesheet" href="<?php echo base_url() . 'module/nivo-slider3.2/nivo-slider/themes/default/default.css'; ?>"/>
<link rel="stylesheet" href="<?php echo base_url() . 'module/CircleHover/css/common_circlehover.css'; ?>"/>
<link rel="stylesheet" href="<?php echo base_url() . 'module/CircleHover/css/style_circlehover.css'; ?>"/>
<style type="text/css">

    .nivo-caption{
        left:5%;
        bottom:10%;
        height:80%;
        width:30%;
        opacity:0.5;	
    }

</style>
<div class="warper">
    <div class="row" style="margin-top:70px">
        <div class="span12">

            <div class="slider-wrapper theme-default boxShadow" >
                <div id="slider" class="nivoSlider">
                    <img src="<?php echo base_url() . 'asset/img/5.jpg' ?>" alt="" title="occasionally circumstances occur "/>
                    <img src="<?php echo base_url() . 'asset/img/2.jpg' ?>" alt=""  title="ccusamus et iusto odio dignissimos "/>

                </div>
            </div>
        </div>

    </div><!-- end row-->


    <div class="content" style="margin-top:0" >


        <div class="row marginTop40" >
            <div class="span7 offset1" >
                <table id="news" class="table table-hover" style="border: 1px solid #dddddd;">
                    <thead>
                        <tr style="background:#EE783A;color:white">
                            <td colspan="2">ข่าวและประกาศ <a  href="#subscribe" class="pull-right cursor-pointer label label-warning"  role="button" class="btn" data-toggle="modal" >Subscribe</a></td> 
                        </tr>
                    </thead>
                    <tbody  style="display: block;height: 250px;overflow-y: scroll;">
                        <?php
                        foreach ($news as $n) {
                            echo '<tr>';
                            echo '<td>' . date('d/m/Y', strtotime($n->timestamp)) . '</td>';
                            echo '<td class="span12" onClick="viewNews(\'' . $n->id . '\')">' . $n->title . '</td>';
                            echo '</tr>';
                        }
                        if (count($news) == 0) {
                            echo '<tr>';
                            echo '<td colspan="2" class="span12" style="text-align: center; color: red">(ยังไม่มีข่าว)</td>';
                            echo '</tr>';
                        }
                        ?>                    
                    </tbody>
                </table>
            </div>
            <div id="subscribe" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">อีเมล์ที่ต้องการรับข่าวสาร</h3>
              </div>
              <form action="/member/subscribe" method="post" id="subscribe_form" onsubmit="return false"> 
                  <div class="modal-body">
                        <label for="email">อีเมล์ <span class="text-error"> * </span></label>
                        <input type="email"  id="email" name="email" required="required" /> <span id="msg"></span>
                    
                  </div>
                  <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">ยกเลิก</button>
                    <button class="btn btn-main" id="submit">ยืนยัน</button> 
                  </div>
              </form>
            </div>
            <div class="span3">
                <ul class="thumbnails">
                    <li class="span3">
                        <a href="#" class="thumbnail">
                            <img data-src="holder.js/300x200" alt="" src="http://www.iiacanadanationalconference.com/wp-content/uploads/2013/01/test.jpg">
                        </a>
                    </li>

                </ul>
            </div>
        </div><!-- end row-->



    </div>
    <div class="row" > 
        <div class="span12">
            <center>

                <div id='menu'>
                    <!-- dock menu-->
                    <!--
                    
                    <img src='http://images.wikia.com/browserwars/images/c/cb/Chrome_2nd_browser_war_(older_logo).png' title='Favourites' alt='' />
                    <img src='http://images.wikia.com/browserwars/images/c/cb/Chrome_2nd_browser_war_(older_logo).png' title='Pictures' alt='' />
                    <img src='http://images.wikia.com/browserwars/images/c/cb/Chrome_2nd_browser_war_(older_logo).png' title='Music' alt='' />
                    <img src='http://images.wikia.com/browserwars/images/c/cb/Chrome_2nd_browser_war_(older_logo).png' title='Videos' alt='' />
                    <img src='http://images.wikia.com/browserwars/images/c/cb/Chrome_2nd_browser_war_(older_logo).png' title='Uploads' alt='' />-->
                    <!-- Hover Effect -->
                    <ul class="ch-grid">
                        <li>
                            <div class="ch-item ch-img-1">
                                <div class="ch-info">
                                    <h3>Use what you have</h3>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="ch-item ch-img-2">
                                <div class="ch-info">
                                    <h3>Common Causes of Stains</h3>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="ch-item ch-img-3">
                                <div class="ch-info">
                                    <h3>Pink Lightning</h3>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="ch-item ch-img-4">
                                <div class="ch-info">
                                    <h3>Pink Lightning</h3>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div><!-- /div menu -->
            </center>
        </div><!-- /div span12-->

    </div><!-- end row-->
</div>


<input type="hidden" id="url" value="<?php echo $url; ?>" />
<div class="footer">
    <div class="footerBar">
        <div class="container ">
            <div class="row marginTop40">    
                <div class="span3 offset1" >
                    Contact us:	
                </div>
                <div class="span3 " >
                    Contact us:	
                </div>
                <div class="span3 " >
                    Contact us:	
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() . 'module/nivo-slider3.2/nivo-slider/jquery.nivo.slider.pack.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'module/CircleHover/js/modernizr.custom.79639.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.min.js'; ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#slider').nivoSlider({
            effect: 'fade',
            manualAdvance: false,
            directionNav: true, // Next & Prev navigation
            controlNav: false
        });

        $('.bx-default-pager').remove();
        $.pnotify.defaults.delay = 2500;
        $.pnotify.defaults.history = false;
        $.pnotify.defaults.animation_speed = 'normal';
		
		
		$('#email').on('change',function(e) {  
			var email = $(this).val();
             $.ajax({ 
				type: 'POST',
				url: '/member/check_subscribe',
				data: {email: email},
				success: function(data) {
					if(data!=0){
						$('#msg').html('<span class="text-error">อีเมล์นี้มีผู้ใช้แล้ว !</span>');
						$('#subscribe_form').attr('onsubmit','return false');
						$(this).focus(); 
					}else{
						$('#msg').html('<span class="text-success">อีเมล์ใช้งานได้ </span>');
						$('#subscribe_form').attr('onsubmit','return true');  
					}
			 	}
			 });
		});
    });
    
    function viewNews(id) {
        loadPage('/member/news/' + id);
    }
</script>