<!-- view Manage -->
<link rel="stylesheet" href="<?php echo base_url() . 'module/DataTables/css/demo_table.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'asset/js/tagsinput2.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.default.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'module/Bootstrap Form Helpers/css/bootstrap-formhelpers.min.css'; ?>" />
<style>
    .controls .checkbox {
        width: 7em;
    }
	 
	#table_history_log table{
		width:100% !important; 	
	} 
	
	#logbook{
		width:660px;
		margin-left:-330px; 
	}
	.controls .checkbox {
    	width: 13em;
	}
</style>
<div class="warper" style="margin-top: 30px;padding:0 15px;">
    <div class="row-fluid">
        <div class="span8">
            <div class="content" style="padding-top:0;min-height:500px">
                <div class="title">
                    <center>
                        บันทึกการทดลอง
                    </center>
                </div>
                <div class="row-fluid" style="margin-top:20px">
                    <div class="span2"> <span class="pull-right" style="font-size: 18px;margin-top: 8px;">เลือกอุปกรณ์ :</span> </div>
                    <div class="span10">
                        <div class="bfh-selectbox" data-name="equipment" data-value="12" data-filter="true"> 
                            <div data-value="null">-- เลือกอุปกรณ์ --</div>
                            <?php
                            foreach ($equipments as $e) {
                                echo '<div data-value="' . $e->equipment_id . '">' . $e->name . ' - ' . $e->room_name .
                                ' <span>(' . $e->tag . ')</span></div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <hr  style="margin:10px 0"/>
                <div class="row-fluid">
                    <div class="span12" id="logbook_div">
                        <div class="row-fluid center" >
                            <div class="span7 logbook_bar"></div>
                            <div class="span4 offset1"> 
                                <a class="btn pull-right" href="PHPToExcel/export_booking_excel">ดาวน์โหลดบันทึก <i class="icon-download-alt"></i></a>
                                <a class="btn pull-right" href="#logbook" role="button" data-toggle="modal">แก้ไขหัวข้อ <i class="icon-edit"></i></a>
                            </div> 
                        </div>
                        <div class="row-fluid"> 
                            <div class="span12">
                                <div id="table_history_log" class="center">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </thead> 
                                        <tbody>
                                            <tr>
                                                <td></td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="content" style="padding-top:0;">
                <div class="title">
                    <center>
                        หัวข้อบันทึกการทดลองทั้งหมด 
                    </center>
                </div>
                <div id="table_logbook" class="center"> 
                	<div class="text-error" style="margin-top:10px"> *หากไม่มีหัวข้อบันทึกการทดลองที่คุณต้องการสามารถเพิ่มได้ทีนี้ </div>
                    <table class="table table-condensed" style="margin-top:20px" >
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>หัวข้อ (อังกฤษ)</td>
                                <td>หัวข้อ (ไทย)</td>
                                <td>ชนิดข้อมูล</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($columns as $name => $col) {
                                echo '<tr data-col-name="' . $name . '">';
                                echo '<td>' . $count++ . '</td>';
                                echo '<td>' . $name . '</td>';
                                echo '<td>' . $col['thai_name'] . '</td>';
                                echo '<td>' . ($col['type'] == 'int(11)' ? 'ตัวเลข' : ($col['type'] == 'varchar(255)' ? 'ข้อความ' : 'วัน/เวลา')) . '</td>';
                                echo '<td><a href="javascript:;" onClick="deleteColumn(\'' . $name . '\')"><i class="icon-trash"></i></a></td>';
                                echo '</tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <br />
                <hr/>
                <div class="center">
                    <button class="btn btn-main btn-block" id="add_logbook">เพิ่มหัวข้อ</button>
                    <form  style="margin-top:20px" id="form_logbook">
                        <label for="room-name">ชื่อหัวข้อภาษาอังกฤษ (เช่น remark)</label>
                        <input type="text" class="input-block-level" id="col_name2" placeholder="ex. remark">
                        <label for="room-name">ชื่อหัวข้อภาษาไทย (เช่น หมายเหตุ)</label>
                        <input type="text" class="input-block-level" class="form-control" id="col_thai_name2" placeholder="ex. หมายเหตุ">
                        <label for="room-name">ชนิดของข้อมูล</label>
                        <select id="col_type2" class="input-block-level">
                            <option>-- เลือกชนิดข้อมูล --</option>
                            <option value="int">ตัวเลข</option>
                            <option value="varchar">ข้อความ</option>
                            <option value="datetime">วัน/เวลา</option>
                        </select>
                        <center>
                            <a class="btn btn-main" href="javascript:;" style="width:60px" onClick="addColumn()">เพิ่ม</a> <a class="btn " href="javascript:;" id="cancel_add_log">ยกเลิก</a>
                        </center>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">แบบฟอร์มเพิ่มคอลัมน์</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-condensed">
                            <tr style="border-top: none">
                                <td style="text-align: right; font-weight: bold; vertical-align: middle; border-top: none">ชื่อหัวข้อภาษาอังกฤษ (เช่น remark):</td>
                                <td style="vertical-align: middle; border-top: none"><input type="text" class="form-control" id="col_name" placeholder="ex. remark"></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight: bold; vertical-align: middle">ชื่อหัวข้อภาษาไทย (เช่น หมายเหตุ):</td>
                                <td style="vertical-align: middle"><input type="text" class="form-control" id="col_thai_name" placeholder="ex. หมายเหตุ"></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight: bold; vertical-align: middle">ชนิดของข้อมูล:</td>
                                <td style="vertical-align: middle"><select class="form-control" id="col_type">
                                        <option value="no">-- เลือกชนิดข้อมูล --</option>
                                        <option value="int">ตัวเลข</option>
                                        <option value="varchar">ข้อความ</option>
                                        <option value="datetime">วัน/เวลา</option>
                                    </select></td>
                            </tr>
                        </table>
                        <input type="hidden" id="col_eq" value="" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                        <button type="button" class="btn btn-main" id="col_add">เพิ่ม</button>
                    </div>
                </div>
                <!-- /.modal-content --> 
            </div>
            <!-- /.modal-dialog --> 
        </div>
        <!-- /.modal --> 

        <!-- Modal logbook  -->
        <div id="logbook" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>แก้ไขหัวข้อ</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-update-column">
                    <div class="control-group">
                        <label class="control-label" for="equipment-name">ชื่ออุปกรณ์</label>
                        <div class="controls" id="equipment-name"></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="equipment-room">บันทึกที่ต้องการเก็บ</label>
                        <div class="controls" id="equipment-data">

                        </div>
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">ยกเลิก</button>
                <a href="javascript:;" onClick="update_column()" class="btn btn-main">บันทึก</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/bootbox.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/typeahead.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'module/DataTables/js/jquery.dataTables.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'module/Bootstrap Form Helpers/js/bootstrap-formhelpers.min.js'; ?>"></script> 
<script type="text/javascript">
                    var historyTable;
                    var first = true;

                    $(function() {
                        $.pnotify.defaults.delay = 2500;
                        $.pnotify.defaults.history = false;
                        $.pnotify.defaults.animation_speed = 'normal';
                        $('body').find('.container').removeClass('container').addClass('container-fluid');
						 
						$('#logbook_div').addClass('hide');		 
                        $('.bfh-selectbox').on('change.bfhselectbox', function() {
                            var id = $(this).val();
                            var text = $(this).find('li a[data-option="' + id + '"]').text();
                            var columns = 0;

                            if (id == 'null') {
                                return false;
                            }else if(id == ''){
								$('#logbook_div').addClass('hide');					
							}else{ 
								$('#logbook_div').removeClass('hide');					
							}	 
							

                            $('.logbook_bar').html(text);
                            $('#equipment-name').html(text);

                            $('#table_history_log thead tr').text('');
                            $('#table_history_log tbody').text('');

                            historyTable.fnClearTable();

                            $.getJSON('/admin/get_columns/' + id, function(data) {
                                $.each(data, function(key, val) {
                                    $('#table_history_log thead tr').append('<td>' + val + '</td>');
                                    columns++;
                                });

                                $.getJSON('/admin/get_log_books/' + id, function(data) {
                                    $.each(data, function(key, val) {
                                        var str = '<tr>';
                                        $.each(this, function(k, v) {
                                            str += '<td>' + v + '</td>';
                                        });
                                        str += '</tr>';
                                        $('#table_history_log tbody').append(str);
                                    });

                                    if (data == '') {
                                        $('#table_history_log tbody').html('<tr><td style="text-align: center" colspan="' + columns + '">(No data)</td></tr>');
                                        historyTable = $('#table_history_log table').dataTable();
                                    } else {
                                        historyTable = $('#table_history_log table').dataTable({
                                            "bPaginate": true,
                                            "bLengthChange": false,
                                            "bFilter": true,
                                            "bSort": true,
                                            "bInfo": true,
                                            "bAutoWidth": true,
                                            'iDisplayLength': 20,
                                            'bDestroy': true 
                                        });
                                    }
                                });
                            });

                            $.get('/admin/detail_item_json/' + id, function(data) {
                                $('#equipment-data').html(data);
                            });
                        });
                    });

                    if ($('#table_logbook table tbody tr').length >= 1) {
                        $('#table_logbook table').dataTable({
                            "bPaginate": true,
                            "bLengthChange": false,
                            "bFilter": false,
                            "bSort": true,
                            "bInfo": false,
                            "bAutoWidth": true,
                            'iDisplayLength': 5,
                        });
                    }

                    if ($('#table_history_log table tbody tr').length >= 1) {
                        historyTable = $('#table_history_log table').dataTable({
                            "bPaginate": true,
                            "bLengthChange": false,
                            "bFilter": true,
                            "bSort": true,
                            "bInfo": true,
                            "bAutoWidth": true,
                            'iDisplayLength': 20
                        });
                    }

                    $("#form_logbook").hide();


                    $("#add_logbook").click(function() {
                        $(this).slideUp();
                        $("#form_logbook").slideToggle();

                    });

                    $("#cancel_add_log").click(function() {
                        $("#add_logbook").slideDown();
                        $("#form_logbook").slideUp();
                    });

                    $('body').on('click', 'input.other', function() {
                        var index = $('input.other').index($(this));
                        var res = $('input.other:eq(' + $('input.other').index($(this)) + ')').val().split(',');
                        $('#col_name').val(res[0]);
                        $('#col_thai_name').val(res[1]);
                        $('#col_type').val(res[2]);
                        $('#col_eq').val(index);
                        $('#myModal').modal('show');
                    });

                    $('#myModal').on('shown.bs.modal', function() {
                        $('#col_name').focus();
                    });

                    $('#col_add').click(function() {
                        $('#myModal').modal('hide');
                        $('input.other:eq(' + $('#col_eq').val() + ')').val($('#col_name').val() + ',' + $('#col_thai_name').val() + ',' + $('#col_type').val());
                    });


                    function deleteColumn(name) {
                        var c = confirm('คุณต้องการลบหัวข้อบันทึกการทดลอง "' + name + '" จริงหรือไม่?');
                        if (!c) {
                            return false;
                        }

                        $.get('/admin/delete_column/' + name, function(data) {
                            if (data == 1) {
                                $('tr[data-col-name="' + name + '"]').fadeOut('slow', function() {
                                    var pos = historyTable.fnGetPosition(this);
                                    historyTable.fnDeleteRow(pos);
                                    $.pnotify({
                                        title: 'การดำเนินการ',
                                        text: 'ลบหัวข้อบันทึกการทดลองเรียบร้อย!',
                                        type: 'success'
                                    });
                                });
                            } else {
                                $.pnotify({
                                    title: 'การดำเนินการ',
                                    text: 'เกิดข้อผิดพลาดระหว่างการลบหัวข้อบันทึกการทดลอง!',
                                    type: 'error'
                                });
                            }
                        });
                        return false;
                    }

                    function update_column() {
                        $.post('/admin/update_column', $('#form-update-column').serialize(), function(data) {
                            if (data == 0) {
                                alert('การแก้ไขข้อมูลผิดพลาด โปรดลองใหม่อีกครั้ง');
                            } else {
                                alert('แก้ไขข้อมูลเสร็จสิ้น !');
                                $('.modal-backdrop').hide();
                                manage();
                            }
                        });
                    }


                    function addColumn() {
                        var name = $('#col_name2').val();
                        var thaiName = $('#col_thai_name2').val();
                        var type = $('#col_type2').val();

                        if (name == '' || thaiName == '' || type == 'no') {
                            $.pnotify({
                                title: 'การดำเนินการ',
                                text: 'กรุณากรอกข้อมูลให้ครบถ้วน!',
                                type: 'error'
                            });
                        }

                        var value = name;
                        if (value != '') {
                            var english = /^[A-Za-z0-9]*$/;
                            if (!english.test(value)) {
                                $.pnotify({
                                    title: 'การดำเนินการ',
                                    text: 'ชื่อของคอลัมน์ที่เป็นภาษาอังกฤษผิดพลาด!',
                                    type: 'error'
                                });
                                error++;
                                return false;
                            }

                            if (value != value.toLowerCase()) {
                                $.pnotify({
                                    title: 'การดำเนินการ',
                                    text: 'ชื่อคอลัมน์ต้องเป็นตัวอักษรพิมพ์เล็กเท่านั้น!',
                                    type: 'error'
                                });
                                error++;
                                return false;
                            }
                        }

                        $.post('/admin/add_column', {name: name, thai_name: thaiName, type: type}, function(data) {
                            if (data == 1) {
                                $.pnotify({
                                    title: 'การดำเนินการ',
                                    text: 'เพิ่มหัวข้อบันทึกการทดลองเรียบร้อย!',
                                    type: 'success'
                                });
                                manage();
                            } else {
                                $.pnotify({
                                    title: 'การดำเนินการ',
                                    text: 'เกิดข้อผิดพลาดระหว่างการเพิ่มหัวข้อบันทึกการทดลอง!',
                                    type: 'error'
                                });
                            }
                        });
                    }
</script>