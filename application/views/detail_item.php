<!-- detail_item -->
<style>
#logbook {
	width: 640px;
	margin-left: -320px;
}
#myTab .active a {
	background: #D5F0CD;
}
.nav-tabs {
	border-bottom: 3px solid #ddd;
}
</style>
<div class="warper">
  <div class="content">
    <div class="row">
      <div class="span10 offset1">
        <ul class="breadcrumb">
          <li><a href="javascript:;" onClick="equipment()" >อุปกรณ์</a> <span class="divider">/</span></li>
          <li class="active"><? echo $detail->name;?></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="span3 offset1"> <img src="<?php echo base_url() . $detail->equipment_image_path; ?>" class="img-polaroid"> </div>
      <div class="span7">
        <h3><? echo $detail->name;?> <span class="label label-warning" style="margin-top:8px">S/N.<?php echo $detail->serial_no; ?></span> <!--<a class="btn pull-right" href="#delete" role="button" class="btn" data-toggle="modal">ลบอุปกรณ์ <i class="icon-trash"></i></a> <a class="btn pull-right" href="#info" role="button" class="btn" data-toggle="modal">แก้ไขข้อมูล <i class="icon-info-sign"></i></a> 
          <a class="btn pull-right" href="#logbook" role="button" class="btn" data-toggle="modal">แก้ไขบันทึกการทดลอง <i class="icon-edit"></i></a>-->   
        </h3> 
        <table class="table">
        	<tr>
            	<td class="span2">อุปกรณ์ด้าน  </td>
                <td><?php echo $detail->type; ?></td>
            </tr>
            <tr> 
            	<td>ห้อง</td>
                <td><?php echo $detail->room; ?>  <a style="cursor:pointer" href="#changeRoom" role="button"  data-toggle="modal" alt="Change room"><i class="icon icon-exchange"></i></a></td>  
            </tr> 
            <tr>
            	<td>ผู้รับผิดชอบ</td>
                <td><?php echo $detail->person_responsible; ?></td>
            </tr>   
            <tr>
            	<td>สภาพครุภัณฑ์</td>
                <td><span class="label label-success"><?php echo $detail->status == 0 ? 'ปกติ' : 'ซ่อมบำรุง'; ?></span></td>
            </tr>
        </table>
         
      </div>
    </div>
    <div class="row marginTop40">
      <div class="span10 offset1">
        <ul class="nav nav-tabs" id="myTab" >
          <li class="active"> <a href="#detail" >รายละเอียดครุภัณฑ์</a> </li>
          <li><a href="#responsibility">ผู้รับผิดชอบ</a></li>
          <li><a href="#buyer">การจัดซื้อ</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="detail">
            <table class="table table-bordered">
              <tr>
                <td>รหัสทรัพย์สิน</td>
                <td><?php echo $detail->asset_id; ?></td>
              </tr>
              <tr>
                <td>Serial Number</td>
                <td>S/N.<?php echo $detail->serial_no; ?></td>
              </tr>
              <tr>
                <td>ชื่อทรัพยสิน</td>
                <td><?php echo $detail->name; ?></td>
              </tr>
              <tr>
                <td>ชื่อตั้งงบประมาณ</td>
                <td><?php echo $detail->budget_name; ?></td>
              </tr>
              <tr>
                <td>ยี่ห้อ</td>
                <td><?php echo $detail->brand; ?></td>
              </tr>
              <tr>
                <td>รุ่น</td>
                <td><?php echo $detail->model; ?></td>
              </tr>
              <tr>
                <td>ประจำที่</td>
                <td><?php echo $detail->place_at; ?></td>
              </tr>
              <tr>
                <td>บันทึก</td>
                <td><?php echo $detail->remark; ?></td>
              </tr>
              <tr>
                <td>ข้อมูลจำเพาะ</td>
                <td><a href="<?php echo base_url() . $detail->specific_info_path; ?>"><i class="icon-file-text"></i></a></td>
              </tr>
            </table>
          </div>
          <div class="tab-pane" id="responsibility">
            <table class="table table-bordered">
              <tr>
                <td>อุปกรณ์ด้าน</td>
                <td><?php echo $detail->type; ?></td>
              </tr>
              <tr>
                <td>ห้อง</td>
                <td><?php echo $detail->room; ?></td>
              </tr>
              <tr>
                <td>สภาพครุภัณฑ์</td>
                <td style="padding-bottom: 10px;"><?php echo $detail->status == 0 ? 'ปกติ' : 'ซ่อมบำรุง'; ?></td>
              </tr>
              <tr>
                <td>ผู้รับผิดชอบ</td>
                <td><?php echo $detail->person_responsible; ?></td>
              </tr>
              <tr>
                <td>ชื่อบริษัทที่ซื้อ</td>
                <td><?php echo $detail->seller_name; ?></td>
              </tr>
              <tr>
                <td>หมายเลขโทรศัพท์</td>
                <td><?php echo $detail->seller_tel_no; ?></td>
              </tr>
              <tr>
                <td>ที่อยู่</td>
                <td><?php echo $detail->seller_address; ?></td>
              </tr>
              <tr>
                <td>หน่วยงานผู้รับผิดชอบ</td>
                <td><?php echo $detail->department_responsible_id; ?></td>
              </tr>
              <tr>
                <td>ชื่อหน่วยงานผู้รับผิดชอบ</td>
                <td><?php echo $detail->department_responsible_name; ?></td>
              </tr>
            </table>
          </div>
          <div class="tab-pane" id="buyer">
            <table class="table table-bordered">
              <tr>
                <td>วิธีจัดหา</td>
                <td><?php echo $detail->buying_method; ?></td>
              </tr>
              <tr>
                <td>วันที่รับทรัพย์สิน </td>
                <td><?php echo $detail->received_date; ?></td>
              </tr>
              <tr>
                <td>มูลค่าที่ซื้อ</td>
                <td><?php echo $detail->buying_cost; ?></td>
              </tr>
              <tr>
                <td>กองทุน</td>
                <td><?php echo $detail->funds; ?></td>
              </tr>
              <tr>
                <td>หน่วยงาน</td>
                <td><?php echo $detail->department_id; ?></td>
              </tr>
              <tr>
                <td>ชื่อหน่วยงาน</td>
                <td><?php echo $detail->department_name; ?></td>
              </tr>
              <tr>
                <td>แผนงาน</td>
                <td><?php echo $detail->plan; ?></td>
              </tr>
              <tr>
                <td>โครงการ</td>
                <td><?php echo $detail->project; ?></td>
              </tr>
              <tr>
                <td>วันที่เริ่มคำนวณค่าเสื่อมราคา</td>
                <td><?php echo $detail->date_start_calculate; ?></td>  
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="changeRoom" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">ย้ายอุปกรณ์</h3>
  </div>
  <div class="modal-body">
    ห้อง <select>
    	<option>A01</option>
    	<option>A02</option>
    </select>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">ยกเลิก</button>
    <button class="btn btn-primary">ยืนยัน</button>
  </div>
</div> 

<script>
	$(document).ready(function(e) { 
		
        $( ".tab-content .tab-pane table tr td:even" ).css({
		  "width":"135px",  
		  "font-weight": "bold " 
		});
		
		 $('#myTab a').click(function (e) {
		  e.preventDefault();
		  $(this).tab('show');
		})
	
    });
 
 	$('#add').on('click', function() {
        $('#other_column').append('<div style="margin:10px 0 0 35px"><input type="text" name="logbook_column"/> <a href="javascript:;" id="remove"><i class=" icon-remove none_underline" ></i></a></div> ');
    });

    $(document).on('click', '#remove', function() {
        $(this).parent('div').remove('div');
    });
	
	
function manage_hideModal(){
	$('.modal-backdrop').hide();
	activeMenu("#manageMenu");
	loadPage('/admin/manage');
}

function update_column(){
	 $.post('/admin/update_column', $('#form-update-column').serialize(), function(data) {
                                                if(data==0){
													alert('การแก้ไขข้อมูลผิดพลาด โปรดลองใหม่อีกครั้ง');	
												}else{
													alert('แก้ไขข้อมูลเสร็จสิ้น !');
													$('.modal-backdrop').hide();
													details(data);	
												}
                                            });	
}
</script> 
