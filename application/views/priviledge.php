<!-- view priviledge-->
<link rel="stylesheet" href="<?php echo base_url() . 'module/DataTables/css/demo_table.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'module/loadover/loadover.css'; ?>" />
<style>
    #priviledge_filter,#approved_list_filter{
        margin-right:20px;	
    }
</style>  
<div class="warper">
    <div class="row">
        <div class="span12">
            <div class="content" style="min-height:420px;padding-top:0">
                <div class="title"><center>รายชื่อคนขอสิทธิ์การใช้อุปกรณ์</center></div>
                <table class="table" id="priviledge">

                    <thead>
                        <tr>
                            <th>วันที่ส่งคำขอ</th>
                            <th>รหัสนักศึกษา</th>
                            <th>ชื่อ</th>
                            <th>หลักสูตร</th>
                            <th>อุปกรณ์ที่ขอ</th>
                            <th>ด้าน</th>
                            <th>ตั้งแต่วันที่</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($unapproved_forms as $u) {
                            echo '<tr data-id=' . $u->id . '>';
                            echo '<td >' . $u->d . '</td>';
                            echo '<td>' . ($u->student_id == '0' ? '(ไม่มีรหัสนักศึกษา)' : $u->student_id) . '</td>';
                            echo '<td>' . $u->name . '</td>';
                            echo '<td>' . $u->course . '</td>';
                            echo '<td>' . $u->items . '</td>';
                            echo '<td>' . $u->lab . '</td>';
                            echo '<td>' . $u->date . '</td>';
                            echo '</tr>';
                        }

                        if (count($unapproved_forms) == 0) {
                            echo '<tr>';
                            echo '<td style="text-align: center; color: red" colspan="6">(ไม่มีข้อมูล)</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!--end content-->
        </div><!-- end span -->
    </div><!-- end row -->

    <div class="row" id="listApproved">
        <div class="span12">
            <div class="content" style="min-height:420px;padding-top:0;margin-top:0px">
                <div class="title">
                    <center style="padding-top: 15px;">
                        สิทธิ์ของผู้ใช้ปัจจุบันในห้อง &nbsp;<select id="select_approved_list">  <?php
                            foreach ($labs as $l) {
                                echo '<option value="' . $l->id . '" ' . ($lab_name == $l->name ? 'selected' : '') . '>' . $l->name . '</option>';
                            }
                            ?>	</select>
                    </center>
                </div> 
                <table class="table  marginTop20" id="approved_list">
                    <thead>
                        <tr>
                            <th>รหัสนักศึกษา</th>
                            <th>ชื่อ</th>
                            <th>อุปกรณ์ที่ขอ</th>
                            <th>ด้าน</th>
                            <th>ตั้งแต่วันที่</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($approved_forms as $r) {
                            echo '<tr data-id="' . $r->id . '">';
                            echo '<td>' . ($r->student_id == '0' ? '(ไม่มีรหัสนักศึกษา)' : $r->student_id) . '</td>';
                            echo '<td>' . $r->name . '</td>';
                            echo '<td>' . $r->items . '</td>';
                            echo '<td>' . $r->lab . '</td>';
                            echo '<td>' . $r->date . '</td>';
                            echo '</tr>';
                        }

                        if (count($approved_forms) == 0) {
                            echo '<tr>';
                            echo '<td style="text-align: center; color: red" colspan="5">(ไม่มีข้อมูล)</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>


            </div><!-- end content -->


        </div><!-- end span-->
    </div>
</div>
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'module/DataTables/js/jquery.dataTables.js'; ?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'module/loadover/loadover.js'; ?>"></script>
<script>
    $(document).ready(function(e) {

        var active_name = $('.title li a').text();
        if ($('#leftMenu li a').text() == active_name) {
            $(this).addClass('nav_active disabled');
        }


        if ($('#priviledge tbody tr').length > 1) {
            $('#priviledge').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                'iDisplayLength': 10,
                "aaSorting": [[0, "desc"]]
            });
        }

        if ($('#approved_list tbody tr').length > 1) {
            $('#approved_list').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                'iDisplayLength': 10,
            });


        }


        $('#priviledge tbody').on("click", "tr[class!='acceptMenu']", function() {
            var id = $(this).attr("data-id");
            var html = '<tr class="acceptMenu"><td colspan="7"><button class="btn pull-right span1" style="margin-left: 10px" onClick="cancel(\'' + id + '\')">ไม่ให้สิทธิ์</button><button class="btn btn-main span2 pull-right" onClick="approve(\'' + id + '\')"><i class="icon-ok"></i> ยืนยันสิทธิ์</button></td></tr>';
            if (!$(this).next().hasClass("acceptMenu")) {
                $(html).insertAfter(this);
            } else {
                $(this).next().remove();
            }
        });

        $('#approved_list tbody').on("click", "tr[class!='cancelMenu']", function() {
            var id = $(this).attr("data-id");
            var html = '<tr class="cancelMenu"><td colspan="6"><button class="btn pull-right span1" style="margin-left: 10px" onClick="unapprove(\'' + id + '\')">ถอนสิทธิ์</button></td></tr>';
            if (!$(this).next().hasClass("cancelMenu")) {
                $(html).insertAfter(this);
            } else {
                $(this).next().remove();
            }
        });

        $('#select_approved_list').change(function(e) {
            loadApproved($(this).val());
        });


    });

    function loadApproved(id) {
        priviledge(id);
    }

    function approve(id) {
        $('body').loadOverStart();
        $.post('/admin/request_approve', {id: id}, function(data) {
            $('body').loadOverStop();
            priviledge(data);
        });
    }

    function unapprove(id) {
        $('body').loadOverStart();
        $.post('/admin/request_unapprove', {id: id}, function(data) {
            $('body').loadOverStop();
            priviledge(data);
        });
    }

    function cancel(id) {
        var c = confirm('คุณต้องการลบการขอสิทธิ์นี้หรือไม่?');
        if (c) {
            $('body').loadOverStart();
            $.post('/admin/request_cancel', {id: id}, function(data) {
                $('body').loadOverStop();
                priviledge(data);
            });
        }
    }
</script>