<div class="row-fluid"> 
	<form class="form-inline">
      <div class="span4 offset1">
        <label>ด้านปฎิบัติการณ์</label>
        <select>
            <option>เชื้อรา</option>
            <option>สาหร่าย</option>
            <option>น้ำ</option>  
        </select>
      </div> 
      <div class="span4"> 
        <label>ห้อง</label>
        <select>
            <option>A01</option>
            <option>A02</option>
            <option>A03</option>
        </select>
      </div>
  </form>
  <div class="span12">
    <div id="total_time_per_equip" style="height: 400px; margin: 0 auto"></div>
  </div>
  <div class="span12">
  <hr />     
  </div> 
  <div class="span4 offset1"> 
      <form class="form-inline">
        <label>ปี&nbsp;</label>  
        <select>
            <option>2014</option>
            <option>2013</option>
            <option>2012</option>
        </select>
      </form>
  </div>
  <div class="span12">
    <div id="total_time_per_year" style="height: 400px; margin: 0 auto"></div>
  </div>
</div>
<script>
 	 $('#total_time_per_equip').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Total time of equipment'
            },
            xAxis: {
                categories: [
                    'equipmentname1',
                    'equipmentname2',
                    'equipmentname3', 
					'equipmentname4', 
					'equipmentname5', 
                ]
            },
            yAxis: { 
                min: 0,
                title: {
                    text: 'Total time (Hr)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                 showInLegend: false,        
                data: [49, 13, 10,22,5]
    		}]
     });
	 
	  $('#total_time_per_year').highcharts({
            chart: { 
                type: 'column'
            },
            title: {
                text: 'Total time of every equipment per year'
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
					'April',
					'May', 
					'Jun', 
					'Jul',
					'Aug',
					'Sep',
					'Oct',
					'Nov',
					'Dec' 
                ]
            },
            yAxis: { 
                min: 0,
                title: {
                    text: 'Total Time (Hr)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                  showInLegend: false,       
                data: [49, 13, 10,22,5,10,15,11,55,20,11,35]
    		}]
     });
 
 </script>