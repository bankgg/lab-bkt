<div class="warper" > 	
    <div class="content" id="news_content" style="padding-top:0;padding-bottom:0">
        <div class="title"><center>แก้ไขข่าว</center></div> 
        <div class="row" id="editForm">

            <div class="span12" style="padding-bottom:0">

                <form class="form-horizontal well" style="margin-bottom:0" onSubmit="return editNews();">
                    <div class="control-group">
                        <label class="control-label" for="">หัวข้อ</label>
                        <div class="controls" >
                            <input type="text" class="span5" name="news_title" id="news_title" value="<?php echo $title; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="">เนื้อหา</label>
                        <div class="controls" >
                            <textarea rows="12" name="news_content" class="span7"></textarea>
                        </div>
                    </div>   
                    <div class="control-group">
                        <div class="controls" >
                            <input type="submit" class="btn btn-main span2" value="แก้ไข!" />
                            <input type="button" class="btn btn-default span2" value="ยกเลิก" onClick="addNews()" />
                        </div>
                    </div>
                    <input type="hidden" name="member_id" id="member_id" value="<?php echo $member_id; ?>" />
                    <input type="hidden" name="news_id" id="news_id" value="<?php echo $id; ?>" />
                </form> 
            </div>
        </div>
    </div>

</div>
<?php
$output = str_replace(array("\r\n", "\r"), "\n", $content);
$lines = explode("\n", $output);
$new_lines = array();

foreach ($lines as $i => $line) {
    if (!empty($line))
        $new_lines[] = trim($line);
}
$new_content = implode($new_lines);
?>
<script type="text/javascript">
    $(document).ready(function(e) {
        tinymce.init({
            selector: "textarea",
            theme: "modern",
            skin: "light",
            height: 300,
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor jbimages"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",
            toolbar2: "print preview media | forecolor backcolor emoticons",
            image_advtab: true
        });
        tinyMCE.activeEditor.setContent('<?php echo $new_content; ?>');
    });

    function editNews(id) {
        if ($.trim($('input[name="news_title"]').val()).length == 0 || $.trim(tinyMCE.activeEditor.getContent()).length == 0) {
            $.pnotify({
                title: 'การแจ้งเตือน',
                text: 'กรุณากรอกข้อมูลให้ครบถ้วน!',
                type: 'error'
            });
            return false;
        }
        var val = $('form').serialize();
        val += '&news_content=' + encodeURIComponent(tinyMCE.activeEditor.getContent());
        $.post('/admin/submit_edit_news', val, function(msg) {
            if (msg != 0) {
                $.pnotify({
                    title: 'การแจ้งเตือน',
                    text: 'แก้ไขข่าวสำเร็จ!',
                    type: 'success'
                });
                addNews();
            } else {
                $.pnotify({
                    title: 'การแจ้งเตือน',
                    text: 'เกิดข้อผิดพลาดระหว่างการแก้ไขข่าว!',
                    type: 'error'
                });
            }
        });
        return false;
    }
</script>