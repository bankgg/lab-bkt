<style>
.nav-tabs li a span { 
	font-size: 20px;
	float: right;
	margin-top: 20px;
}
.content {
	min-height: 900px;
}
.highcharts-container{
	width:500px; 
}
</style> 
        
<script src="../../module/Highcharts/js/highcharts.js"></script> 
<script src="../../module/Highcharts/js/modules/exporting.js"></script>  
<div class="warper">
  <div class="row-fluid">
    <div class="span3">
      <ul class="nav nav-tabs nav-stacked boxShadow" id="dashboardMenu"  style="background:#F2F2F2;">
        <li class="nav_active disabled"><a href="#" onclick="total_requested()"><i class="icon-user icon-4x"></i><span>จำนวนคนขอใช้สิทธิ์</span></a></li>
        <li><a href="#" onclick="total_people()"><i class="icon-group icon-4x"></i><span>จำนวนคนใช้</span></a></li> 
        <li><a href="#" onclick="total_time()"><i class="icon-time icon-4x"></i><span>เวลาในการใช้</span></a></li>
        <li><a href="#" onclick="maintenance()"><i class="icon-wrench icon-4x"></i><span>ซ่อมบำรุง</span></a></li> 
      </ul>
    </div> 
    <div class="span9">
      <div class="content">
       		
      </div> 
    </div>
  </div>
</div>
<script>
	$(function(){		
		$('body').find('.container').removeClass('container').addClass('container-fluid'); 
		total_requested();
	});
	
	function total_requested(){		
		loadContent('/member/total_requested');
	} 
	
	function total_people(){
		loadContent('/member/total_people');
	}  
	
	function total_time(){
		loadContent('/member/total_time');
	} 
	
	function maintenance(){
		loadContent('/member/maintenance');
	} 
	
	$('#dashboardMenu li a').click(function(e) {
        $(this).parent().addClass('nav_active disabled').siblings().removeClass('nav_active disabled');	
    });
			 
	function loadContent(page) {    
   	 	$('.content').load(page);
	} 
	
</script>