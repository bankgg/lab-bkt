<!DOCTYPE html >
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="maximum-scale=0.8,  user-scalable=yes" />
        <meta charset="utf-8" />
        <title>เข้าสู่ระบบ</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/bootstrap.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/bootstrap-responsive.min.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'module/font-awesome/css/font-awesome.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/style.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/ui-lightness/jquery-ui-1.10.3.custom.min.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.default.css'; ?>" />
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="change-password-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">แบบฟอร์มเปลี่ยนรหัสผ่าน</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-condensed">
                            <tr style="border-top: none">
                                <td style="text-align: right; font-weight: bold; vertical-align: middle; border-top: none">รหัสผ่านใหม่:</td>
                                <td style="vertical-align: middle; border-top: none"><input type="password" class="form-control" id="new-password" required="required"></td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-weight: bold; vertical-align: middle">ยืนยันรหัสผ่านใหม่:</td>
                                <td style="vertical-align: middle"><input type="password" class="form-control" id="confirm-new-password" required="required"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                        <button type="button" class="btn btn-primary" id="change-submit">เปลี่ยน</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <input type="hidden" id="key" value="<?php echo $key; ?>" />
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery-ui-1.10.3.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/lab.js'; ?>"></script> 
        <script type="text/javascript" src="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.min.js'; ?>"></script>
        <script type="text/javascript">
            $(function() {
                $.pnotify.defaults.delay = 2500;
                $.pnotify.defaults.history = false;
                $.pnotify.defaults.animation_speed = 'normal';

                $('#change-password-modal').modal('show');

                $('#change-password-modal').on('shown.bs.modal', function() {
                    $('#new-password').focus();
                });

                $('#change-submit').click(function() {
                    if ($('#new-password').val() != $('#confirm-new-password').val()) {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'รหัสผ่านใหม่ทั้งสองช่องไม่ตรงกัน!',
                            type: 'error'
                        });
                        return false;
                    } else {
                        var result = $('#new-password').val();
                        if (result == '') {
                            $.pnotify({
                                title: 'การดำเนินการ',
                                text: 'คุณยังไม่ได้กรอกรหัสผ่าน!',
                                type: 'error'
                            });
                            return false;
                        } else if (result === null) {
                            window.location.href = '/member';
                        } else {
                            $.post('/member/change_password', {key: $('#key').val(), password: result}, function(data) {
                                alert('เปลี่ยนรหัสผ่านสำเร็จ!');
                                window.location.href = '/member';
                            });
                        }
                    }
                });
            });
        </script>
    </body>
</html>