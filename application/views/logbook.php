<!-- view logbook(member view) -->
<link rel="stylesheet" href="<?php echo base_url() . 'module/DataTables/css/demo_table.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'asset/js/tagsinput2.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.default.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'module/Bootstrap Form Helpers/css/bootstrap-formhelpers.min.css'; ?>" />
<style>
    .controls .checkbox {
        width: 7em;
    }
	 
	#table_history_log table{
		width:100% !important; 	
	} 
	
	#logbook{
		width:660px;
		margin-left:-330px; 
	}
	.controls .checkbox {
    	width: 13em;
	}
</style>
<div class="warper" style="padding:0 15px;">
    <div class="row">
        <div class="span12">
            <div class="content" style="padding-top:0;min-height:500px">
                <div class="page-title">
                    <center>
                        บันทึกการทดลอง
                    </center>
                </div>
                <div class="row" style="margin-top:20px">
                    <div class="span2"> <span class="pull-right" style="font-size: 18px;margin-top: 8px;">เลือกอุปกรณ์ :</span> </div>
                    <div class="span9">
                        <div class="bfh-selectbox" data-name="equipment" data-value="12" data-filter="true"> 
                            <div data-value="null">-- เลือกอุปกรณ์ --</div>
                            <?php
                            foreach ($equipments as $e) {
                                echo '<div data-value="' . $e->equipment_id . '">' . $e->name . ' - ' . $e->room_name .
                                ' <span>(' . $e->tag . ')</span></div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <hr  style="margin:10px 0"/>
                <div class="row">
                    <div class="span12" id="logbook_div">
                        <div class="center">
                            <span class="logbook_bar"></span>
                             <a class="btn pull-right" href="PHPToExcel/export_booking_excel" id="export_booking">ดาวน์โหลดบันทึก <i class="icon-download-alt"></i></a>
                        </div>
                        <div class="row" style="margin-top:20px"> 
                            <div class="span12">
                                <div id="table_history_log" class="center">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </thead> 
                                        <tbody>
                                            <tr>
                                                <td></td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/bootbox.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/typeahead.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'module/DataTables/js/jquery.dataTables.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'module/Bootstrap Form Helpers/js/bootstrap-formhelpers.min.js'; ?>"></script> 
<script type="text/javascript">
                    var historyTable;
                    var first = true;

                    $(function() {
                        $.pnotify.defaults.delay = 2500;
                        $.pnotify.defaults.history = false;
                        $.pnotify.defaults.animation_speed = 'normal';
						 
						$('#logbook_div').addClass('hide');		 
                        $('.bfh-selectbox').on('change.bfhselectbox', function() {
                            var id = $(this).val();
							var text = $(this).find('li a[data-option="' + id + '"]').text();
                            var columns = 0;

                            if (id == 'null') {
                                return false;
                            }else if(id == ''){
								$('#logbook_div').addClass('hide');					
							}else{ 
								$('#logbook_div').removeClass('hide');					
							}	 
							

                            $('.logbook_bar').html(text);
                            $('#equipment-name').html(text);

                            $('#table_history_log thead tr').text('');
                            $('#table_history_log tbody').text('');

                            historyTable.fnClearTable();
 
                            $.getJSON('/admin/get_columns_thai/' + id, function(data) {
                                $.each(data, function(key, val) {
                                    $('#table_history_log thead tr').append('<td>' + val + '</td>');
                                    columns++;
                                });

                                $.getJSON('/admin/get_log_books/' + id, function(data) {
                                    $.each(data, function(key, val) {
                                        var str = '<tr>';
                                        $.each(this, function(k, v) {
                                            str += '<td>' + v + '</td>';
                                        });
                                        str += '</tr>';
                                        $('#table_history_log tbody').append(str);
                                    });

                                    if (data == '') {
                                        $('#table_history_log tbody').html('<tr><td style="text-align: center" colspan="' + columns + '">(No data)</td></tr>');
                                        historyTable = $('#table_history_log table').dataTable();
                                    } else {
                                        historyTable = $('#table_history_log table').dataTable({
                                            "bPaginate": true,
                                            "bLengthChange": false,
                                            "bFilter": true,
                                            "bSort": true,
                                            "bInfo": true,
                                            "bAutoWidth": true,
                                            'iDisplayLength': 20,
                                            'bDestroy': true 
                                        });
                                    }
                                });
                            });

                            $.get('/admin/detail_item_json/' + id, function(data) {
                                $('#equipment-data').html(data);
                            });
							 
							$("#export_booking").attr("href","PHPToExcel/export_booking_excel/"+id);
                        });
                    });
 

                    if ($('#table_history_log table tbody tr').length >= 1) {
                        historyTable = $('#table_history_log table').dataTable({
                            "bPaginate": true,
                            "bLengthChange": false,
                            "bFilter": true,
                            "bSort": true,
                            "bInfo": true,
                            "bAutoWidth": true,
                            'iDisplayLength': 20
                        });
                    }


					
</script>