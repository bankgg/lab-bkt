 <div id="total_user" style="height: 400px; margin: 0 auto"></div>
        <div class="row-fluid">
        	<div class="span6 offset3"><div id="total_student" style=" height: 400px; margin: 0 auto"></div></div>
            <div class="span6 offset3"><div id="total_researcher" style="height: 400px; margin: 0 auto"></div> </div>
        </div>
        
<script>
	$(function() {
       
		$('#total_student').hide();
		$('#total_researcher').hide();
		$('#total_user').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false 
        }, 
        title: {
            text: 'Total Number of Requests '
        }, 
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: { 
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            data: [ 
                ['Student',   44.5],
                ['Researcher',      55.5] 
            ],
			 point:{
              events:{
                  click: function (event) {	 
					  if(this.x == 0){
						 $('#total_researcher').hide();
						$('#total_student').show();  								
					  }else if(this.x == 1){ 
						  $('#total_student').hide(); 	
						  $('#total_researcher').show();
					  } 
                  }
              }
          }          
        }]
    });
	
	 $('#total_student').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Total usage of Students'
            },
            subtitle: { 
                text: 'Separate by Institute and Faculty'
            },
            xAxis: {
                categories: [
                    'IT-KMUTT',
                    'CS-KMUTL',
                    'CHE-KMUTNB', 
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total People'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Student',
                data: [49.9, 71.5, 106.4]
    		}]
     });
	 
		$('#total_researcher').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Total usage of Researchers'
            },
            subtitle: {
                text: 'Separate by Lab'
            }, 
            xAxis: {
                categories: [
                    'Lab-A01',
                    'Lab-A02',
                    'Lab-A55',
                    'Lab-B44',
                    'Lab-C65',
                    'Lab-C66',
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total People'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
			colors: [
				'#0D233A',	
				'#0D233A',	
				'#0D233A',	
				'#0D233A',	
				'#0D233A',	
				'#0D233A' 		
				],
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
					 colorByPoint: true
                }
            },
            series: [{ 
                name: 'Researcher',
                data: [49.9, 71.5, 106.4,10.5,110,54.2]
    		}]
        });		
});
</script>