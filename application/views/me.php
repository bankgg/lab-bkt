<!-- me -->
<link rel="stylesheet" href="<?php echo base_url() . 'module/DataTables/css/demo_table.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'asset/js/bootstrap-tagsinput.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'asset/js/tagsinput2.css'; ?>" />
<link rel="stylesheet" href="<?php echo base_url() . 'asset/css/jasny-bootstrap.min.css'; ?>" />
<style>
    #profile tr td {
        font-size: 16px;
        line-height: 44px;
    }
    hr {
        border-top: 2px solid #eeeeee;
        border-bottom: 2px solid #ffffff;
    }
    .tab-content{ 
        overflow:visible;	
    } 
</style>
<div class"warper">
     <div class="content" style="padding-top:0;margin-top: 20px;"> 
        <div class="row" id="detail">
            <div class="span12 well"  style="padding-left: 0;
                 padding-right: 0;">
                 <?php echo form_open_multipart('member/upload_photo', array('id' => 'form-photo-upload')); ?>
                <div class="span3 marginTop30"> 
                    <div class="fileinput fileinput-new pull-right" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 190px;">
                            <img src="<?php echo $member->image_path == '' ? base_url() . 'asset/img/nophoto.png' : base_url() . 'photos/' . $member->image_path; ?>" alt="ไม่มีรูปภาพ" width="100%" height="100%">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                            <span class="btn btn-default btn-file btn-block">
                                <span class="fileinput-new">เปลี่ยนรูปภาพ</span><span class="fileinput-exists">เปลี่ยน</span>
                                <input type="file" name="userfile" id="userfile"></span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">ลบออก</a>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
                <div class="span8">
                    <table id="profile">
                        <tr>
                            <td class="span2"><b>รหัสนักศึกษา</b></td>
                            <td class="span2"><?php echo $member->student_id == '0' ? '(ไม่มีรหัสนักศึกษา)' : $member->student_id; ?></td>
                            <td class="span2"><b>สถานภาพ</b></td>
                            <td class="span2"><?php echo $member->status; ?></td>
                        </tr>
                        <tr>
                            <td class="span2"><b>ชื่อ-นามสกุล</b></td>
                            <td class="span2"><?php echo $member->firstname . ' ' . $member->lastname; ?></td>
                            <td class="span2"><b>เบอร์โทรศัพท์มือถือ</b></td>
                            <td class="span2"><?php echo $member->tel; ?></td>
                        </tr>
                        <tr>
                            <td class="span2"><b>อีเมล์</b></td>
                            <td class="span2"><?php echo $member->email; ?></td>
                            <td class="span2"><b>เบอร์ภายใน</b></td>
                            <td class="span2"><?php echo $member->tel_in; ?></td>
                        </tr>
                        <tr>
                            <td class="span2"><b>คณะ</b></td>
                            <td class="span2"><?php echo $member->faculty; ?></td>
                            <td class="span2"><b>แล็บ</b></td>
                            <td class="span2"><?php echo $member->lab; ?></td>
                        </tr>
                        <tr>
                            <td class="span2"><b>หลักสูตร (สังกัด)</b></td>
                            <td colspan="3" class="span2"><?php echo $member->course; ?></td>
                        </tr>
                        <tr>
                            <td class="span2"><b>มหาวิทยาลัย</b></td>
                            <td colspan="3"><?php echo $member->university; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><button id="change-password" type="button" class="btn btn-default">เปลี่ยนรหัสผ่าน</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- End row-->
        <div class="row">
            <div class="span10 offset1">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#reserved">การจอง</a></li>
                    <li><a href="#request">คำร้องขอใช้อุปกรณ์</a></li>
                    <li><a href="#logbook">บันทึกการทดลอง</a></li>
                </ul>
            </div>
        </div>
        <div class="row" >
            <div class="tab-content">
                <!-- tab 1 -->
                <div class="tab-pane active" id="reserved">
                    <div class="span7 offset1">
                        <table class="table  table-bordered" id="reserved_history">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่ออุปกรณ์</th>
                                    <th>รูปแบบ</th>
                                    <th>ห้อง</th>
                                    <th>วันที่</th>
                                    <th>ช่วงเวลา</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                if (count($booking) == 0) {
                                    echo '<tr><td colspan="6" style="text-align: center; color: red">(ยังไม่เคยจองอุปกรณ์)</td></tr>';
                                }
                                foreach ($booking as $b) {
                                    $add = 0;
                                    if ($b->c > 1) {
                                        for ($j = 1; $j < $b->c; $j++) {
                                            $add += 30;
                                        }
                                    }
                                    echo '<tr data-time="' . (time() < strtotime($b->start) ? $b->timestamp : '') . '">';
                                    echo '<td>' . $i++ . '</td>';
                                    echo '<td><span class="label label-warning">' . $b->equipment_tag . '</span> <a href="javascript:;" onClick="click_booking(\'' . date('d-m-Y', strtotime($b->start)) . '\', ' . $b->room_id . ', \'' . $b->equipment_tag . '\')">' . $b->equipment_name . '</a></td>';
                                    echo '<td>' . $b->type . '</td>';
                                    echo '<td>' . $b->room_name . '</td>';
                                    echo '<td>' . date('Y-m-d', strtotime($b->start)) . '</td>';
                                    echo '<td>' . (date('H:i', strtotime($b->start)) . ' - ' . date('H:i', strtotime($b->end . "+" . $add . " minutes"))) . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                        <!--<button onclick="click_item_schedule()" class="btn pull-right" >จองเพิ่ม</button>--> 
                    </div>
                    <div class="span3">
                        <h4> อุปกรณ์ที่สามารถใช้/จองได้ </h4>
                        <div class="well well-small">
                            <ul class="nav nav-list">
                                <?php
                                if ($types == null) {
                                    echo '<li class="nav-header"><h5 style="color: red">(ไม่มีอุปกรณ์ที่จองได้)</h5></li>';
                                }
                                for ($i = 0; $i < count($types); $i++) {
                                    echo '<li class="nav-header"><h5>ด้าน' . $types[$i] . '</h5></li>';
                                    foreach ($equipments[$i] as $e) {
                                        echo '<li><a href="javascript:;" onClick="click_item_schedule(\'' . rawurlencode($e->name) . '\');">' . $e->name . '</a></li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- tab 2 -->
                <div class="tab-pane" id="request">
                    <div class="span7 offset1">
                        <table class="table  table-bordered" id="request_history">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>อุปกรณ์ที่ขอ</th>
                                    <th>รูปแบบ</th>
                                    <th>ขอเมื่อวันที่</th>
                                    <th>สถานะ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                $round = 0;
                                $eq = '';
                                $tmp_id = '';
                                $total_history = count($request_history);
								if($total_history>0){
									$tmp_id = $request_history[0]->id; 
								}
                                //print_r ($request_history); 
                                if (count($request_history) == 0) {
                                    echo '<tr><td colspan="6" style="text-align: center; color: red">(ยังไม่เคยส่งคำร้อง)</td></tr>';
                                }
                                for ($i = 0; $i < $total_history; $i++) {
                                    $round++;
                                    if ($tmp_id == $request_history[$i]->id) {
                                        $eq .= $request_history[$i]->equipment_name . ", ";
                                        if ($round == $total_history) {
                                            if (substr($eq, -2) == ", ") {
                                                $eq = substr($eq, 0, strlen($eq) - 2);
                                            }
                                            echo '<tr data-id="' . $tmp_id . '">';
                                            echo '<td>' . $count++ . '</td>';
                                            echo '<td class="span8">' . ($eq == '' ? $request_history[$i]->equipment_name : $eq) . '</td>';
                                            echo '<td>' . $request_history[$i]->type_name . '</td>';
                                            echo '<td class="span3">' . $request_history[$i]->timestamp . '</td>';
                                            echo ($request_history[$i]->status == 0 ? '<td class="span2 text-warning">รออนุมัติ</td>' : '<td class="span2 text-success">อนุมัติ</td>');
                                            echo '</tr>';
                                        }
                                    } else {
                                        if (substr($eq, -2) == ", ") {
                                            $eq = substr($eq, 0, strlen($eq) - 2);
                                        }
                                        echo '<tr data-id="' . $tmp_id . '">';
                                        echo '<td>' . $count++ . '</td>';
                                        echo '<td class="span8">' . ($eq == '' ? $request_history[$i - 1]->equipment_name : $eq) . '</td>';
                                        echo '<td>' . $request_history[$i - 1]->type_name . '</td>';
                                        echo '<td class="span3">' . $request_history[$i - 1]->timestamp . '</td>';
                                        echo ($request_history[$i - 1]->status == 0 ? '<td class="span2 text-warning">รออนุมัติ</td>' : '<td class="span2 text-success">อนุมัติ</td>');
                                        echo '</tr>';
                                        $tmp_id = $request_history[$i]->id;
                                        $eq = $request_history[$i]->equipment_name . ", ";
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="span3">
                        <div class="well well-small">
                            <ul class="nav nav-list">
                                <li class="text-success">
                                    <h3>อนุมัติ <span id="count_approved"><?php echo $approved; ?></span> รายการ </h3>
                                </li>
                                <li class="text-warning">
                                    <h3>รออนุมัติ  <span id="count_unapproved"><?php echo $not_approved; ?></span> รายการ</h3>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <button class="btn btn-block btn-main" role="button" href="#req" data-toggle="modal">ส่งคำร้องขอใช้อุปกรณ์ </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- tab 3 -->
                <div class="tab-pane" id="logbook">
                    <div class="span7 offset1">
                        <table class="table table-bordered" id="log_history">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่ออุปกรณ์</th> 
                                    <th>วันที่ใช้งาน</th>
                                    <th>ต้องบันทึกภายในวันที่</th> 
                                    <th>สถานะ</th>
                                </tr>
                            </thead>
                            <!--Note: ลบ id="body-log" ใน tbody-->
                            <tbody>
                                <?php
                                $c = 1;
                                $n = 0;
                                $s = 0;
                                if (count($log_books) == 0) {
                                    echo '<tr><td colspan="6" style="text-align: center; color: red">(ยังไม่มีการบันทึกการทดลอง)</td></tr>';
                                }
                                
                                foreach ($log_books as $log) {

                                    echo '<tr data-id=' . $log->booking_id . ' recorded=' . $log->recorded . '>';
                                    echo '<td>' . $c . '</td>';
                                    echo '<td>' . $log->name . '</td>';
                                    echo '<td>' . $log->reserved_date . '</td>';
                                    echo '<td>' . $log->expected_to_record . '</td>';
                                    echo '<td><b>' . ($log->recorded == 1 ? '<span class="text-success">บันทึกแล้ว</span>' : '<span class="text-warning">รอการบันทึก</span>') . '</b></td>';
                                    echo '</tr>';
                                    if ($log->recorded == 1) {
                                        $s++;
                                    } else {
                                        $n++;
                                    }

                                    $c++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="span3 ">
                        <div class="well well-small">
                            <ul class="nav nav-list">
                                <li class="text-warning">
                                    <h4>ยังไม่บันทึก <? echo $n;?> รายการ</h4>
                                </li>
                                <li class="text-success">
                                    <h4>บันทึกแล้ว <? echo $s;?> รายการ</h4> 
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End content -->

<div class="modal hide fade" id="req" style="width: 850px;
     margin-left: -425px;">
    <div class="modal-dialog">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">คำร้องขอใช้อุปกรณ์</h4>
            <p class="text-error"><small>การส่งคำขอไปจะไม่ได้สิทธิ์การจองในทันที ต้องรอการอนุมัติก่อนนะครับ</small></p>
        </div>
        <div class="modal-body">
            <!--<form action="" method="post" id="form-request" onSubmit="return request()" class="form-horizontal">-->
            <form action="/member/request" method="post" id="form-request"  class="form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="typeRoom" style="width: 290px; margin-right:20px">เรียน หัวหน้าห้องปฎิบัติการเทตโนโลยีชีวภาพด้าน</label>
                    <div class="controls">
                        <select name="request-lab" class="span3">
                            <?php
                            foreach ($labs as $lab) {
                                echo '<option value="' . $lab->id . '">' . $lab->name . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inform1" style="width: 39px;">ผ่าน</label>
                    <div class="controls">
                        <input type="text" name="request-inform1" >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inform2" style="width: 39px;">ผ่าน</label>
                    <div class="controls">
                        <input type="text" name="request-inform2" >
                    </div>
                </div>
                <div class="well">
                    <p>ข้าพเจ้า
                        <input type="text" name="name" readonly="readonly" value="<?php echo $member->firstname . ' ' . $member->lastname; ?>" />
                        นักศึกษา(นักวิจัย) หลักสูตร(สังกัด)
                        <input type="text" name="syllabus" readonly="readonly" value="<?php echo $member->lab; ?>" />
                    </p>
                    คณะ
                    <input type="text" name="faculty" readonly="readonly" value="<?php echo $member->faculty; ?>"/>
                    มหาวิทยาลัย
                    <input type="text" name="university" readonly="readonly" value="<?php echo $member->university; ?>"/>
                    </p>
                    ทำวิทยานิพนธ์ (วิจัย) เรื่อง
                    <input type="text" class="span2" name="request-subject" />
                    อาจารย์ที่ปรึกษาวิทยานิพนธ์
                    <input type="text" class="span2" name="request-advisor" />
                    อาจารย์ที่ปรึกษาวิทยานิพนธ์ร่วม
                    <input type="text"  class="span2" name="request-sub-advisor"/>
                    สังกัดกลุ่มวิจัย (ห้องปฎิบัติการ) 
                    <select name="request-advisor-lab" class="span2">
                        <?php
                        foreach ($labs as $lab) {
                            echo '<option value="' . $lab->id . '">' . $lab->name . '</option>';
                        }
                        ?>
                    </select>
                    <br />
                    มีความจำเป็นต้องใช้อุปกรณ์ภายในห้องปฎิบัติการเทตโนโลยีชีวภาพด้าน <span class="text-error">*</span>
                    <select name="request-lab" class="span3">
                        <?php
                        foreach ($labs as $lab) {
                            echo '<option value="' . $lab->id . '">' . $lab->name . '</option>';
                        }
                        ?>
                    </select>
                    <br />
                    ดังนี้ <span class="text-error">*</span>
                    <!--<input type="text" class="span2" value="" onKeyUp="loadItems()" id="search" />-->
                    <input type="text" id="items" name="items" />
                    <!--<div id="loaded_items"></div>
                                  <div id="items_shown" style="margin:10px 0"></div>
                                  <div id="items"></div>--> 
                    <br />
                    ระหว่างวันที่ <span class="text-error">*</span>
                    <input type="date" name="request-start_date" id="request-start_date" />
                    -
                    <input type="date" name="request-end_date" id="request-end_date" />
                    <input type="hidden" name="request-member_id" value="<?php echo $member->id; ?>" />
                </div>
        </div>
        <div class="modal-footer"> <a data-dismiss="modal" class="btn">ปิด</a> <a  onClick="requestButton()" class="btn btn-main">ส่งคำขอ</a> </div>
        <input type="hidden" id="req-lab" disabled="disabled" value="" />
        </form>
    </div>
    <!-- /.modal-dialog --> 
</div>
<!-- /.modal -->  

<!-- Modal -->
<div class="modal hide fade" id="change-password-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">แบบฟอร์มเปลี่ยนรหัสผ่าน</h4>
            </div>
            <div class="modal-body">
                <table class="table table-condensed">
                    <tr style="border-top: none">
                        <td style="text-align: right; font-weight: bold; vertical-align: middle; border-top: none">รหัสผ่านเก่า:</td>
                        <td style="vertical-align: middle; border-top: none"><input type="password" class="form-control" id="old-password" required="required"></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold; vertical-align: middle">รหัสผ่านใหม่:</td>
                        <td style="vertical-align: middle"><input type="password" class="form-control" id="new-password" required="required"></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold; vertical-align: middle">ยืนยันรหัสผ่านใหม่:</td>
                        <td style="vertical-align: middle"><input type="password" class="form-control" id="confirm-new-password" required="required"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                <button type="button" class="btn btn-main" id="change-submit">เปลี่ยน</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'module/DataTables/js/jquery.dataTables.js'; ?>"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'asset/js/bootstrap-tagsinput.min.js'; ?>"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'asset/js/typeahead.min.js'; ?>"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url() . 'asset/js/jasny-bootstrap.min.js'; ?>"></script> 
<script type="text/javascript">
            var historyTable;
            $(function() {
                $('#change-password').click(function() {
                    $('#change-password-modal').modal('show');
                });

                $('#change-submit').click(function() {
                    var oldPassword = $('#old-password').val();
                    var newPassword = $('#new-password').val();
                    var confirmNewPassword = $('#confirm-new-password').val();

                    if (newPassword != confirmNewPassword) {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'รหัสผ่านใหม่ทั้งสองช่องไม่ตรงกัน!',
                            type: 'error'
                        });
                        return false;
                    } else {
                        $.post('/member/change_password_me', {old_password: oldPassword, new_password: newPassword}, function(data) {
                            if (data == 1) {
                                window.location.href = '/member/logout/member';
                            } else if (data == 2) {
                                $.pnotify({
                                    title: 'การดำเนินการ',
                                    text: 'รหัสผ่านเก่าไม่ถูกต้อง!',
                                    type: 'error'
                                });
                            } else {
                                alert(data);
                                $.pnotify({
                                    title: 'การดำเนินการ',
                                    text: 'เกิดข้อผิดพลาดระหว่างการเปลี่ยนรหัสผ่าน!',
                                    type: 'error'
                                });
                            }
                        });
                    }
                });

                $('#userfile').on('change.bs.fileinput', function() {
                    $.pnotify({
                        title: 'การดำเนินการ',
                        text: 'กำลังอัพโหลด...',
                        hide: false,
                        type: 'info'
                    });
                    $('#form-photo-upload').submit();
                });

                $('#req-lab').text($('select[name=request-lab]').val());
                var elt = $('#items');

                elt.tagsinput({
                    itemValue: 'equipment_id',
                    itemText: 'name'
                });
                elt.tagsinput('input').typeahead({
                    valueKey: 'name',
                    prefetch: '/member/load_items_json'
                }).bind('typeahead:selected', $.proxy(function(obj, datum) {
                    this.tagsinput('add', datum);
                    this.tagsinput('input').typeahead('setQuery', '');
                }, elt));

                if ($('#reserved_history tbody tr').length > 1) {
                    $('#reserved_history').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "sDom": '<"toolbar">frtip'
                    });
                    $("div.toolbar").html(' รายการจอง');
                    append_button_reserved();
                }

                if ($('#request_history tbody tr').length > 1) {
                    historyTable = $('#request_history').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "sDom": '<"toolbar2">frtip',
                        'iDisplayLength': 5
                    });
                    $("div.toolbar2").html('ประวัติการส่งคำร้อง');
                    append_button_request();
                }


                if ($('#log_history tbody tr').length > 1) {
                    $('#log_history').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "sDom": '<"toolbar3">frtip'
                    });
                    $("div.toolbar3").html('บันทึกการทดลอง');
                    append_button_logbook();
                }

                $('#reserved_history_previous,#reserved_history_next,.sorting').on('click', function() {

                    append_button_reserved();
                });

                $('#request_history_previous,#request_history_next,.sorting').on('click', function() {
                    append_button_request();
                });

                $('#log_history_previous,#log_history_next,.sorting').on('click', function() {
                    append_button_logbook();
                });

                $('#myTab a').click(function(e) {
                    e.preventDefault();
                    $(this).tab('show');
                })
                var active_tab_id = '<? echo $active_tab_index;?>';
                if (active_tab_id != '') {
                    $("#myTab li a:eq(" + active_tab_id + ")").trigger('click');
                }

            });

            function append_button_reserved() {
                $("#reserved_history tbody tr").each(function(index, element) {
                    if (typeof $(this).find(".dynamic_td").html() == "undefined") {
                        var time = $(this).attr("data-time");
                        var custom_colume = '';
                        if (time != '') {
                            custom_column = '<td class="dynamic_td"><a href="javascript:;" onClick="cancelByTimestamp(\'' + time + '\', $(this));"><i class="icon-remove-sign"></i></a></td>';
                        } else {
                            custom_column = '<td class="dynamic_td"><i  class="icon-remove-sign" title="ลบไม่ได้เนื่องจากเลยเวลาปัจจุบันแล้ว" ></i></td>';
                        }

                        $(this).append(custom_column);
                    }
                });
            }

            function append_button_request() {
                $("#request_history tbody tr").each(function(index, element) {
                    if (typeof $(this).find(".dynamic_td").html() == "undefined") {
                        var id = $(this).attr("data-id");
                        var custom_column = '<td class="dynamic_td"><a href="/member/request_without_data/' + id + '"><i class="icon-print"></i></a></td><td class="dynamic_td"><a href="javascript:;" onClick="delete_approval($(this))"><i class="icon-remove-sign"></i></a></td>'
                        $(this).append(custom_column);
                    }
                });

            }

            function append_button_logbook() {
                $("#log_history tbody tr").each(function(index, element) {
                    if (typeof $(this).find(".dynamic_td").html() == "undefined") {
                        var id = $(this).attr("data-id");
                        var recorded = $(this).attr("recorded");
                        var custom_column = '';
                        if (recorded != 1) {
                            custom_column = '<td class="dynamic_td"><a href="javascript:;" onclick="get_form(' + id + ')">ลงบันทึก <i class="icon-file-text-alt"></i> </a></td>'
                        } else {
                            custom_column = '<td class="dynamic_td"><a href="javascript:;" onclick="edit_form(' + id + ')">แก้ไข <i class="icon-edit"></i> </a></td>';
                        }
                        $(this).append(custom_column);
                    }
                });
            }

            function delete_approval(obj) {
                var id = obj.parents('tr').attr('data-id');
                var app = obj.parents('tr').find('td:eq(4)').text();

                $.get('/member/delete_approval/' + id, function(data) {
                    if (data == 1) {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'ลบการขอสิทธิ์เรียบร้อย!',
                            type: 'success'
                        });
                        obj.parents("tr").fadeOut("slow", function() {
                            var pos = historyTable.fnGetPosition(this);
                            historyTable.fnDeleteRow(pos);
                            append_button_request();

                            if (app == 'อนุมัติ') {
                                var count = parseInt($('#count_approved').text(), 10);
                                $('#count_approved').text(--count);
                            } else {
                                var count = parseInt($('#count_unapproved').text(), 10);
                                $('#count_unapproved').text(--count);
                            }
                        });
                    } else {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'เกิดข้อผิดพลาด!',
                            type: 'error'
                        });
                    }
                });
            }

            var m = 15;
            var countBooking = <?php echo $count_booking; ?>;
            $(document).on('mouseover', '.more', function() {
                $(this).css('background-color', '#FFB347');
            }).on('mouseout', '.more', function() {
                $(this).css('background-color', '#AEC6CF');
            });

            $('select[name=request-lab]').change(function() {
                $('#items').tagsinput('removeAll');
                $('#req-lab').text($(this).val());
            });



            function saveLog(form) {
                if ($(form).parent().find('.description').val() == '' || $(form).parent().find('.amount').val() == '') {
                    alert('กรุณากรอกข้อมูลให้ครบถ้วน!');
                    return false;
                }
                $.post('/member/save_log', $(form).serialize(), function(data) {
                    if (data.status == 1) {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'บันทึกข้อมูลการใช้งานเรียบร้อย!',
                            type: 'success'
                        });
                        $(form).parent('tr').hide(1000, function() {
                            if ($(this).siblings().size() == 0) {
                                $('#body-log').html('<tr><td colspan="7" style="text-align: center; color: red">(ไม่มีการจองที่ต้องบันทึก)</td></tr>');
                            }
                            $(this).remove();
                        });
                    } else {
                        alert('เกิดข้อผิดพลาด!');
                    }
                }, 'json');
                return false;
            }

            function cancelByTimestamp(timestamp, obj) {
                var c = confirm('คุณต้องการลบการจองนี้ทั้งหมดจริงหรือไม่?');
                if (!c)
                    return false;
                $.post('/member/cancel_booking_by_timestamp', {timestamp: timestamp}, function(data) {
                    if (data == '1') {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'ลบการจองเรียบร้อย!',
                            type: 'success'
                        });
                        obj.parents('tr').hide(1000, function() {
                            $(this).remove();
                        });
                    } else if (data == '2') {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'ไม่สามารถลบการจองของผู้อื่นได้!',
                            type: 'error'
                        });
                    } else {
                        $.pnotify({
                            title: 'การดำเนินการ',
                            text: 'เกิดข้อผิดพลาด!',
                            type: 'error'
                        });
                    }
                });
            }

            $('#items_shown').change(function() {
                if ($(this).html() == '') {
                    $(this).removeClass('well').removeClass('well-small');
                } else {
                    $(this).addClass('well').addClass('well-small');
                }
            });


            /*function request() {
             $.post('/member/request', $('#form-request').serialize());, function(data) {
             alert('done');
             $('#req').modal('hide');
             });
             return false;
             }*/



            function requestButton() {
                var start_date = $("#request-end_date").val();
                var end_date = $("#request-end_date").val();
                var items = $("#items").val();

                if (start_date == '' || end_date == '' || items == '') {
                    alert('กรุณาใส่ข้อมูลต่อไปให้ให้ครบถ้วน\n- อุปกรณ์ที่ต้องการขอใช้\n- ช่วงเวลาที่เริ่ม/สิ้นสุด ');
                } else {
                    $('#form-request').trigger('submit');
                    $('#req').modal('hide');
                }
            }

            function get_form(id) {
                loadPage("/member/logbook_form/" + id);
            }

            function edit_form(id) {
                loadPage("/member/logbook_form/" + id + "/" + true);
            }

</script> 
</div>
