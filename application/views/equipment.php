<!-- equipment -->
<style>
	#add_equip a{
		text-align:center;
		background: rgb(6,102,90); 	
		color:white;
	}
	
	#add_equip a:hover{
			background: rgb(6,102,90); 	
	}
</style>
<div class="warper">
	<div class="row">
    	<div class="span3">
        	<div class="title"><center>ด้านปฎิบัติการ</center></div>
            <ul class="nav nav-tabs nav-stacked boxShadow" id="admin-type"  style="background:#F2F2F2;">
                  <?php
                                		foreach ($equipment_types as $e) {
											echo '<li class="">';
                                			echo '<a href="#"><span style="display:none">'.$e->id.'</span>'. $e->name . '<i class="icon-chevron-right pull-right"></i></a>';
                                			echo '</li>';
										}
                                	?>             
            </ul>
        </div>
        <div class="span9">
        	<div class="content" style="padding-top:0;">
            	<div class="title"><center>อุปกรณ์</center></div>
            	
               <!-- <form class="form-horizontal marginTop20 pull-right" style="margin-right:10px;margin-bottom:0">
                    <div class="control-group">
                            <label class="control-label" for="admin-type">เลือกด้านปฎิบัติการ</label>
                            <div class="controls">
                                <select id="admin-type">
                                    <option value="no">-- กรุณาเลือกด้านปฏิบัติการ --</option>
                                	<?php
                                		foreach ($equipment_types as $e) {
                                			echo '<option value="' . $e->id . '">' . $e->name . '</option>';
                                		}
                                	?>
                                </select>
                            </div>
                    </div>
                </form>-->
            	<div id="before-row">
	                <div class="row"><div class="span7 offset1"><h4><< กรุณาเลือกห้องจากเมนูด้านซ้าย</h4></div>
               </div>
            </div><!-- end div content-->
            
        </div><!-- end span7-->
    </div><!-- end row-->

</div><!-- end warper-->
<script type="text/javascript">	
	$('#admin-type li a').click(function() {
		$('#before-row div').html('<p class="span5 offset1">กำลังโหลด..</p>');
		if ($(this).find('span').text() != 'no') {
			$.post('/admin/view', { type: $(this).find('span').text() }, function(data) {
				if ($(data).find('tbody').text() == '') {
					$('#before-row').html('<div class="row"><div class="span7 offset1"><h3 align="center"><font color="red">(ไม่มีอุปกรณ์ในห้องนี้)</font></h3></div>');	
				} else {
					$('#before-row').html(data);
				}
			});
		}
		$(this).parent().addClass('nav_active disabled').siblings().removeClass('nav_active disabled');
	});
	
</script>
