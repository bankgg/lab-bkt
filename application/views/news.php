<div class="warper" > 
	<div class="content" id="news_content">
        <div class="row">
            <div class="span10 offset1">
                <div class="page-header"><h2><?php echo $news->title; ?></h2></div>
                <?php echo $news->content; ?>
            </div>
            <div class="span3 offset8"><small class="pull-right">โพสเมื่อ <?php echo date('d/m/Y', strtotime($news->timestamp)); ?> เวลา <?php echo date('G:i', strtotime($news->timestamp)); ?> น.</small></div>
                
        </div>
    </div>


</div>