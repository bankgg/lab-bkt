<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="maximum-scale=0.8,  user-scalable=yes" />
<meta charset="utf-8" />
<title>เข้าสู่ระบบ</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/bootstrap.css'; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/bootstrap-responsive.min.css'; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'module/font-awesome/css/font-awesome.php'; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/style.css'; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/ui-lightness/jquery-ui-1.10.3.custom.min.css'; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.default.css'; ?>" />
</head>

<body>
<div class="navbar ">
  <div class="navbar-inner"  style="background: rgb(244, 119, 53)"> <a class="brand" href="javascript:;" onClick="home()" style="padding-top: 5px;
padding-bottom: 4px;"><img alt="ระบบจองอุปกรณ์" src="<?php echo base_url() .'/asset/img/2011071193826.jpg'?>" width="30">ระบบจองอุปกรณ์</a> 
    <ul class="nav" id="top_menu" >
      <li id="homeMenu" class="active"><a href="javascript:;" onClick="home()" class="  nonMember"><i class="icon-home"></i> หน้าแรก</a></li>
      <li id="bookMenu"><a href="javascript:;" onClick="click_item_schedule()" class="nonMember"><i class="icon-book"></i> จองอุปกรณ์</a></li>
      <li id="logbookMenu" ><a href="javascript:;" onClick="logbook()" class="nonMember">บันทึกการทดลอง</a></li>
      <li id="meMenu"><a class="meMenu member" href="javascript:;" onClick="me()"><i class="icon-user"></i> me</a></li>
    </ul> 
    <ul class="nav pull-right" id="top_menu_right" >
        <?php if ($privilege->id > 0): ?>
      <li id="adminMenu"><a class="adminMenu member" href="<?php echo base_url() .'admin'?>"><i class="icon-user-md"></i> admin</a></li>
      <?php endif; ?>
      <li id="loginMenu" class=" nonMember"><a class="loginShow" href="javascript:;" onClick="click_login()"><i class="icon-signin"></i> เข้าสู่ระบบ</a></li>
      <li id="regisMenu"><a class="regisShow nonMember" href="javascript:;" onClick="click_regis()"><i class="icon-plus"></i> สมัครสมาชิก</a></li>
      <li id="logoutMenu"><a class="logoutShow member" href="javascript:;" onClick="click_logout()"><i class="icon-signout"></i> ออกจากระบบ</a></li>
    </ul>
  </div> 
</div>
<div class="container" style="padding:0px"> </div>
<!-- End container -->

<div class="footer"> </div>
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/bootstrap.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery-ui-1.10.3.custom.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/js/lab.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.min.js'; ?>"></script> 
<script type="text/javascript">
                        $(function() {
                            $.pnotify.defaults.delay = 2500;
                            $.pnotify.defaults.history = false;
                            $.pnotify.defaults.animation_speed = 'normal';
                        });
						
						$("#top_menu li a").click(function(e) {
                            $("#top_menu_right li").removeClass('active');
                        });
						$("#top_menu_right li a").click(function(e) {
                            $("#top_menu li").removeClass('active');
                        }); 
        </script>
</body>
</html>