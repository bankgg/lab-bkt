<!DOCTYPE html >
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="maximum-scale=0.8,  user-scalable=yes" /> 
        <meta charset="utf-8" />
        <title>Admin</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/bootstrap.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/bootstrap-responsive.min.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'module/font-awesome/css/font-awesome.php'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/style.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/admin.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/ui-lightness/jquery-ui-1.10.3.custom.min.css'; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.default.css'; ?>" />
    </head>

    <body>
        <div class="navbar ">

            <div class="navbar-inner"  style="background: rgb(6,102,90)">
 
                <a class="brand" href="javascript:;" onClick="dashboard()" style="padding-top: 5px;
padding-bottom: 4px;"><img alt="ระบบจองอุปกรณ์" src="<?php echo base_url() .'/asset/img/2011071193826-nonebg.png'?>" width="30">ระบบจองอุปกรณ์</a> 
                <ul class="nav" >   
                    <li id="dashboardMenu"><a href="javascript:;" onClick="dashboard()" class="  nonMember">Dashboard</a></li>
                    <li id="equipmentMenu"><a href="javascript:;" onClick="equipment()" class="  nonMember">อุปกรณ์</a></li>
                    <li id="manageMenu" ><a href="javascript:;" onClick="manage()" class="nonMember">บันทึกการทดลอง</a></li>
                    <li id="priviledgeMenu" ><a href="javascript:;" onClick="priviledge()" class="  nonMember">สิทธิ์การใช้ &nbsp;<span class="req_count"></span></a> </li>
                    <?php if ($privilege->id > 1): ?>
                    <li id="manageEquipmentMenu" ><a href="javascript:;" onClick="manage_equipment()" class="  nonMember">จัดการครุภัณฑ์</a> </li>
                    <?php endif; ?>
                    <li id="addNewsMenu" ><a href="javascript:;" onClick="addNews()" class="  nonMember">ข่าว</a></li>
                    <li id="updatesMenu" ><a href="javascript:;" onClick="updates()" class="  nonMember">ความเคลื่อนไหว</a></li>
 
                </ul> 
                <ul class="nav pull-right" > 
                    <li id="userMenu"><a class="userMenu member" href="<?php echo base_url() .'member'?>"><i class="icon-user"></i> user</a></li>
                   <li id="logoutMenu"><a class="logoutShow member" href="javascript:;" onClick="click_logout()"><i class="icon-signout"></i> ออกจากระบบ</a></li>  
                </ul> 
            </div> 
        </div>

        <div class="container" style="padding:0px">

        </div> <!-- End container-fluid -->
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery-ui-1.10.3.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/adm.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/tinymce/tinymce.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/pnotify/jquery.pnotify.min.js'; ?>"></script>
        <script type="text/javascript">
                        $(function() {
                            $.pnotify.defaults.delay = 2500;
                            $.pnotify.defaults.history = false;
                            $.pnotify.defaults.animation_speed = 'normal';
                        });
        </script>
    </body>
</html>