<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('member_model', 'member');
        $this->load->model('booking_model', 'booking');
        $this->load->model('Me_model', 'me');
        $this->load->model('news_model', 'news');
        $this->load->model('log_model', 'log');
        $this->load->model('subscribe_model', 'subscribe');

    }

    public function index() {
        $this->load->view('index');
    }

    public function home() {
        $data = array(
            'url' => 'home',
            'news' => $this->news->get_home_news()
        );
        $this->load->view('home', $data);
    }

    public function register() {
        $data = array(
            'firstname' => $this->input->post('firstname'),
            'lastname' => $this->input->post('lastname'),
            'student_id' => $this->input->post('student_id'),
            'status' => $this->input->post('status'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'tel' => $this->input->post('tel'),
            'tel_in' => $this->input->post('tel_in'),
            'university' => $this->input->post('university'),
            'faculty' => $this->input->post('faculty'),
            'course' => $this->input->post('course'),
            'lab' => $this->input->post('lab')
        );

        if ($this->member->check_email($this->input->post('email')) > 0) {
            echo "email";
        } else {
            // Send to member model to insert to db
            $this->member->register($data);
            echo "success";
        }
    }

    public function emptydiv() {
        $this->load->view('emptydiv');
    }

    public function login() {
        $this->load->view('login');
    }

    public function load_register() {
        $data = array('labs' => $this->booking->get_equipment_types());
        $this->load->view('register', $data);
    }

    public function get_session() {
        if ($this->session->userdata('email') != '') {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function delete_approval($id) {
        echo $this->me->delete_approval($id) ? 1 : 0;
    }

    public function get_bookings($equipment_id, $date) {
        $data = $this->booking->get_bookings($equipment_id, $date);
        echo json_encode($data);
    }

    public function login2() {
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));
        $success = $this->member->login($email, $password);

        if ($success) {
            $this->session->set_userdata('email', $email);
        }

        echo $success ? 0 : -1;
    }

    public function check_email() {
        $email = $this->input->post('email');
        echo $this->member->check_email($email) > 0 ? "-1" : "1";
    }

    //Note เพิ่ม parameter d date เข้าไป
    public function booking($d, $room = '', $tag) {
        $date_elements = explode("-", $d);
        $date = $date_elements[2] . '-' . $date_elements[1] . '-' . $date_elements[0];

        $data = array(
            'member' => $this->me->get_me(),
            'equipment_types' => $this->booking->get_equipment_types(),
            //Note เพิ่ม 'date'=>$d
            'date' => $d,
            'date_inverse' => $date,
            'equipment' => $this->booking->get_equipment_from_tag($tag),
            'tag' => $tag,
            'room' => $room
        );
        $this->load->view('booking', $data);
    }

    public function logout($url = null) {
        if ($url == null) {
            $this->session->unset_userdata('email');
        } else {
            $this->session->unset_userdata('email');
            header('Location: /' . $url);
        }
    }

    public function book($step) {
        if ($step == 'type') {
            $type_id = $this->input->post('id');
            $rooms = $this->booking->get_rooms($type_id);
            $html = '';
            if (count($rooms) == 0) {
                $html .= '<option value="no">(ไมมีห้องของอุปกรณ์ด้านนี้)</option>';
            } else {
                $html .= '<option value="no">-- กรุณาเลือกห้อง --</option>';
            }

            foreach ($rooms as $r) {
                $html .= '<option value="' . $r->id . '">' . $r->name . '</option>';
            }
            echo $html;
        } else if ($step == 'time') {
            // Set time array
            $time1 = array('00:00 - 00:30', '00:30 - 01:00', '01:00 - 01:30', '01:30 - 02:00', '02:00 - 02:30', '02:30 - 03:00', '03:00 - 03:30',
                '03:30 - 04:00', '04:00 - 04:30', '04:30 - 05:00', '05:00 - 05:30', '05:30 - 06:00', '06:00 - 06:30', '06:30 - 07:00',
                '07:00 - 07:30', '07:30 - 08:00', '08:00 - 08:30', '08:30 - 09:00', '09:00 - 09:30', '09:30 - 10:00', '10:00 - 10:30',
                '10:30 - 11:00', '11:00 - 11:30', '11:30 - 12:00');
            $time2 = array('12:00 - 12:30', '12:30 - 13:00', '13:00 - 13:30', '13:30 - 14:00', '14:00 - 14:30', '14:30 - 15:00', '15:00 - 15:30',
                '15:30 - 16:00', '16:00 - 16:30', '16:30 - 17:00', '17:00 - 17:30', '17:30 - 18:00', '18:00 - 18:30', '18:30 - 19:00',
                '19:00 - 19:30', '19:30 - 20:00', '20:00 - 20:30', '20:30 - 21:00', '21:00 - 21:30', '21:30 - 22:00', '22:00 - 22:30',
                '22:30 - 23:00', '23:00 - 23:30', '23:30 - 24:00');

            $type_id = $this->input->post('typeId');
            $room = $this->input->post('roomId');
            $date = $this->input->post('date');
            $time = $this->input->post('time');
            $interval = $time == '1' ? '00.00 - 12.00น.' : '12.00 - 24.00น.';

            $timeArray = $time == '1' ? $time1 : $time2;
            $bookings = $this->booking->get_bookings($type_id, $room, $date);
            $equipments = $this->booking->get_equipments($room);

            $html = '<table  class="table table-bordered  table-condensed" id="itemlist">';
            $html .= '<caption>ตารางการจองของห้อง <b>' . $this->booking->get_room_name($room) . '</b> ในวันที่ <b>' . $date . '</b> ช่วงเวลา <b>' . $interval . '</b></caption>';
            $html .= '<thead><tr><th>เวลา</th>';
            $ii = 1;
            foreach ($equipments as $e) {
                $html .= '<th>' . strtoupper($e->name) . '<input type="hidden" id="index' . $ii++ . '" value="' . $e->equipment_id . '" /></th>';
            }
            $html .= '</tr></thead>';
            // Table body
            $html .= '<tbody>';
            $k = 1;
            $countEquip = count($equipments);
            $span = ceil(12.0 / $countEquip);
            $member_id = $this->member->get_id_from_email($this->session->userdata('email'));
            for ($i = 0; $i < count($timeArray); $i++) {
                $timeInBlock = explode('-', $timeArray[$i]);
                $startInBlock = trim($timeInBlock[0]);
                $endInBlock = trim($timeInBlock[1]);
                $html .= '<tr id="t' . $k++ . '">';
                $html .= '<td style="width: 110px; text-align: center">' . $timeArray[$i] . '</td>';
                foreach ($equipments as $e) {
                    $bookingInThisBlock = '';
                    foreach ($bookings as $b) {
                        $start = strtotime($b->start);
                        $end = strtotime($b->end);
                        $dateStart = date('Y-m-d', strtotime($b->start));
                        $dateEnd = date('Y-m-d', strtotime($b->end));
                        $blockStart = strtotime($dateStart . ' ' . $startInBlock . ':00');
                        $blockEnd = strtotime($dateEnd . ' ' . $endInBlock . ':00');
                        if ($blockStart >= $start && $blockEnd <= $end && $e->equipment_id == $b->equipment_id) {
                            $bookingInThisBlock = $b;
                        }
                    }
                    $timeId = str_replace(':', '', $startInBlock);
                    $start = $startInBlock;
                    $can = $this->booking->check_privilege($e->equipment_id);
                    $end = substr($timeId, 2) + 30 == 60 ? (substr($timeId, 0, 1) . substr($timeId, 1, 1) + 1 . ':00') : (substr($timeId, 0, 2) . ':' . (substr($timeId, 2) + 30));
                    if ($bookingInThisBlock == '') {
                        if ($can) {
                            $html .= '<td class="t' . $timeId . ' span' . $span . '" onClick="book($(this), \'' . $e->equipment_id . '\', \'' . $start . '\', \'' . $end . '\')"></td>';
                        } else {
                            $html .= '<td class="t' . $timeId . ' span' . $span . '" onClick="loadRequestForm();"></td>';
                        }
                    } else {
                        if ($member_id == $bookingInThisBlock->member_id) {
                            $html .= '<td class="bookline span' . $span . '" onClick="cancel($(this), \'' . $bookingInThisBlock->equipment_id . '\', \'' . $bookingInThisBlock->start . '\', \'' . $bookingInThisBlock->end . '\')"><b>จองโดย:</b> ' . $bookingInThisBlock->firstname . ($bookingInThisBlock->description != '' ? (' (' . $bookingInThisBlock->description . ')') : '') . '</td>';
                        } else {
                            $html .= '<td class="bookline span' . $span . '"><b>จองโดย:</b> ' . $bookingInThisBlock->firstname . ($bookingInThisBlock->description != '' ? (' (' . $bookingInThisBlock->description . ')') : '') . '</td>';
                        }
                    }

                    $html .= '</td>';
                }
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            echo $html;
        }
    }

    public function book_done() {
        if (!$this->booking->check_privilege($this->input->post('equipment_id'))) {
            echo -1;
            return true;
        }
        $member_id = $this->member->get_id_from_email($this->input->post('email'));
        $data = array(
            'equipment_id' => $this->input->post('equipment_id'),
            'start' => $this->input->post('start'),
            'end' => $this->input->post('end'),
            'description' => $this->input->post('description'),
            'member_id' => $member_id
        );

        if ($this->booking->book($data)) {
            echo '<b>จองโดย:</b> ' . $this->member->get_member_data($member_id)->firstname . ($this->input->post('description') != '' ? (' (' . $this->input->post('description') . ')') : '');
        } else {
            echo 0;
        }
    }

    public function me($active_tab_index = null) {
        $types = $this->me->get_types_i_can();
        //print_r($types);
        $i = 0;
        foreach ($types as $t) {
            $ts[$i] = $t->name;
            $equipments[$i++] = $this->me->get_privileges($t->id);
        }
        $data = array('member' => $this->me->get_me(),
            'booking' => $this->me->get_booking(),
            'equipments' => @$equipments,
            'types' => @$ts,
            'count_booking' => $this->me->count_booking(),
            'log_books' => $this->log->get_log_books(),
            'labs' => $this->booking->get_labs(),
            'request_history' => $this->me->get_requestform_by_member(),
            'approved' => $this->me->get_total_status_by_member(1),
            'not_approved' => $this->me->get_total_status_by_member(0),
            'active_tab_index' => $active_tab_index
        );
        $this->load->view('me', $data);
    }

    public function check_privilege() {
        if ($this->booking->check_privilege($this->input->post('equipment_id'))) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function me_load_more($start) {
        $more = $this->me->get_booking($start);
        $mores = $this->input->post('more');
        foreach ($more as $b) {
            echo '<tr>';
            echo '<td>' . ++$start . '</td>';
            echo '<td><a href="javascript:;" onClick="click_booking(\'' . date('d-m-Y', strtotime($b->start)) . '\', ' . $b->room_id . ', \'' . $b->equipment_tag . '\')">' . $b->equipment_name . '</a> ' . (time() < strtotime($b->start) ? '<a href="javascript:;" onClick="cancelByTimestamp(\'' . $b->timestamp . '\', $(this));"><span class="label label-warning">ลบ</span></a>' : '') . '</td>';
            echo '<td>' . $b->type . '</td>';
            echo '<td>' . $b->room_name . '</td>';
            echo '<td>' . date('Y-m-d', strtotime($b->start)) . '</td>';
            echo '<td>' . (date('H:i', strtotime($b->start)) . ' - ' . date('H:i', strtotime($b->end))) . '</td>';
            echo '</tr>';
        }
        echo $mores == '1' ? '<tr><td colspan="6" class="more" onClick="more($(this))" style="text-align: center; background-color: #AEC6CF"><a href="javascript:;"><b><font color="orange">แสดงเพิ่มเติม..</font></b></a></td></tr>' : '';
    }

    public function save_log() {
        $data = array(
            'equipment_id' => $this->input->post('equipment_id'),
            'date' => $this->input->post('date'),
            'amount' => $this->input->post('amount'),
            'description' => $this->input->post('description'),
            'remark' => $this->input->post('remark'),
            'old_start' => $this->input->post('old_start'),
            'old_end' => $this->input->post('old_end')
        );
        if ($this->me->save_log($data)) {
            echo json_encode(array('status' => '1'));
        } else {
            echo json_encode(array('status' => '0'));
        }
    }

    public function load_items() {
        $items = $this->booking->search($this->input->post('lab'), $this->input->post('search'));
        foreach ($items as $i) {
            echo '<a href="javascript:;" onClick="addItem(\'' . $i->equipment_id . '\', \'' . $i->name . '\')">' . $i->name . '</a>&nbsp;&nbsp&nbsp;';
        }
    }

    public function load_items_json($lab = null) {
        $items = $this->booking->search($lab);
        echo json_encode($items);
    }

    public function request() {
        $lab = $this->member->get_lab_from_id($this->input->post('request-lab'));
        $advisor_lab = $this->member->get_lab_from_id($this->input->post('request-advisor-lab'));
        $data = array(
            'member_id' => $this->input->post('request-member_id'),
            'subject' => $this->input->post('request-subject'),
            'lab' => $this->input->post('request-lab'),
            'inform1' => $this->input->post('request-inform1'),
            'inform2' => $this->input->post('request-inform2'),
            'advisor' => $this->input->post('request-advisor'),
            'sub_advisor' => $this->input->post('request-sub-advisor'),
            'advisor_lab' => $this->input->post('request-advisor-lab'),
            'start_date' => $this->input->post('request-start_date'),
            'end_date' => $this->input->post('request-end_date')
        );
        $insert_id = $this->me->request($data);
        $items = explode(',', $this->input->post('items'));
        if ($items != null) {
            foreach ($items as $i) {
                $this->me->request_item_add($insert_id, $i);
            }
        }

        //เปลี่ยน Data เพื่อเอาไปใช้เขียน PDF    
		$data['advisor_lab'] = $advisor_lab->name;
		$data['lab'] = $lab->name;
        $data['syllabus'] = $this->input->post('syllabus');
        $data['faculty'] = $this->input->post('faculty');
        $data['university'] = $this->input->post('university');
        $data['name'] = $this->input->post('name');
        $data['items'] = $this->me->get_items_by_id($insert_id);
        header('Location: /member/generate_request_form/' . rawurlencode(json_encode($data)));
        //$this->generate_request_form($data);
    }

    public function request_without_data($request_id) {
        $req_data = $this->me->get_requestform_by_id($request_id);
        $lab = $this->member->get_lab_from_id($req_data->lab);
        $advisor_lab = $this->member->get_lab_from_id($req_data->advisor_lab);
        $member_data = $this->member->get_member_data($req_data->member_id);
        $lab_data = $this->member->get_lab_from_id($member_data->lab);
        $items_array = $this->me->get_items_by_id($request_id);

        $data = array(
            'name' => $member_data->firstname . ' ' . $member_data->lastname,
            'member_id' => $req_data->member_id,
            'subject' => $req_data->subject,
            'lab' => $lab->name,
            'inform1' => $req_data->inform1,
            'inform2' => $req_data->inform2,
            'advisor' => $req_data->advisor,
            'sub_advisor' => $req_data->sub_advisor,
            'advisor_lab' => $advisor_lab->name,
            'start_date' => $req_data->start_date,
            'end_date' => $req_data->end_date,
            'syllabus' => $lab_data->name,
            'faculty' => $member_data->faculty,
            'university' => $member_data->university,
            'items' => $items_array
        );
        //$this->generate_request_form($data);
        header('Location: /member/generate_request_form/' . rawurlencode(json_encode($data)));
    }

    public function generate_request_form($data) {
        $data = json_decode(rawurldecode($data), true);

        define('FPDF_FONTPATH', 'asset/File/fpdf/font/');
        require_once('asset/File/fpdf/fpdf.php');
        require_once('asset/File/fpdi/fpdi.php');
        // initiate FPDI  
        $pdf = new FPDI();

        // set the sourcefile  
        $pdf->setSourceFile('asset/File/requestForm.pdf');
        // import page 1  
        $tplIdx = $pdf->importPage(1);
        $pdf->AddPage();
        // use the imported page and place it at point 10,10 with a width of 200 mm   (This is the image of the included pdf)
        $pdf->useTemplate($tplIdx, 10, 10, 200);
        // now write some text above the imported page
        //$pdf->SetTextColor(0,0,255);


        $pdf->AddFont('angsana', '', 'angsa.php');
        $pdf->SetFont('angsana', '', 14);


        $pdf->SetXY(47, 62);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['inform1']));

        $pdf->SetXY(47, 69);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['inform2']));

        $pdf->SetXY(62, 79);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['name']));

        $pdf->SetXY(167, 79);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['syllabus']));

        $pdf->SetXY(34, 85);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['faculty']));

        $pdf->SetXY(115, 85);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['university']));

        $pdf->SetXY(59, 91);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['subject']));

        $pdf->SetXY(120, 91);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['advisor']));

        $pdf->SetXY(70, 98);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['sub_advisor']));

        $pdf->SetXY(48, 104);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['advisor_lab']));


        $pdf->SetXY(166, 110);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['lab']));


        $pdf->SetXY(48, 117);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['end_date']));

        $pdf->SetXY(86, 117);
        $pdf->Write(30, iconv('UTF-8', 'cp874', $data['start_date']));



        // import page 2  
        $tplIdx = $pdf->importPage(2);
        $pdf->AddPage();
        // use the imported page and place it at point 10,10 with a width of 200 mm   (This is the image of the included pdf)
        $pdf->useTemplate($tplIdx, 10, 10, 200);
        // now write some text above the imported page
        //$pdf->SetTextColor(0,0,255);


        $pdf->AddFont('angsana', '', 'angsa.php');
        $pdf->SetFont('angsana', '', 14);


        $y = 29;
        $item = '';
        if (is_array($data['items'])) {
            $item = $data['items'];
            for ($i = 0; $i < count($item); $i++) {
                $pdf->SetXY(40, $y+=7);
                $tmp = $i + 1;
                $pdf->Write(30, iconv('UTF-8', 'cp874', $tmp . ". " . $item[$i]['name']));
            }
        } else {
            $item = preg_split("/,/", $data['items']);
            for ($i = 0; $i < count($item); $i++) {
                $pdf->SetXY(40, $y+=7);
                $tmp = $i + 1;
                $pdf->Write(30, iconv('UTF-8', 'cp874', $tmp . ". " . $item[$i]));
            }
        }


        $y = 29;
        for ($i = 1; $i <= 5; $i++) {
            $pdf->SetXY(138, $y+=7);
            $pdf->Write(30, iconv('UTF-8', 'cp874', "$i. Mindfire"));
        }


        $y = 128;
        for ($i = 1; $i <= 5; $i++) {
            $pdf->SetXY(40, $y+=7);
            $pdf->Write(30, iconv('UTF-8', 'cp874', "$i. Mindfire"));
        }

        $y = 128;
        for ($i = 1; $i <= 5; $i++) {
            $pdf->SetXY(138, $y+=7);
            $pdf->Write(30, iconv('UTF-8', 'cp874', "$i. Mindfire"));
        }
        $pdf->Output('request_form.pdf', 'I');
    }

    public function cancel() {
        $equipment_id = $this->input->post('equipment_id');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $email = $this->input->post('email');

        $member_id = $this->member->get_id_from_email($email);
        $owner_member_id = $this->booking->get_booking($equipment_id, $start, $end)->member_id;
        if ($member_id != $owner_member_id)
            echo 2;
        else
            echo $this->booking->cancel($equipment_id, $start, $end) ? 1 : 0;
    }

    public function cancel_booking_by_timestamp() {
        $timestamp = $this->input->post('timestamp');

        $member_id = $this->me->get_me()->id;
        $owner_member_id = $this->booking->get_booking_from_timestamp($timestamp)->member_id;
        if ($member_id != $owner_member_id)
            echo 2;
        else
            echo $this->booking->cancel_by_timestamp($timestamp) ? 1 : 0;
    }

    public function footer() {
        $this->load->view('footer');
    }

    public function news($id) {
        $data = array('news' => $this->news->get_news_data($id));
        $this->load->view('news', $data);
    }

    public function item_schedule($name = null, $room = null, $tag = '') {
        if (empty($name)) {
            $data = array('equipments' => $this->booking->get_all_equipments_groupby_name(), 'name' => '', 'room' => '', 'tag' => '');
        } else {
            $data = array('equipments' => $this->booking->get_all_equipments_groupby_name(), 'name' => rawurldecode($name), 'room' => $room, 'tag' => $tag);
        }
        $this->load->view('item_schedule', $data);
    }

    public function findRoomFromEquipName() {
        $name = $this->input->post('name');
        $data = array('room' => $this->booking->get_room_from_equip_name($name));
        echo json_encode($data);
    }

    public function findTagFromRoom() {
        $room = $this->input->post('room');
        $name = $this->input->post('name');
        $data = array('tag' => $this->booking->get_tag_from_nameAndRoom($name, $room));
        echo json_encode($data);
    }

    public function find_equipments_from_nameAndRoom() {
        $room = $this->input->post('room');
        $name = $this->input->post('name');
        $data = array('equipments' => $this->booking->get_equipments_from_nameAndRoom($name, $room));
        echo json_encode($data);
    }

    public function find_equipments_from_equipmenid() {
        $id = $this->input->post('id');
        $data = array('equipments' => $this->booking->get_equipment_name_from_id($id));
        echo json_encode($data);
    }

    public function find_booking_from_nameAndRoom() {
        $room = $this->input->post('room');
        $name = $this->input->post('name');
        $data = array('booking' => $this->booking->get_booking_from_nameAndRoom($name, $room));
        echo json_encode($data);
    }

    public function find_booking_from_id() {
        $id = $this->input->post('equipment_id');
        $data = array('booking' => $this->booking->get_booking_from_equipment($id));
        echo json_encode($data);
    }

    public function find_booking_from_nameRoomAndTag() {
        $room = $this->input->post('room');
        $name = $this->input->post('name');
        $tag = $this->input->post('tag');
        $data = array('booking' => $this->booking->get_booking_from_nameRoomAndTag($name, $room, $tag),
            'equipments' => $this->booking->get_equipment_from_tag($tag));
        echo json_encode($data);
    }

    public function add_log($edit = null) {
        $arr = array();
        $columns = $this->log->get_columns($this->input->post('booking_id'));
        foreach ($columns as $column) {
            $arr[$column] = $this->input->post($column);
        }

        if ($edit == 'edit') {
            echo $this->log->edit_log($arr, $this->input->post('booking_id')) ? 1 : 0;
        } else {
            $arr['booking_id'] = $this->input->post('booking_id');
            echo $this->log->add_log($arr) ? 1 : 0;
        }
    }

    public function get_form($booking_id, $key = null) {
        if ($key == null) {
            $arr = array();
            $columns = $this->log->get_columns($booking_id);
            foreach ($columns as $column) {
                $arr[$column] = array(
                    'thai_name' => $this->log->get_thai_name($column),
                    'type' => (strpos(strtolower($this->log->get_column_type($column)), 'int') !== false ? 'int' :
                            (strpos(strtolower($this->log->get_column_type($column)), 'datetime') !== false ? 'datetime' : 'varchar'))
                );
            }
            echo json_encode($arr);
        } else {
            $log_book = $this->log->get_log_book($booking_id);
            echo $log_book->$key;
        }
    }

    public function logbook_form($booking_id, $edit = false) {
        $data = array();

        if ($edit == true) {
            $data['edit'] = 'true';
        } else {
            $data['edit'] = 'false';
        }
        $data['booking_id'] = $booking_id;
        $data['equipment_name'] = $this->booking->get_equipment_name_from_booking_id($booking_id);
        $this->load->view('logbook_form', $data);
    }

    public function send_email_forget() {
        $email = $this->input->post('email');
        $rand = $this->me->insert_temp_key($email);

        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'bankgg@gmail.com';
        $config['smtp_pass'] = '1993bank';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;
        $this->email->initialize($config);

        $this->email->from('admin@labbkt.com', 'LAB BKT');
        $this->email->to($email);
 
        $this->email->subject('[LAB] Your password reset link');
        $this->email->message('คุณสามารถรีเซ็ตรหัสผ่านของคุณได้ที่<br /><br />' . anchor('member/forget/' . urlencode($rand)));

        $this->email->send();
        echo $this->email->print_debugger();
    }

    public function forget($key) {
        if (!$this->me->key_exists($key)) {
            echo 'The key doesn\'t exist!';
        } else {
            $data = array(
                'key' => $key
            );
            $this->load->view('forget_password', $data);
        }
    }

    public function change_password() {
        $key = urldecode($this->input->post('key'));
        $password = $this->input->post('password');
        $this->me->change_password($key, $password);
    }

    public function change_password_me() {
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');

        if ($this->me->is_old_password_correct($old_password)) {
            if ($this->me->change_password_me($new_password)) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function upload_photo() {
        $config['upload_path'] = 'photos/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1024';
        $config['max_width'] = '2048';
        $config['max_height'] = '1536';
        $config['file_name'] = 'photo-' . $this->me->get_me()->id;
        $config['overwrite'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            echo $this->upload->display_errors();
        } else {
            $data = array('upload_data' => $this->upload->data());
            $this->me->update_photo($data['upload_data']['file_name']);
            header('Location: /member');
        }
    }

    public function check_pass_of_current_user() {
        $password = md5($this->input->post('password'));
        $correct_password = $this->me->get_me()->password;

        echo ($password === $correct_password) ? 1 : 0;
    }
	
	public function subscribe(){ 
		$data = array(
			'email' => $this->input->post('email')
		);
		if( $this->subscribe->add_subscribe($data)==1){
			$this->load->library('email');
 
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ssl://smtp.gmail.com';
			$config['smtp_port'] = '465';
			$config['smtp_timeout'] = '7';
			$config['smtp_user'] = 'bankgg@gmail.com';
			$config['smtp_pass'] = '1993bank';
			$config['charset'] = 'utf-8';
			$config['newline'] = "\r\n";
			$config['mailtype'] = 'html';
			$config['validation'] = TRUE;
			$this->email->initialize($config);
	
			$this->email->from('admin@labbkt.com', 'LAB BKT');
			$this->email->to($data['email']);
	  
			$this->email->subject('[LAB] อีเมล์คุณได้ทำการติดตามข่าวสารเรียบร้อยแล้ว');
			$this->email->message('คุณจะได้รับข่าวสารทันทีที่มีการอัพเดตในหน้าเว็บ <br/><br/> คุณสามารถเลิกรับข่าวสารจากระบบได้โดยกรอกอีเมล์ที่นี่<form action="'.base_url().'member/unsubscribe" method="post">
				<input type="text" name="email">    
				<input type="submit" value="OK!">
			</form>');
			$this->email->send();
			
			header("Location: ".base_url()."member"); 
		};
	}
	
	
	public function unsubscribe(){  
		$email = $this->input->post('email'); 
		if($email!=''){
			$result =  $this->subscribe->remove_subscribe($email);
		}
		$data = array(
			'email' => $email, 
			'result' => $result
		);
		$this->load->view('unsubscribe',$data);
	}  
	
	public function check_subscribe(){
		$email = $this->input->post('email');
		echo $this->subscribe->check_subscribe($email); 
	}
	
	public function logbook(){
		 $data = array(
            'equipments' => $this->booking->get_all_equipments_and_tag()
        );
        $this->load->view('logbook', $data);
   }
}
