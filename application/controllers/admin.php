<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('admin_model', 'admin');
        $this->load->model('booking_model', 'booking');
        $this->load->model('news_model', 'news');
        $this->load->model('log_model', 'log');
        $this->load->model('subscribe_model', 'subscribe');
        $this->load->model('me_model', 'me');
    }

    public function index() {
        $data = array(
            'privilege' => $this->me->get_privilege()
        );
        $this->load->view('admin', $data);
    }

    public function equipment() {
        $data = array(
            'equipment_types' => $this->booking->get_equipment_types()
        );
        $this->load->view('equipment', $data);
    }

    public function add_type() {
        $data = array(
            'name' => $this->input->post('type')
        );
        echo $this->admin->add_type($data) ? 1 : 0;
    }

    public function add_room() {
        $data = array(
            'name' => $this->input->post('name'),
            'type' => $this->input->post('type')
        );
        echo $this->admin->add_room($data) ? 1 : 0;
    }

    public function add_equipment() {
//        $arr = array();
//        array_push($arr, 'time_start');
//        array_push($arr, 'time_end');
//        if ($this->input->post('columns') != null) {
//            foreach ($this->input->post('columns') as $c) {
//                array_push($arr, $c);
//            }
//        }
//
//        $arr2 = array();
//        if ($this->input->post('columns_added') != null) {
//            foreach ($this->input->post('columns_added') as $c) {
//                if (strlen($c) > 0) {
//                    $vars = explode(',', $c);
//                    $name = $vars[0];
//                    $thai_name = $vars[1];
//                    $type = $vars[2];
//
//                    $this->log->add_column($name, $thai_name, $type);
//                    array_push($arr, $name);
//                }
//            }
//        }
//
//        $data = array(
//            'name' => $this->input->post('equipment-name'),
//            'room' => $this->input->post('equipment-room'),
//            'tag' => $this->input->post('equipment-tag'),
//            'limit_times' => $this->input->post('equipment-limit-times'),
//            'limit_hours' => $this->input->post('equipment-limit-hours'),
//            'columns' => json_encode($arr)
//        );
//        echo $this->admin->add_equipment($data) ? 1 : 0;
        $data = array_filter($this->input->post());

        $config['upload_path'] = 'files/';
        $config['allowed_types'] = 'gif|jpg|png|xlsx|docx|doc|xls|pdf';
        $config['max_size'] = '8192';
        $config['max_width'] = '2048';
        $config['max_height'] = '2048';

        $this->load->library('upload', $config);

        foreach ($_FILES as $fieldname => $fileObject) {  //fieldname is the form field name
            if (!empty($fileObject['name'])) {
                $this->upload->initialize($config);
                if (!$this->upload->do_upload($fieldname)) {
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } else {
                    $upload_data = $this->upload->data();
                    $data[$fieldname == 'equipment_image_path' ? 'equipment_image_path' : 'specific_info_path'] = $config['upload_path'] . $upload_data['file_name'];
                }
            }
        }


        unset($data['type']);
        unset($data['equipment_type']);
        //print_r($data);
        if ($this->admin->add_equipment($data)) {
            echo 'เพิ่มอุปกรณ์สำเร็จ!<br /><a href="javascript:history.back()">&lt;&lt; Back</a>';
        } else {
            echo 'Error!';
        }
    }

    public function add_news() {
        $data = array(
            'member_id' => $this->member->get_member_id(),
            'all_news' => $this->news->get_news(),
            'count_news' => $this->news->count_news()
        );
        $this->load->view('addNews', $data);
    }

    public function view() {
        $type = $this->input->post('type');
        $rooms = $this->booking->get_rooms($type);

        foreach ($rooms as $r) {
            $records = $this->admin->get_stats($r->id);
            echo '<div class="row">
		                	<div class="span7 offset1">
		                        <div class="page-header" style="margin-top:0">
		                            <h1>' . $r->name . '</h1>
		                        </div>
		                        <table class="table table-bordered">
		                          <thead>
		                            <tr>
		                              <th>ชื่ออุปกรณ์ (TAG)</th>
		                              <th>เวลาใช้งานสะสม</th>
		                              <th>สถานะอุปกรณ์</th>
		                            </tr>
		                          </thead>
		                          <tbody style="cursor:pointer">';
            if (count($records) == 0) {
                echo '<tr>';
                echo '<td colspan="3" style="color: red; text-align: center">(ยังไม่มีอุปกรณ์ในห้องนี้)</td>';
                echo '</tr>';
            }
            foreach ($records as $rec) {
                if ($rec->hours != '') {
                    $time = 'ใช้ไป ' . $rec->hours . ' ชั่วโมง';
                } else {
                    $time = '<i><font color="red">(ยังไม่มีการใช้งาน)</font></i>';
                }

                if (($rec->limit_times != '' && $rec->times >= $rec->limit_times && $rec->hours != '') || ($rec->limit_hours != '' && $rec->real_hours >= $rec->limit_hours && $rec->hours != '')) {
                    $status = '<tr class="warning" onclick="details(' . $rec->equipment_id . ')">';
                    $status1 = '<td>ต้องซ่อมบำรุง</td>';
                } else {
                    $status = '<tr onclick="details(' . $rec->equipment_id . ')">';
                    $status1 = '<td>ใช้งานได้ปกติ</td>';
                }
                echo $status . '
										  <td>' . $rec->name . ' (' . $rec->tag . ')</td>
										  <td>' . $time . '</td>
										  ' . $status1 . '
										</tr>';
            }
            echo '</tbody>
									</table>
								</div><!-- end span-->
							</div><!-- end row-->';
        }
    }

    public function priviledge($lab = -1) {
        if ($lab == -1) {
            $lab = $this->admin->get_first_lab()->id;
        }
        $data = array(
            'approved_forms' => $this->admin->get_approved_forms($lab),
            'unapproved_forms' => $this->admin->get_unapproved_forms(),
            'labs' => $this->booking->get_equipment_types(),
            'lab_name' => $this->booking->get_equipment_type($lab)->name
        );
        $this->load->view('priviledge', $data);
    }

    public function new_privilege_exist() {
        echo count($this->admin->get_unapproved_forms());
    }

    public function request_approve() {
        $id = $this->input->post('id');
        $req = $this->admin->get_request_form($id);
        $items = $this->admin->get_request_form_items($id);
        $lab = $req->lab;
        $this->admin->request_approve($id);

        $items_list = '';
        foreach ($items as $i) {
            $items_list .= '- ' . $i->name . '<br />';
        }

        // Sending Email to Requester
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'bankgg@gmail.com';
        $config['smtp_pass'] = '1993bank';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;
        $this->email->initialize($config);
        $this->email->from('admin@labbkt.com', 'Mr. Approver');
        $this->email->to($this->member->get_member_data($req->member_id)->email);
        $this->email->subject('[LAB ' . $req->type_name . '] คุณสามารถจองอุปกรณ์ที่ร้องขอใช้งานไว้ได้แล้วครับ');
        $this->email->message('คุณสามารถจองอุปกรณ์ภายในห้องปฏิบัติการด้าน "' . $req->type_name . '" ตั้งแต่วันที่ ' . $req->start_date . ' จนถึง ' . $req->end_date . '<br /><br />โดยมีอุปกรณ์ที่มีผลดังต่อไปนี้:<br />' . $items_list . '<br />คุณสามารถตรวจสอบอุปกรณ์ที่คุณสามารถจองได้ ได้ที่ ' . base_url('member'));
        $this->email->send();

        echo $lab;
    }

    public function request_unapprove() {
        $id = $this->input->post('id');
        $req = $this->admin->get_request_form($id);
        $items = $this->admin->get_request_form_items($id);
        $lab = $req->lab;
        $this->admin->request_unapprove($id);

        $items_list = '';
        foreach ($items as $i) {
            $items_list .= '- ' . $i->name . '<br />';
        }

        // Sending Email to Requester
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'bankgg@gmail.com';
        $config['smtp_pass'] = '1993bank';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;
        $this->email->initialize($config);
        $this->email->from('admin@labbkt.com', 'Mr. Approver');
        $this->email->to($this->member->get_member_data($req->member_id)->email);
        $this->email->subject('[LAB ' . $req->type_name . '] คุณถูกยกเลิกสิทธิการขอจองอุปกรณ์');
        $this->email->message('คุณถูกยกเลิกการขอจองอุปกรณ์ในห้องปฏิบัติการด้าน "' . $req->type_name . '" ที่ได้ทำการร้องขอจองอุปกรณ์ไว้ตั้งแต่วันที่ ' . $req->start_date . ' จนถึง ' . $req->end_date . '<br /><br />โดยมีอุปกรณ์ที่มีผลดังต่อไปนี้:<br />' . $items_list . '<br />คุณสามารถตรวจสอบอุปกรณ์ที่คุณสามารถใช้งานได้ ได้ที่ ' . base_url('member'));
        $this->email->send();

        echo $lab;
    }

    public function request_cancel() {
        $id = $this->input->post('id');
        $req = $this->admin->get_request_form($id);
        $items = $this->admin->get_request_form_items($id);
        $lab = $req->lab;
        $this->admin->request_cancel($id);

        $items_list = '';
        foreach ($items as $i) {
            $items_list .= '- ' . $i->name . '<br />';
        }

        // Sending Email to Requester
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'bankgg@gmail.com';
        $config['smtp_pass'] = '1993bank';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;
        $this->email->initialize($config);

        $this->email->from('admin@labbkt.com', 'Mr. Approver');
        $this->email->to($this->member->get_member_data($req->member_id)->email);
        $this->email->subject('[LAB ' . $req->type_name . '] คำร้องขอจองอุปกรณ์ของคุณไม่ได้รับการอนุมัติ');
        $this->email->message('คำร้องขอใช้อุปรณ์ของคุณภายในห้องปฏิบัติการด้าน "' . $req->type_name . '" ที่ได้ทำการร้องขอจองอุปกรณ์ไว้ตั้งแต่วันที่ ' . $req->start_date . ' จนถึง ' . $req->end_date . ' ไม่ได้รับการอนุมัติ<br /><br />โดยที่อุปกรณ์ที่ทำการร้องขอไปมีดังนี้:<br />' . $items_list . '<br />คุณสามารถตรวจสอบอุปกรณ์ที่คุณสามารถใช้งานได้ ได้ที่ ' . base_url('member'));
        $this->email->send();

        echo $lab;
    }

    public function manage() {
        $columns = $this->log->get_all_columns();
        $arr = array();
        foreach ($columns as $column) {
            if (strtolower($column->name) == 'booking_id' || strtolower($column->name) == 'timestamp')
                continue;
            $arr[strtolower($column->name)] = array(
                'thai_name' => $this->log->get_thai_name(strtolower($column->name)),
                'type' => $this->log->get_column_type($column->name),
            );
        }
        $data = array(
            'columns' => $arr,
            'equipments' => $this->booking->get_all_equipments_and_tag()
        );
        $this->load->view('manage', $data);
    }

    public function submit_news() {
        $data = array(
            'title' => $this->input->post('news_title'),
            'content' => stripcslashes($this->input->post('news_content')),
            'member_id' => $this->input->post('member_id')
        );
        $result = $this->news->submit_news($data);
        $subscribe_email_str = '';
        $subscribe_email = $this->subscribe->get_all_subscriber();
        foreach ($subscribe_email as $sem) {
            $subscribe_email_str .= $sem['email'] . ',';
        }
        // Sending Email to Subscriber 
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'bankgg@gmail.com';
        $config['smtp_pass'] = '1993bank';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;
        $this->email->initialize($config);

        $this->email->from('admin@labbkt.com', 'MR. News');
        $this->email->bcc($subscribe_email_str);
        $this->email->subject('[LAB News] - ' . $data['title']);
        $this->email->message($data['content'] . '<br/><br/>คุณสามารถเลิกรับข่าวสารจากระบบได้โดยกรอกอีเมล์ที่นี่<form action="http://lab.bankgg.in.th/member/unsubscribe" method="post">
        	<input type="text" name="email">   
            <input type="submit" value="OK!">
        </form>');

        $this->email->send();

        echo $result ? 1 : 0;
    }

    public function get_news() {
        $start = $this->input->post('start');
        $news = $this->news->get_news($start);
        $i = $start;
        foreach ($news as $n) {
            echo '<tr>';
            echo '<td>' . ++$i . '</td>';
            echo '<td>' . $n->title . '</td>';
            echo '<td>' . $n->timestamp . '</td>';
            echo '<td><a href="javascript:;" onClick="editNews(\'' . $n->id . '\')">แก้ไข</a></td>';
            echo '<td><a href="javascript:;" onClick="deleteNews(\'' . $n->id . '\')"><i class="icon-remove-sign"></i></a></td>';
            echo '</tr>';
        }
    }

    public function delete_news() {
        $id = $this->input->post('id');
        echo $this->news->delete_news($id) ? 1 : 0;
    }

    public function edit_news($id) {
        $news = $this->news->get_news_data($id);
        $data = array(
            'title' => $news->title,
            'content' => $news->content,
            'id' => $news->id,
            'member_id' => $news->member_id
        );
        $this->load->view('edit_news', $data);
    }

    public function submit_edit_news() {
        $data = array(
            'title' => $this->input->post('news_title'),
            'content' => $this->input->post('news_content')
        );
        $news_id = $this->input->post('news_id');
        $member_id = $this->input->post('member_id');
        echo $this->news->submit_edit_news($news_id, $member_id, $data) ? 1 : 0;
    }

    public function detail_item($id = 0) {
        $columns = $this->log->get_all_columns();
        $arr = array();
        foreach ($columns as $column) {
            if (strtolower($column->name) == 'booking_id' || strtolower($column->name) == 'timestamp')
                continue;
            $arr[strtolower($column->name)] = array(
                'thai_name' => $this->log->get_thai_name(strtolower($column->name)),
                'type' => $this->log->get_column_type($column->name)
            );
        }
        $data = array(
            'columns' => $arr,
            'detail' => $this->admin->get_equipment_by_id($id),
            'column_item' => $this->log->get_columns_from_equipment_id($id)
        );
        $this->load->view('detail_item', $data);
    }

    public function detail_item_json($id = 0) {
        $columns = $this->log->get_all_columns();
        $arr = array();
        foreach ($columns as $column) {
            if (strtolower($column->name) == 'booking_id' || strtolower($column->name) == 'timestamp')
                continue;
            $arr[strtolower($column->name)] = array(
                'thai_name' => $this->log->get_thai_name(strtolower($column->name)),
                'type' => $this->log->get_column_type($column->name)
            );
        }
        $columns = $arr;
        $detail = $this->admin->get_equipment_by_id($id);
        $column_item = $this->log->get_columns_from_equipment_id($id);

        $count = 0;
        foreach ($columns as $name => $col) {
            echo '<label class="checkbox inline">';
            echo '<input type="checkbox" name="columns[]" value="' . $name . '" ' . ($name == 'time_start' || $name == 'time_end' ? 'checked="checked" readonly="readonly"' : '') . ' ' . (in_array($name, $column_item) ? 'checked="checked"' : '') . '/>' . $col['thai_name'];
            echo '</label>';
            echo $count % 2 == 1 ? '<br />' : '';
            $count++;
        }
        echo '<input type="hidden" value="' . $detail->equipment_id . '" name="equipment_id"  />';
        echo '<div style="margin-top:10px" id="other_column" class="text-error"> *หากไม่มีหัวข้อที่ต้องการให้ไปกด "ยกเลิก" แล้วเพิ่มหัวข้อที่ด้านขวามือก่อน </div>';
    }

    public function json_all_equipments() {
        $equipments = $this->booking->get_all_equipments();
        $arr = array();
        foreach ($equipments as $e) {
            array_push($arr, $e->name);
        }
        echo json_encode($arr);
    }

    public function delete_column($name) {
        echo $this->log->delete_column($name) ? 1 : 0;
    }

    public function delete_room($id) {
        echo $this->booking->delete_room($id) ? 1 : 0;
    }

    public function delete_equipment_type($id) {
        echo $this->booking->delete_equipment_type($id) ? 1 : 0;
    }

    public function add_column() {
        $name = $this->input->post('name');
        $thai_name = $this->input->post('thai_name');
        $type = $this->input->post('type');

        echo $this->log->add_column($name, $thai_name, $type) ? 1 : 0;
    }

    public function update_column() {
        $column = $this->input->post('columns');
        $id = $this->input->post('equipment_id');
        $result = $this->log->update_columns($id, $column);
        if ($result == 1)
            $result = $id;
        else
            $result = 0;
        echo $result;
    }

    public function dashboard() {
        $this->load->view("dashboard");
    }

    public function updates() {
        $this->load->model('updates_model', 'updates');
        $data = array(
            'updates' => $this->updates->get_updates()
        );
        $this->load->view('updates', $data);
    }

    public function add_update() {
        $this->load->model('updates_model', 'updates');
        $config['upload_path'] = 'updates/';
        $config['allowed_types'] = 'doc|docx|pdf|xls|xlsx|ppt|pptx';
        $config['max_size'] = '8192';
        $config['file_name'] = 'update-' . $this->input->post('date');
        $config['overwrite'] = false;
        $data = array();

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            echo $this->upload->display_errors();
        } else {
            $d = $this->upload->data();
            $data['filename'] = $d['file_name'];
            $data['date'] = $this->input->post('date');
            $this->updates->add_update($data);
            header('Location: /admin');
        }
    }

    public function rand_char($length) {
        $this->load->model('me_model', 'me');
        return $this->me->rand_char($length);
    }

    public function manage_equipment() {
        $columns = $this->log->get_all_columns();
        $arr = array();
        foreach ($columns as $column) {
            if (strtolower($column->name) == 'booking_id' || strtolower($column->name) == 'timestamp')
                continue;
            $arr[strtolower($column->name)] = array(
                'thai_name' => $this->log->get_thai_name(strtolower($column->name)),
                'type' => $this->log->get_column_type($column->name)
            );
        }
        $data = array(
            'equipment_types' => $this->booking->get_equipment_types(),
            'columns' => $arr,
            'rooms' => $this->booking->get_all_rooms(),
            'buying_methods' => $this->admin->get_buying_methods(),
            'all_equipment_data' => $this->admin->get_all_equipment_data()
        );
        $this->load->view('manage_equipment', $data);
    }

    public function get_columns($equipment_id) {
        echo json_encode($this->log->get_columns_from_equipment_id($equipment_id));
    }

    public function get_log_books($equipment_id) {
        echo json_encode($this->log->get_log_books_from_equipment_id($equipment_id));
    }

}

?>