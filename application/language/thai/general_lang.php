<?php
// General
$lang['gen_edit'] = 'แก้ไข';
$lang['gen_equipment_reservation_system'] = 'ระบบจองอุปกรณ์';
$lang['gen_equipment'] = 'อุปกรณ์';
$lang['gen_view_more'] = 'ดูเพิ่ม';
$lang['gen_room'] = 'ห้อง';
$lang['gen_date'] = 'วันที่';
$lang['gen_time'] = 'เวลา';
$lang['gen_cancel'] = 'ยกเลิก';
$lang['gen_loading'] = 'กำลังโหลด..';

?>