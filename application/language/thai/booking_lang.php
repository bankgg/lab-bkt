<?php
// Booking
$lang['book_reserved_by'] = 'จองโดย:';
$lang['book_equipment_reserved_by'] = 'จองอุปกรณ์ โดย';
$lang['book_to_do'] = 'สิ่งที่จะทำ';
$lang['book_all_day'] = 'ตลอดทั้งวัน';
$lang['book_from_datetime'] = 'จากวันที่ / เวลา';
$lang['book_to_datetime'] = 'ถึงวันที่ / เวลา';
$lang['book_reserve_now'] = 'จองอุปกรณ์เดี๋ยวนี้!';
$lang['book_select_time_range'] = '-- กรุณาเลือกช่วงเวลา --';
$lang['book_reservation_of_room'] = 'ตารางการจองของห้อง';
?>