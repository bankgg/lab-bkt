<?php
// News
$lang['news'] = 'ข่าว';
$lang['news_subject'] = 'หัวข้อ';
$lang['news_datetime_posted'] = 'วัน/เวลาที่ลง';
$lang['news_no_data'] = 'ยังไม่มีข่าว';
$lang['news_add'] = 'เพิ่มข่าว';
$lang['news_banner'] = 'แบนเนอร์';
$lang['news_select_image'] = 'เลือกรูป';
$lang['news_image_caption'] = 'คำบรรยายใต้ภาพ';
$lang['news_content'] = 'เนื้อหา';
$lang['news_add'] = 'เพิ่ม!';
?>