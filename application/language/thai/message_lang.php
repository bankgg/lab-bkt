<?php
// Message - Form
$lang['msg_please_fill'] = 'กรุณากรอกข้อมูลให้ครบถ้วน!';
$lang['msg_error_alert'] = 'เกิดข้อผิดพลาด!';

// Message - News
$lang['msg_confirm_delete_news'] = 'คุณต้องการลบข่าวนี้จริงหรือไม่?';

// Message - Booking
$lang['msg_click_to_reserve'] = 'คลิกเพื่อจอง';
$lang['msg_invalid_time'] = 'เวลาไม่ถูกต้อง!';
$lang['msg_cofirm_delete_booking'] = 'คุณต้องการลบการจองนี้จิงหรือไม่?';
$lang['msg_success_delete_booking'] = 'ลบการจองเรียบร้อย!';
?>