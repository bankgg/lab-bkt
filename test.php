
<!DOCTYPE html>
<html>
    <head>
        <title>Bootstrap Tags Input</title>
        <meta name="robots" content="index, follow" />
        <base href="../" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

        <link rel="stylesheet" href="http://timschlechter.github.io/bootstrap-tagsinput/examples/bower_components/bootstrap/dist/css/bootstrap.min.css">

        <link rel="stylesheet" href="http://timschlechter.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
        <link rel="stylesheet" href="http://timschlechter.github.io/bootstrap-tagsinput/examples/assets/app.css">


    </head>
    <body>


        <input type="text" id="items" />
        <button onClick="alert($('#items').val())">Alert</button>

        <script src="http://timschlechter.github.io/bootstrap-tagsinput/examples/bower_components/jquery/jquery.min.js"></script>
        <script src="http://timschlechter.github.io/bootstrap-tagsinput/examples/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="http://timschlechter.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>
        <script src="http://timschlechter.github.io/bootstrap-tagsinput/examples/bower_components/typeahead.js/dist/typeahead.min.js"></script>
        <script type="text/javascript">
            var elt = $('#items');

            elt.tagsinput({
                itemValue: 'equipment_id',
                itemText: 'name'
            });
            elt.tagsinput('input').typeahead({
                valueKey: 'name',
                prefetch: '/member/load_items_json/13'
            }).bind('typeahead:selected', $.proxy(function(obj, datum) {
                this.tagsinput('add', datum);
                this.tagsinput('input').typeahead('setQuery', '');
            }, elt));


//            elt = $('#items');
//            elt.tagsinput({
//                itemValue: 'value',
//                itemText: 'text'
//            });
////            elt.tagsinput('add', {"value": 1, "text": "Amsterdam", "continent": "Europe"});
////            elt.tagsinput('add', {"value": 4, "text": "Washington", "continent": "America"});
////            elt.tagsinput('add', {"value": 7, "text": "Sydney", "continent": "Australia"});
////            elt.tagsinput('add', {"value": 10, "text": "Beijing", "continent": "Asia"});
////            elt.tagsinput('add', {"value": 13, "text": "Cairo", "continent": "Africa"});
//
//            elt.tagsinput('input').typeahead({
//                valueKey: 'text',
//                prefetch: 'asset/cities.json',
//
//            }).bind('typeahead:selected', $.proxy(function(obj, datum) {
//                this.tagsinput('add', datum);
//                this.tagsinput('input').typeahead('setQuery', '');
//            }, elt));
        </script>
    </body>
</html>
