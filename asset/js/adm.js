﻿// JavaScript Document
$(function() {
	$('.req_count').addClass('hide');
	dashboard()	
});


function equipment(){
	activeMenu("#equipmentMenu");
	loadPage('/admin/equipment');	  
}

function priviledge(id){
	id = typeof id !== 'undefined' ? id : -1;
	activeMenu("#priviledgeMenu");
	loadPage('/admin/priviledge/' + id);
}

function addNews(){
	activeMenu("#addNewsMenu");
	loadPage('/admin/add_news')	;
}

function updates(){
	activeMenu("#updatesMenu");
	loadPage('/admin/updates')	;
}

function showAddForm(){
	$('#addForm').slideToggle('fast');
	$("html, body").animate({ scrollTop: $('body').height() }, "slow");	
}

function manage(){
	activeMenu("#manageMenu");
	loadPage('/admin/manage')	;
}
 
function details(id){
	loadPage('/admin/detail_item/'+id);
}

function loadPage(page) {
	 $('body').find('.container-fluid').removeClass('container-fluid').addClass('container');
   var loading = '<div class="row"><div class="span4 offset4" id="loading" style="font-weight: bold; text-align: center"><center><i class="icon-spinner icon-spin icon-4x"></i></center><br>กำลังโหลดข้อมูล...</div> <!-- End success --></div>';
    $('.container:first').html(loading);
    $('.container:first').load(page, function() {
	   checkNewPrivilege();
       $(this).show(1000); 
    });
}


function activeMenu(id){
		$(id).addClass('active').siblings().removeClass('active'); 
}

function checkNewPrivilege() {
	$.get('/admin/new_privilege_exist', function(data) {
		if (data > '0') {
			if ($('.req_count').hasClass('hide')) {
				$('.req_count').removeClass('hide');
			}
			$('.req_count').text(data);
		} else {
			if (!$('.req_count').hasClass('hide')) {
				$('.req_count').addClass('hide');
			}
		}
	});
}

function dashboard(){ 
	activeMenu("#dashboardMenu");
	loadPage("/admin/dashboard");	
}

function manage_equipment(){	
	activeMenu("#manageEquipmentMenu");
	loadPage("/admin/manage_equipment");	
}