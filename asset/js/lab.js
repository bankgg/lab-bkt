﻿$(function() {
    $('#logoutMenu').hide();
    $('#meMenu').hide();

    $('.carousel').carousel({
        interval: 4000,
    });

    $.ajax({
        type: 'POST',
        url: '/member/get_session',
        success: function(msg) {
            if (msg == 0) {
                home();
                changeMenu(0);
            } else {
                document.title = 'ระบบจองอุปกรณ์';
                home();
                changeMenu(1);
            }
        }
    });


});

function home() {
    activeMenu("#homeMenu");
    document.title = 'ระบบจองอุปกรณ์';
    loadPage('/member/home');

}

function me() {
    activeMenu("#meMenu");
    document.title = 'Me';
    loadPage('/member/me');
}

function register() {
    $('#btn-register').attr('readonly', 'readonly');
    $('#btn-register').attr('value', 'Please wait..');

    var firstname = $('#regis-inputFirstname').val();
    var lastname = $('#regis-inputLastname').val();
    var status = !$('#regis-status-student').prop('checked') && !$('#regis-status-researcher').prop('checked') ? null :
            $('#regis-status-student').prop('checked') ? $('#regis-status-student').val() : $('#regis-status-researcher').val();
    var email = $('#regis-inputEmail').val();
    var password = $('#regis-inputPassword').val();
    var conPassword = $('#regis-inputConPassword').val();
    var tel = $('#regis-tel').val();
    var telIn = $('#regis-tel_in').val();
    var university = $('#regis-university').val();
    var faculty = $('#regis-faculty').val();
    var course = $('#regis-course').val();
    var lab = $('#regis-lab').val();
    var studentId = $('#student_id').val();

    var text = '';
    var c = 0;

    if (firstname == '') {
        text += '\n- ชื่อจริง';
        c++;
    }

    if (lastname == '') {
        text += '\n- นามสกุล';
        c++;
    }

    if (status == null) {
        text += '\n- สถานภาพ';
        c++;
    }

    if (email == '') {
        text += '\n- อีเมล';
        c++;
    }

    if (password == '') {
        text += '\n- รหัสผ่าน';
        c++;
    }

    if (university == '') {
        text += '\n- มหาวิทยาลัย';
        c++;
    }

    if (faculty == '') {
        text += '\n- คณะ';
        c++;
    }

    if (course == '') {
        text += '\n- หลักสูตร (สังกัด)';
        c++;
    }

    if (lab == 'no') {
        text += '\n- สังกัดกลุ่มวิจัย (ห้องปฎิบัติการ)';
        c++;
    }

    if (tel == '') {
        text += '\n- เบอร์โทรศัพท์';
        c++;
    }

    if (telIn == '') {
        text += '\n- เบอร์โทรศัพท์ (ภายใน)';
        c++;
    }

    if (c > 0) {
        alert('กรุณากรอกข้อมูลดังต่อไปนี้:\n' + text);
        return false;
    }

    if (password !== conPassword) {
        alert('รหัสผ่านทั้งสองช่องไม่ตรงกัน!');
        return false;
    }

    if (email.indexOf('kmutt.ac.th') == -1) {
        alert('คุณต้องเป็นนักศึกษา/บุคลากรของ KMUTT \n กรุณาใช้ E-mail ของมหาวิทยาลัยเท่านั้น');
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/member/register',
        data: {firstname: firstname, lastname: lastname, status: status, student_id: studentId, email: email, password: password, tel: tel, tel_in: telIn, university: university, faculty: faculty, course: course, lab: lab},
        success: function(msg) {
            if (msg == 'email') {
                alert('อีเมล์นี้มีผู้ใช้แล้ว!');
            } else {
                document.title = 'เข้าสู่ระบบ';
                $.pnotify({
                    title: 'การดำเนินการ',
                    text: 'สมัครสมาชิกเรียบร้อย!',
                    type: 'success'
                });
                click_login();
            }
        }
    });

    return false;
}
//Note เปลี่ยน parameter จาก obj -> date
function click_booking(date, room, tag) {
    $.ajax({
        type: 'get',
        url: '/member/get_session',
        success: function(msg) {
            if (msg == 0) {
                click_login();
            } else {
                document.title = 'ระบบจองห้อง';
                activeMenu("#bookMenu");
                //Note เพิ่ม date
                loadPage('/member/booking/' + date + '/' + room + '/' + tag);
                changeMenu(1);
            }
        }
    });
}
function click_regis() {
    activeMenu("#regisMenu");
    $('.warper').animate({
        'margin-top': '-100%'

    }, 500, function() {

        $('.container:first').load('/member/load_register', function() {
            document.title = 'สมัครสมาชิก';
            activeMenu("#regisMenu");
            $('#form_register').animate({
                'margin-top': '0%'
            })
        });


    });
}

function click_login() {
    activeMenu("#loginMenu");
    $('.warper').animate({
        'margin-top': '-100%'

    }, 500, function() {

        $('.container:first').load('/member/login', function() {
            document.title = 'เข้าสู่ระบบ';
            $('#form_login').animate({
                'margin-top': '0%'
            }, 500, function() {
                $('#login-inputEmail').focus();
            });
            $("html, body").animate({scrollTop: 0}, "slow");
        });
    });
}

function click_logout() {

    $.ajax({
        type: 'POST',
        url: '/member/logout',
        success: function(msg) {
            document.title = 'เข้าสู่ระบบ';
            changeMenu(0);
            activeMenu('#loginMenu');
            $('.container').load('/member/login', '', function() {
                click_login();
            });

        }
    });
}

function click_item_schedule(name) {
    activeMenu("#bookMenu");
    if (name == null) {
        loadPage('/member/item_schedule');
    } else {
        loadPage('/member/item_schedule/' + name);
    }
}

function confirmPassword() {
    var password1 = $('#regis-inputPassword').val();
    var password2 = $('#regis-inputConPassword').val();

    if (password1 !== password2) {
        $('#error-password').text('รหัสไม่ตรงกัน!');
    } else {
        $('#error-password').text('');
    }
}

function checkEmail() {
    var email = $('#regis-inputEmail').val();

    $.ajax({
        type: 'POST',
        url: '/member/check_email',
        data: {email: email},
        success: function(msg) {
            if (msg == -1) {
                $('#error-email').text('อีเมล์นี้มีผู้ใช้แล้ว!');
            } else {
                $('#error-email').text('');
            }
        }
    });
}

function login() {
    var email = $('#login-inputEmail').val();
    var password = $('#login-inputPassword').val();

    $.ajax({
        type: 'POST',
        url: '/member/login2',
        data: {email: email, password: password},
        success: function(msg) {
            if (msg == -1) {
                $('#login_error').show(500);
            } else {
                $('#form_login').animate({
                    'margin-top': '-100%'

                }, 500, function() {
                    activeMenu("#bookMenu");
                    changeMenu(1);
                    document.title = 'ระบบจองห้อง';

                    loadPage('/member/item_schedule');

                }
                );
            }
        }
    });

    return false;
}



function changeMenu(val) {
    switch (val) {
        case 0:
            $('#logoutMenu').hide();
            $('#meMenu').hide();
            $('#loginMenu').show();
            $('#regisMenu').show();
            $('#bookMenu').hide();
            break;

        case 1:
            $('#logoutMenu').show();
            $('#meMenu').show();
            $('#loginMenu').hide();
            $('#regisMenu').hide();
            $('#bookMenu').show();
            break;
    }
}


function loadRequestForm() {
    var r = confirm('คุณไม่มีสิทธิ์ในการจองอุปกรณ์นี้!\n\nกดปุ่มตกลง เพื่อกรอกแบบฟอร์มสำหรับการจอง หรือกดยกเลิก เพื่อยกเลิกการจอง');
    if (r) {
        $('.container').load('/member/me', function() {
            $('#req').modal('show');
        });
    }
}

function loadPage(page) {
    var loading = '<div class="row"><div class="span4 offset4" id="loading" style="font-weight: bold; text-align: center"><center><i class="icon-spinner icon-spin icon-4x"></i></center><br>กำลังโหลดข้อมูล...</div> <!-- End success --></div>';
    $('.container:first').html(loading);
    $('.container:first').load(page, function() {

        $(this).show(1000);
    });
}

function activeMenu(id) {
    $(id).addClass('active').siblings().removeClass('active');
}

function addZero(str) {
    var a = str;
    if (str < 10) {
        a = "0" + str;
    }
    return a;
}


// JavaScript Document